#!/bin/bash

############################################################
############# BEGIN SLURM CONFIGURATION ######################

#SBATCH --partition=standard

### Walltime is clock time
### --time HH:MM:SS (below, 1 hour)
#SBATCH --time=01:00:00

### Specify the number of nodes, cpus and memory
#SBATCH --nodes=1 --ntasks=16

### Specify a name for the job
#SBATCH --job-name=runmrtrix3_participant_none

### Specify the group name
#SBATCH --account=dkp

### standard error and standard out are joined by default

#SBATCH --mail-type=ALL
#SBATCH --mail-user=dkp@arizona.edu


############################################################
############## BEGIN SCRIPT TO RUN #########################
###    Run me on el gato ###

# Run this script from the MRIS directory so that logs will be created there.

# Note the following can be defined in your .bash_profile, but to be safe we'll define it here as well.
# This one should take precedence if the other is incorrect or not defined.
export SIF=/contrib/singularity/shared/neuroimaging

# Define variables to point to the directories you want to work in
# This is not strictly necessary, but may be helpful
# bids and derivatives directories are siblings in this scheme unde a directory called MRI
export MRIS=/groups/dkp/BIDS

# Just to be safe, create the derivatives subdirectory if it does not exist.
if [ ! -d ${MRIS}/derivatives ]; then 
    mkdir ${MRIS}/derivatives
fi

Subject=${sub}

# Note, derivatives/MRtrix3_connectome-participant will be overwritten if it exists!

singularity run --cleanenv --bind ${MRIS}/Nifti:/data:ro --bind ${MRIS}/derivatives:/outputs ${SIF}/mrtrix3_connectome.sif /data /outputs participant --participant_label ${Subject} --parcellation none --output_verbosity 4
