#!/bin/bash

############################################################
############# BEGIN SLURM CONFIGURATION ######################

#SBATCH --partition=standard

### Walltime is clock time
### --time HH:MM:SS (below, 5 minutes)
#SBATCH --time=0:05:00

### Specify the number of nodes, cpus and memory
#SBATCH --nodes=1 --ntasks=2 

### Specify a name for the job
#SBATCH --job-name=runmriqc_group_dev

### Specify the group name
#SBATCH --account=dkp

### standard error and standard out are joined by default

#SBATCH --mail-type=ALL
#SBATCH --mail-user=dkp@arizona.edu

############################################################
############## BEGIN SCRIPT TO RUN #########################

# Run this script from the MRIS directory so that logs will be created there.

# Note the following can be defined in your .bash_profile, but to be safe we'll define it here as well.
# This one should take precedence if the other is incorrect or not defined.
export SIF=/contrib/singularity/shared/neuroimaging

# Which container do you want to run
export APP=${SIF}/mriqc_v23.1.0.sif


# Define variables to point to the directories you want to work in
# bids and derivatives directories are siblings in this scheme under a directory referred to as MRIS
export MRIS=/groups/dkp/Batch/CAM003_bids_data
export DATA=${MRIS}/data

# Define the output directories used by this app:
export APP_DERIV_DIR=${MRIS}/derivatives/mriqc_dev
export WORK_DIR=${MRIS}/mriqc_work_dev

# Just to be safe, create the derivatives subdirectory if it does not exist.
if [ ! -d ${APP_DERIV_DIR} ]; then 
    mkdir -p ${APP_DERIV_DIR}
fi


# Just to be safe, create the work directory if it does not exist. 
# This should be outside the bids input dir.
if [ ! -d ${WORK_DIR} ]; then 
    mkdir ${WORK_DIR}
fi

# bind mount derivatives or derivatives/mriqc, but operate on the contents of the derivatives/mriqc directory for the group processing
# For mriqc 23.1.0 add -m modality flag with the modalities to process. Without this, the presence of a dwi directory will result in a crash.
apptainer run --cleanenv --bind ${DATA}:/data:ro --bind ${APP_DERIV_DIR}:/outputs ${APP} /data /outputs group -m T1w T2w bold

