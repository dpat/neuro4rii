#!/bin/bash

############################################################
############# BEGIN SLURM CONFIGURATION ######################

#SBATCH --partition=standard

### Walltime is clock time
### --time HH:MM:SS (below, 30 minutes)
#SBATCH --time=0:30:00

### Specify the number of nodes, cpus and memory
#SBATCH --nodes=1 --ntasks=10 

### Specify a name for the job
#SBATCH --job-name=runmriqc

### Specify the group name
#SBATCH --account=dkp

### standard error and standard out are joined by default

#SBATCH --mail-type=ALL
#SBATCH --mail-user=dkp@arizona.edu

############################################################
############## BEGIN SCRIPT TO RUN #########################

# Run this script from the MRIS directory so that logs will be created there.

# Note the following can be defined in your .bash_profile, but to be safe we'll define it here as well.
# This one should take precedence if the other is incorrect or not defined.
export SIF=/contrib/singularity/shared/neuroimaging

# Which container do you want to run? The default linked one is 23.0.1:
export APP=${SIF}/mriqc_v23.0.1.sif

# Define variables to point to the directories you want to work in
# bids and derivatives directories are siblings in this scheme under a directory referred to as MRIS
export MRIS=/groups/dkp/Batch/CAM003_bids_data
export DATA=${MRIS}/data

# Define the output directories used by this app:
export APP_DERIV_DIR=${MRIS}/derivatives/mriqc
export WORK_DIR=${MRIS}/mriqc_work


# Just to be safe, create the derivatives subdirectory if it does not exist.
if [ ! -d ${APP_DERIV_DIR} ]; then 
    mkdir -p ${APP_DERIV_DIR}
fi

# Just to be safe, create the work directory if it does not exist. 
# This should be outside the bids input dir.
if [ ! -d ${WORK_DIR} ]; then 
    mkdir ${WORK_DIR}
fi


Subject=${sub}

# Use --cleanenv to prevent problems with singularity grabbing variables, libraries etc.
# from your HPC directories.  You want the environment to be defined
# by what is inside the container.
# --bind just like docker, we want to use /data defined inside the container.
# We need to bind the directory on the HPC (e.g., ${MYSTUFF}) to /data:
# We specify the name of the singularity container to run.
# Use -ica to generate melodic ica output. This capability is removed in subsequent versions 


# Because the directories and the app to run have been defined as variables above, 
# there is no need to alter the apptainer command, unless you want to revise app-specific flags to the end!

apptainer run --cleanenv --bind ${DATA}:/data:ro --bind ${APP_DERIV_DIR}:/outputs ${APP} \
/data /outputs participant --participant_label ${Subject} --ica -w ${WORK_DIR} --verbose-reports
