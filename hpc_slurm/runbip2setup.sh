#!/bin/bash

############################################################
############# BEGIN SLURM CONFIGURATION ######################

#SBATCH --partition=standard

### Walltime is clock time
### --time HH:MM:SS (below, 5 minutes)
#SBATCH --time=0:05:00

### Specify the number of nodes and cpus 
#SBATCH --nodes=1 --ntasks=1 

### Specify a name for the job
#SBATCH --job-name=runbip2setup

### Specify the group name
#SBATCH --account=dkp

### standard error and standard out are joined by default

#SBATCH --mail-type=ALL
#SBATCH --mail-user=dkp@arizona.edu

############################################################
############## BEGIN SCRIPT TO RUN #########################

# Note the following can be defined in your .bash_profile, but to be safe we'll define it here as well.
# This one should take precedence if the other is incorrect or not defined.
export SIF=/groups/dkp/shared/singularity-images

# Define variables to point to the directories you want to work in
# This is not strictly necessary, but may be helpful
# bids and derivatives directories are siblings in this scheme under a directory called MRI
export MRIS=/groups/dkp/BIDS

# Just to be safe, create the derivatives subdirectory if it does not exist.
if [ ! -d ${MRIS}/derivatives ]; then 
  mkdir -p ${MRIS}/derivatives
fi

# Just to be safe, create the fsl_dwi_proc directory if it does not exist. 
if [ ! -d ${MRIS}/derivatives/fsl_dwi_proc ]; then 
  mkdir -p ${MRIS}/derivatives/fsl_dwi_proc
fi


Subject=${sub}

singularity run --cleanenv ${SIF}/bip2.sif --bind ${MRIS}/Nifti:/data:ro ${MRIS}/derivatives/fsl_dwi_proc participant --participant_label ${Subject} --stages setup --skip_bids_validator 