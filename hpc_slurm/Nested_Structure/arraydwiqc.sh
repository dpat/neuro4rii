#!/bin/bash

############################################################
############# BEGIN PBS CONFIGURATION ######################
#PBS -q standard
#PBS -l select=1:ncpus=4:mem=6gb

### Specify the group name
#PBS -W group_list=dkp

### Used if job requires partial node only
#PBS -l place=pack:shared

### CPUtime required in hhh:mm:ss.
### Leading 0's can be omitted e.g 48:0:0 sets 48 hours
#PBS -l cput=6:00:00

### Walltime is created by cputime divided by total cores.
### This field can be overwritten by a longer time
#PBS -l walltime=3:00:00

### Joins standard error and standard out
#PBS -j oe

############################################################
#### BEGIN STUFF TO ADD TO ARRAY SCRIPT CALLED BY QSUBR ####

### Change to the directory where the PBS script was submitted. This is harmless. 
cd $PBS_O_WORKDIR

### This is critical for any call to qsubr. It gets the subject names from the list.
### Note that the subject list can be specified, but defaults to subjects.txt in the current directory if it is not specified. 
### Pull filename from line number = PBS_ARRAY_INDEX
Subject="$( sed "${PBS_ARRAY_INDEX}q;d" "${SUBJS:-subjects.txt}")"

### The following is useful for documentation purposes. 
### The array index and subject number get echoed to every output file produced by qsub.
### Print job information to each output job
loginfo="JOBNAME=$PBS_JOBNAME, JOB ID: $PBS_JOBID, Array Index: ${PBS_ARRAY_INDEX}, Subject: sub-${Subject}"

### Also create a log file for the job that echos the subject number and index of each subjob to a single log file.
echo ${loginfo} >>${PBS_JOBNAME}.log
echo ${loginfo}

#### END STUFF TO ADD TO ARRAY SCRIPT CALLED BY QSUBR ####
############################################################
############## BEGIN SCRIPT TO RUN #########################

# Define variables to point to the directories you want to work in
# This is not strictly necessary, but may be helpful
export STUFF=/groups/dkp
export MRIS=${STUFF}/Nifti

# Just to be safe, create the derivatives subdirectory if it does not exist.
if [ ! -d ${MRIS}/derivatives ]; then 
  mkdir -p ${MRIS}/derivatives
fi

# Just to be safe, create the fsl_dwi_proc directory if it does not exist. 
if [ ! -d ${MRIS}/derivatives/fsl_dwi_proc ]; then 
  mkdir -p ${MRIS}/derivatives/fsl_dwi_proc
fi

# load the singularity module
module load singularity

singularity run --cleanenv --bind ${MRIS}:/data ${SIF}/dwiqc.sif /data /data/derivatives/fsl_dwi_proc participant --participant_label ${Subject}

