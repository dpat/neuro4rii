#!/bin/bash

############################################################
############# BEGIN PBS CONFIGURATION ######################
#PBS -q standard
#PBS -l select=1:ncpus=16:mem=12gb

### Specify a name for the job
#PBS -N mrtrix3_desikan

### Specify the group name
#PBS -W group_list=dkp

### Walltime is created by cputime divided by total cores.
### This field can be overwritten by a longer time
#PBS -l walltime=16:00:00

### Joins standard error and standard out
#PBS -j oe

### The mailing options are set using the -m and -M arguments for a Non-Array job 
### The -m argument sets the conditions under which the batch server 
### will send a mail message about the job and -M will define the users 
### that emails will be sent to (multiple users can be specified in a list seperated by commas). 
### The arguments for the -m argument include:
### a: mail is sent when the job is aborted.
### b: mail is sent when the job begins.
### e: main is sent when the job ends.

#PBS -m ae
#PBS -M dkp@email.arizona.edu

############################################################
############## BEGIN SCRIPT TO RUN #########################
###    Run me on el gato ###

# Define a variable to point to the directory you want to work in
# This is not strictly necessary, but may be helpful
export STUFF=/groups/dkp/
export MRIS=${STUFF}/Nifti

# Just to be safe, create the mrtrix_desikan directory if it does not exist. 
if [ ! -d ${MRIS}/derivatives/mrtrix_desikan ]; then 
mkdir ${MRIS}/derivatives/mrtrix_desikan
fi

module load singularity

Subject=${sub}

singularity run --cleanenv --bind ${MRIS}:/bids_dataset --bind ${MRIS}/derivatives/mrtrix_desikan:/outputs ${SIF}/mrtrix3_connectome.sif /bids_dataset /outputs participant --participant_label ${Subject} --parcellation desikan --output_verbosity 3 --debug 
