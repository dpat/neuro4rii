#!/bin/bash

############################################################
############# BEGIN PBS CONFIGURATION ######################

#PBS -q standard
#PBS -l select=1:ncpus=28:mem=56gb:ngpus=1
## ElGato has 16 cpus per gpu
## PBS -l select=1:ncpus=16:mem=6gb:ngpus=1

### Specify a name for the job
#PBS -N arraybip2bip

### Specify the group name
#PBS -W group_list=dkp

### Walltime is created by cputime divided by total cores.
### This field can be overwritten by a longer time
### Roughly give 5-10 minutes for each tract run you uncomment below
#PBS -l walltime=0:15:00

### Joins standard error and standard out
#PBS -j oe

############################################################
#### BEGIN STUFF TO ADD TO ARRAY SCRIPT CALLED BY QSUBR ####

### Change to the directory where the PBS script was submitted. This is harmless. 
cd $PBS_O_WORKDIR

### This is critical for any call to qsubr. It gets the subject names from the list.
### Note that the subject list can be specified, but defaults to subjects.txt in the current directory if it is not specified. 
### Pull filename from line number = PBS_ARRAY_INDEX
Subject="$( sed "${PBS_ARRAY_INDEX}q;d" "${SUBJS:-subjects.txt}")"

### The following is useful for documentation purposes. 
### The array index and subject number get echoed to every output file produced by qsub.
### Print job information to each output job
loginfo="JOBNAME=$PBS_JOBNAME, JOB ID: $PBS_JOBID, Array Index: ${PBS_ARRAY_INDEX}, Subject: sub-${Subject}"

### Also create a log file for the job that echos the subject number and index of each subjob to a single log file.
echo ${loginfo} >>${PBS_JOBNAME}.log
echo ${loginfo}

#### END STUFF TO ADD TO ARRAY SCRIPT CALLED BY QSUBR ####
############################################################
############## BEGIN SCRIPT TO RUN #########################

# Define variables to point to the directories you want to work in
# This is not strictly necessary, but may be helpful
export STUFF=/groups/dkp
export MRIS=${STUFF}/Nifti

# Just to be safe, create the derivatives subdirectory if it does not exist.
if [ ! -d ${MRIS}/derivatives ]; then 
  mkdir -p ${MRIS}/derivatives
fi

# load the singularity module
module load singularity
module load cuda10.0
# ElGato calls the module cuda10
# module load cuda10
# --cleanenv prevents inappropriate library conflicts.  It does not seem to interfere with --nv, but it makes it possible to run on ElGato
# --nv allows flexible binding of internal and external gpu driver libraries
# Comment lines fro tracts you do not want to run, uncomment lines for tracts you do want to run
# singularity run --cleanenv --nv ${SIF}/bip2.sif ${MRIS} ${MRIS}/derivatives/bip participant --participant_label ${Subject} --stages bip --tract arc_l --gpu yes --skip_bids_validator 
# singularity run --cleanenv --nv ${SIF}/bip2.sif ${MRIS} ${MRIS}/derivatives/bip participant --participant_label ${Subject} --stages bip --tract arc_r --gpu yes --skip_bids_validator 
# singularity run --cleanenv --nv ${SIF}/bip2.sif ${MRIS} ${MRIS}/derivatives/bip participant --participant_label ${Subject} --stages bip --tract extcap_l --gpu yes --skip_bids_validator
# singularity run --cleanenv --nv ${SIF}/bip2.sif ${MRIS} ${MRIS}/derivatives/bip participant --participant_label ${Subject} --stages bip --tract extcap_r --gpu yes --skip_bids_validator
# singularity run --cleanenv --nv ${SIF}/bip2.sif ${MRIS} ${MRIS}/derivatives/bip participant --participant_label ${Subject} --stages bip --tract slf2_l --gpu yes --skip_bids_validator
# singularity run --cleanenv --nv ${SIF}/bip2.sif ${MRIS} ${MRIS}/derivatives/bip participant --participant_label ${Subject} --stages bip --tract slf2_r --gpu yes --skip_bids_validator
# singularity run --cleanenv --nv ${SIF}/bip2.sif ${MRIS} ${MRIS}/derivatives/bip participant --participant_label ${Subject} --stages bip --tract unc_l --gpu yes --skip_bids_validator
# singularity run --cleanenv --nv ${SIF}/bip2.sif ${MRIS} ${MRIS}/derivatives/bip participant --participant_label ${Subject} --stages bip --tract unc_r --gpu yes --skip_bids_validator
# singularity run --cleanenv --nv ${SIF}/bip2.sif ${MRIS} ${MRIS}/derivatives/bip participant --participant_label ${Subject} --stages bip --tract b3tob3_ih --gpu yes --skip_bids_validator
# singularity run --cleanenv --nv ${SIF}/bip2.sif ${MRIS} ${MRIS}/derivatives/bip participant --participant_label ${Subject} --stages bip --tract w5tow5_ih --gpu yes --skip_bids_validator
# singularity run --cleanenv --nv ${SIF}/bip2.sif ${MRIS} ${MRIS}/derivatives/bip participant --participant_label ${Subject} --stages bip --tract ilf_l --gpu yes --skip_bids_validator 
# singularity run --cleanenv --nv ${SIF}/bip2.sif ${MRIS} ${MRIS}/derivatives/bip participant --participant_label ${Subject} --stages bip --tract ilf_r --gpu yes --skip_bids_validator 
# singularity run --cleanenv --nv ${SIF}/bip2.sif ${MRIS} ${MRIS}/derivatives/bip participant --participant_label ${Subject} --stages bip --tract mdlf_l --gpu yes --skip_bids_validator
# singularity run --cleanenv --nv ${SIF}/bip2.sif ${MRIS} ${MRIS}/derivatives/bip participant --participant_label ${Subject} --stages bip --tract mdlf_r --gpu yes --skip_bids_validator
singularity run --cleanenv --nv ${SIF}/bip2.sif ${MRIS} ${MRIS}/derivatives/bip participant --participant_label ${Subject} --stages bip --tract cst_l --gpu yes --skip_bids_validator
singularity run --cleanenv --nv ${SIF}/bip2.sif ${MRIS} ${MRIS}/derivatives/bip participant --participant_label ${Subject} --stages bip --tract cst_r --gpu yes --skip_bids_validator
