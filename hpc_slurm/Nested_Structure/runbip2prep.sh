#!/bin/bash

############################################################
############# BEGIN PBS CONFIGURATION ######################

#PBS -q standard
#PBS -l select=1:ncpus=28:mem=6gb:ngpus=1
## ElGato
## PBS -l select=1:ncpus=16:mem=6gb:ngpus=1

### Specify a name for the job
#PBS -N runbip2prep

### Specify the group name
#PBS -W group_list=dkp

### Used if job requires partial node only
#PBS -l place=pack:shared

### Walltime is created by cputime divided by total cores.
### This field can be overwritten by a longer time
### If you need to run eddy, you need more time, like 3 hours:
###PBS -l walltime=03:00:00
### If we are just running bedpost, because dwiqc was completed first, we don't need as much time: 5-10 minutes is plenty
#PBS -l walltime=00:10:00

### Joins standard error and standard out
#PBS -j oe

### The mailing options are set using the -m and -M arguments for a Non-Array job 
### The -m argument sets the conditions under which the batch server 
### will send a mail message about the job and -M will define the users 
### that emails will be sent to (multiple users can be specified in a list seperated by commas). 
### The arguments for the -m argument include:
### a: mail is sent when the job is aborted.
### b: mail is sent when the job begins.
### e: main is sent when the job ends.

#PBS -m ae
#PBS -M dkp@email.arizona.edu

############################################################
############## BEGIN SCRIPT TO RUN #########################

# Define variables to point to the directories you want to work in
# This is not strictly necessary, but may be helpful
export STUFF=/groups/dkp
export MRIS=${STUFF}/Nifti

# Just to be safe, create the derivatives subdirectory if it does not exist.
if [ ! -d ${MRIS}/derivatives ]; then 
  mkdir -p ${MRIS}/derivatives
fi

# Just to be safe, create the fsl_dwi_proc directory if it does not exist. 
if [ ! -d ${MRIS}/derivatives/fsl_dwi_proc ]; then 
  mkdir -p ${MRIS}/derivatives/fsl_dwi_proc
fi

module load singularity
module load cuda10.0
# ElGato calls the module cuda10 instead
# module load cuda10

Subject=${sub}

# --cleanenv prevents inappropriate library conflicts.  It does not seem to interfere with --nv, but it makes it possible to run on ElGato
# --nv allows flexible binding of internal and external gpu driver libraries
singularity run --cleanenv --nv ${SIF}/bip2.sif ${MRIS} ${MRIS}/derivatives/fsl_dwi_proc participant --participant_label ${Subject} --stages prep --gpu yes --skip_bids_validator
