#!/bin/bash

############################################################
############# BEGIN PBS CONFIGURATION ######################

#PBS -q standard
## Ocelote-only
#PBS -l select=1:ncpus=28:mem=56gb:ngpus=1
## ElGato
## PBS -l select=1:ncpus=16:mem=6gb:ngpus=1

### Specify a name for the job
#PBS -N runbip2bip_1

### Specify the group name
#PBS -W group_list=dkp

### Walltime is created by cputime divided by total cores.
### This field can be overwritten by a longer time
#PBS -l walltime=0:15:00

### Joins standard error and standard out
#PBS -j oe

### The mailing options are set using the -m and -M arguments for a Non-Array job 
### The -m argument sets the conditions under which the batch server 
### will send a mail message about the job and -M will define the users 
### that emails will be sent to (multiple users can be specified in a list seperated by commas). 
### The arguments for the -m argument include:
### a: mail is sent when the job is aborted.
### b: mail is sent when the job begins.
### e: main is sent when the job ends.

#PBS -m ae
#PBS -M dkp@email.arizona.edu

############################################################
############## BEGIN SCRIPT TO RUN #########################

# Define variables to point to the directories you want to work in
# This is not strictly necessary, but may be helpful
export STUFF=/groups/dkp
export MRIS=${STUFF}/Nifti

module load singularity
module load cuda10.0
# ElGato calls the module cuda10 instead
# module load cuda10

Subject=${sub}
tracta=${tract}

# --cleanenv prevents inappropriate library conflicts.  It does not seem to interfere with --nv, but it makes it possible to run on ElGato
# --nv allows flexible binding of internal and external gpu driver libraries
singularity run --cleanenv --nv ${SIF}/bip2.sif ${MRIS} ${MRIS}/derivatives/bip participant --participant_label ${Subject} --stages bip --tract ${tracta} --gpu yes --skip_bids_validator 
