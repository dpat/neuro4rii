#!/bin/bash

############################################################
############# BEGIN PBS CONFIGURATION ######################

#PBS -q standard
###Ocelote request for os7 nodes
###PBS -l select=1:ncpus=4:mem=6gb:os7=True
###ElGato version
#PBS -l select=1:ncpus=4:mem=6gb
### Specify a name for the job
#PBS -N dwiqcPB

### Specify the group name
#PBS -W group_list=dkp

### Used if job requires partial node only
#PBS -l place=pack:shared

### CPUtime required in hhh:mm:ss.
### Leading 0's can be omitted e.g 48:0:0 sets 48 hours
#PBS -l cput=6:00:00

### Walltime is created by cputime divided by total cores.
### This field can be overwritten by a longer time
#PBS -l walltime=3:00:00

### Joins standard error and standard out
#PBS -j oe

### The mailing options are set using the -m and -M arguments for a Non-Array job 
### The -m argument sets the conditions under which the batch server 
### will send a mail message about the job and -M will define the users 
### that emails will be sent to (multiple users can be specified in a list seperated by commas). 
### The arguments for the -m argument include:
### a: mail is sent when the job is aborted.
### b: mail is sent when the job begins.
### e: main is sent when the job ends.

#PBS -m ae
#PBS -M dkp@email.arizona.edu

############################################################
############## BEGIN SCRIPT TO RUN #########################

# Define variables to point to the directories you want to work in
# This is not strictly necessary, but may be helpful
export STUFF=/groups/dkp
export MRIS=${STUFF}/Nifti

# Just to be safe, create the derivatives subdirectory if it does not exist.
if [ ! -d ${MRIS}/derivatives ]; then 
  mkdir -p ${MRIS}/derivatives
fi

# Just to be safe, create the fsl_dwi_proc directory if it does not exist. 
if [ ! -d ${MRIS}/derivatives/fsl_dwi_proc ]; then 
  mkdir -p ${MRIS}/derivatives/fsl_dwi_proc
fi

# Just to be safe, create the fsl_anat_proc directory if it not exist. 
if [ ! -d ${MRIS}/derivatives/fsl_anat_proc ]; then 
  mkdir -p ${MRIS}/derivatives/fsl_anat_proc
fi

# load the singularity module
module load singularity

Subject=${sub}

singularity run --cleanenv --bind ${MRIS}:/data ${SIF}/dwiqc.sif /data /data/derivatives/fsl_dwi_proc participant --participant_label ${Subject} 

