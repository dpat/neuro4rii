#!/bin/bash

############################################################
############# BEGIN SLURM CONFIGURATION ######################

#SBATCH --partition=standard

### Walltime is clock time
### --time HH:MM:SS (below, 40 minutes)
#SBATCH --time=0:40:00

### Specify the number of nodes, cpus and memory
#SBATCH --nodes=1 --ntasks=10 

### Specify a name for the job
#SBATCH --job-name=runmriqc_dev

### Specify the group name
#SBATCH --account=dkp

### standard error and standard out are joined by default

#SBATCH --mail-type=ALL
#SBATCH --mail-user=dkp@arizona.edu

############################################################
############## BEGIN SCRIPT TO RUN #########################

# Run this script from the MRIS directory so that logs will be created there.

# Note the following can be defined in your .bash_profile, but to be safe we'll define it here as well.
# This one should take precedence if the other is incorrect or not defined.
export SIF=/contrib/singularity/shared/neuroimaging

# Which container do you want to run
export APP=${SIF}/mriqc_v23.1.0.sif


# Define variables to point to the directories you want to work in
# bids and derivatives directories are siblings in this scheme under a directory referred to as MRIS
export MRIS=/groups/dkp/Batch/CAM003_bids_data
export DATA=${MRIS}/data

# Define the output directories used by this app:
export APP_DERIV_DIR=${MRIS}/derivatives/mriqc_dev
export WORK_DIR=${MRIS}/mriqc_work_dev

# Just to be safe, create the derivatives subdirectory if it does not exist.
if [ ! -d ${APP_DERIV_DIR} ]; then 
    mkdir -p ${APP_DERIV_DIR}
fi


# Just to be safe, create the work directory if it does not exist. 
# This should be outside the bids input dir.
if [ ! -d ${WORK_DIR} ]; then 
    mkdir ${WORK_DIR}
fi


Subject=${sub}

# As of mriqc 23.1.0, 
# The --ica flag no longer works because melodic has been removed and 
# -m modality must be included as they work toward a dwi check 
# (if there is a dwi directory in the dataset, 23.1.0 crashes without the -m flag and its specified modalities)

# Because the directories and the app to run have been defined as variables above, 
# there is no need to alter the apptainer command, unless you want to revise app-specific flags to the end!

apptainer run --cleanenv --bind ${DATA}:/data:ro --bind ${APP_DERIV_DIR}:/outputs ${APP} \
/data /outputs participant --participant_label ${Subject} -m T1w T2w bold -w ${WORK_DIR} --verbose-reports


