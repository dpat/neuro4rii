#!/bin/bash

############################################################
############# BEGIN SLURM CONFIGURATION ######################

#SBATCH --partition=standard

### Walltime is clock time
### --time HH:MM:SS (below, 5 minutes)
#SBATCH --time=0:05:00

### Specify the number of nodes, cpus and memory
#SBATCH --nodes=1 --ntasks=1 

### Specify a name for the job
#SBATCH --job-name=runbet

### Specify the group name
#SBATCH --account=dkp

### standard error and standard out are joined by default

#SBATCH --mail-type=ALL
#SBATCH --mail-user=dkp@arizona.edu

############################################################
############## BEGIN SCRIPT TO RUN #########################

# Note the following can be defined in your .bash_profile, but to be safe we'll define it here as well.
# This one should take precedence if the other is incorrect or not defined.
export SIF=/contrib/singularity/shared/neuroimaging

# Define variables to point to the directories you want to work in
# This is not strictly necessary, but may be helpful
# bids and derivatives directories are siblings in this scheme unde a directory called MRI
export MRIS=/groups/dkp/BIDS

# Just to be safe, create the derivatives subdirectory if it does not exist.
if [ ! -d ${MRIS}/derivatives ]; then 
    mkdir ${MRIS}/derivatives
fi

Subject=${sub}

singularity run --cleanenv --bind ${MRIS}/Nifti:/data:ro --bind ${MRIS}/derivatives:/outputs ${SIF}/bet.sif  /data /outputs participant --participant_label ${Subject}
