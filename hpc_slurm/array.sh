#!/bin/bash

############################################################
############# BEGIN SBATCH CONFIGURATION ######################

#SBATCH --partition=standard

### Walltime is cputime divided by total cores.
### --time HH:MM:SS (below, 1 minute)
#SBATCH --time=0:01:00

### Specify the number of nodes, cpus and memory
#SBATCH --nodes=1 --ntasks=1 

### Specify a name for the job
#SBATCH --job-name=array

### Specify the group name
#SBATCH --account=dkp

### standard error and standard out are joined by default

############################################################
#### BEGIN STUFF TO ADD TO ARRAY SCRIPT CALLED BY SBATCHR ####

### SLURM can specify an informative name for the output: Jobname-Jobnumber.out 
#SBATCH --output=%x-%a.out

### SLURM Inherits your environment. cd $SLURM_SUBMIT_DIR not needed

### This is critical for any call to sbatchr. It gets the subject names from the list.
### Note that the subject list can be specified, but defaults to subjects.txt in the current directory if it is not specified. 
### Pull filename from line number = SLURM_ARRAY_TASK_ID
Subject="$( sed "${SLURM_ARRAY_TASK_ID}q;d" "${SUBJS:-subjects.txt}")"

### The following is useful for documentation purposes. 
### The array index and subject number get echoed to every output file produced by sbatch.
### Print job information to each output job
pwd; hostname; date
loginfo="JOBNAME=$SLURM_JOB_NAME, JOB ID: $SLURM_JOB_ID, Array Index: ${SLURM_ARRAY_TASK_ID}, Subject: sub-${Subject}"

### Also create a log file for the job that echos the subject number and index of each subject job to a single log file.
echo ${loginfo} >>${SLURM_JOB_NAME}.log
echo ${loginfo}

#### END STUFF TO ADD TO ARRAY SCRIPT CALLED BY SBATCHR ####
############################################################
############## BEGIN SCRIPT TO RUN #########################

touch sub-${Subject}

