#!/bin/bash


############################################################
############# BEGIN SLURM CONFIGURATION ######################

#SBATCH --partition=standard

### Walltime is clock time
### --time HH:MM:SS (below, 7 hours)
#SBATCH --time=7:00:00

### Specify the number of nodes, cpus and memory
#SBATCH --nodes=1 --ntasks=16

### Specify a name for the job
#SBATCH --job-name=runqsirecon
### Specify the group name
#SBATCH --account=dkp

### standard error and standard out are joined by default

#SBATCH --mail-type=ALL
#SBATCH --mail-user=dkp@arizona.edu


############################################################
############## BEGIN SCRIPT TO RUN #########################

# Run this script from the MRIS directory so that logs will be created there.

# Note the following can be defined in your .bash_profile, but to be safe we'll define it here as well.
# This one one you've defined in your bash profile take precedence, but if not found, the standard neuroimaging 
# shared dir is used.
export SIF=${SIF:-/contrib/singularity/shared/neuroimaging}

# Which container do you want to run
export APP=${SIF}/qsiprep_v0.19.0.sif

# Define variables to point to the directories you want to work in
# bids and derivatives directories are siblings in this scheme under a directory referred to as MRIS
export MRIS=/groups/dkp/Batch/CAM003_bids_data
export DATA=${MRIS}/data

# Define the output directories used by this app:
# The qsirecon dir will be created under this dir:
export APP_DERIV_DIR=${MRIS}/derivatives
# This is the location of the qsiprep output
export APP_PREP_DERIV_DIR=${MRIS}/derivatives/qsiprep
# If you've run fmriprep, you may have a freesurfer directory. 
# qsirecon can use that.
export FREESURFER_DIR=${MRIS}/derivatives/fmriprep_dev/sourcedata/freesurfer
# define a unique work dir to separate recon from prep work
export WORK_DIR=${MRIS}/qsirecon_work

# See alternative recon specs at 
# https://qsiprep.readthedocs.io/en/latest/reconstruction.html#anatomical-data-for-reconstruction-workflows
export RECON_SPEC=mrtrix_multishell_msmt_ACT-hsvs

# Just to be safe, create the derivatives subdirectory if it does not exist.
if [ ! -d ${APP_DERIV_DIR} ]; then 
    mkdir -p ${APP_DERIV_DIR}
fi

# Just to be safe, create the work directory if it does not exist. 
# This should be outside the bids input dir.
if [ ! -d ${WORK_DIR} ]; then 
    mkdir ${WORK_DIR}
fi


##############


# For an array job, remove the following line to avoid conflicting definitions of subject
Subject=${sub}

# Use --cleanenv to prevent problems with singularity grabbing variables, libraries etc.
# from your HPC directories.  You want the environment to be defined
# by what is inside the container.
# --bind just like docker, 
# 1) use /data defined inside the container.
# 2) the derivatives created by qsiprep 
# 3) the main derivatives directory
# 4) the work directory
# The recon-input is the derivatives created by qsiprep
# The recon spec can vary, there are a dozen or so canned 
# options described on the read-the-docs site.  I'm using the 
# most recommended one that required both distortion correction (RPE images)
# in the preceding step and freesurfer output.  This should be an excellent segmentation.
# Due to ongoing issues with crashing on HPC machines (some issue with DiPy Fury), use 
# --skip-odf-reports
# Having already done the preprocessing, all we need is reconstruction, hence
# --recon-only
# Output resolution is 1.3 as per their suggestions
# If you ran freesurfer when you did fmriprep, then you can now specify those freesurfer results to be used with qsirecon
# Ensure you identify the correct path to /sourcedata/freesurfer


singularity run --cleanenv \
--bind ${DATA}:/data:ro \
--bind ${APP_PREP_DERIV_DIR}:/qsiprep-output:ro \
--bind ${APP_DERIV_DIR}:/out \
--bind ${WORK_DIR}:/work \
${APP} /data /out participant \
--participant_label ${Subject} \
--recon-input /qsiprep-output \
--recon-spec ${RECON_SPEC} \
--stop-on-first-crash \
--output-resolution 1.3 -w /work -v -v  \
--n-cpus 16 --omp-nthreads 15 \
--skip-odf-reports --recon-only \
--fs-license-file ${HOME}/license.txt \
--freesurfer_input ${FREESURFER_DIR}
