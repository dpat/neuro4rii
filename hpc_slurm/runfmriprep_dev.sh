#!/bin/bash


############################################################
############# BEGIN SLURM CONFIGURATION ######################

##### NOte that SBATCH commands require an initial hash mark!  Do not remove it! ######

#SBATCH --partition=standard

### Walltime is clock time
### --time HH:MM:SS (below, usually 7 hours)
### This takes about 1 hour if the freesurfer directory already exists!)
#SBATCH --time=7:00:00

### Specify the number of nodes and cpus (ntasks).  Memory will default to the max for the cluster.
#SBATCH --nodes=1 --ntasks=16 

### Specify a name for the job
#SBATCH --job-name=runfmriprep_dev

### Specify the group name
#SBATCH --account=dkp

### standard error and standard out are joined by default

#SBATCH --mail-type=ALL
#SBATCH --mail-user=dkp@arizona.edu


############################################################
############## BEGIN SCRIPT TO RUN #########################

# Run this script from the MRIS directory so that logs will be created there.

# Note the following can be defined in your .bash_profile, but to be safe we'll define it here as well.
# This one should take precedence if the other is incorrect or not defined.
export SIF=/contrib/singularity/shared/neuroimaging

# Which container do you want to run
export APP=${SIF}/fmriprep_v23.1.4.sif

# Define variables to point to the directories you want to work in
# bids and derivatives directories are siblings in this scheme under a directory referred to as MRIS
export MRIS=/groups/dkp/Batch/CAM003_bids_data
export DATA=${MRIS}/data

# Define the output directories used by this app:
export APP_DERIV_DIR=${MRIS}/derivatives/fmriprep_dev
export WORK_DIR=${MRIS}/fmriprep_work_dev

# Just to be safe, create the derivatives subdirectory if it does not exist.
if [ ! -d ${APP_DERIV_DIR} ]; then 
    mkdir -p ${APP_DERIV_DIR}
fi


# Just to be safe, create the work directory if it does not exist. 
# This should be outside the bids input dir.
if [ ! -d ${WORK_DIR} ]; then 
    mkdir ${WORK_DIR}
fi


Subject=${sub} 

# Use --cleanenv to prevent problems with singularity grabbing variables, libraries etc.
# from your HPC directories.  You want the environment to be defined
# by what is inside the container.
# --bind just like docker, we want to use /data defined inside the container.
# We need to bind the directory on the HPC (e.g., ${MYSTUFF}) to /data:
# We specify the name of the singularity container to run.

# The rest should look like the docker command.  See https://fmriprep.readthedocs.io/en/stable/
# --fs-license-file Is used to point to the freesurfer license
# --ignore slicetiming is a good idea if you have multiband data (and has saved us from crashes)
# --cifti-output provides cifti files, probably the right thing to do as we move towards surface processing streams instead of volumetric streams. 
# cifti requires the following template output space: MNI152NLin2009cAsym
# I have added --stop-on-first-crash as suggested by Saren...this prevents the process from lingering and using CPU time when it has already had some fatal problem
# output spaces: cifti data in native space can overlay on the T1w GIFTIs that are produced
# output spaces fsaverage and MNI152NLin6Asym:res-2 are preferred by CONN

# Because the directories and the app to run have been defined as variables above, 
# there is no need to alter the apptainer command, unless you want to revise app-specific flags to the end!

apptainer run --cleanenv --bind ${MRIS}/data:/data:ro  --bind ${APP_DERIV_DIR}:/outputs \
--bind ${WORK_DIR}:/work ${APP} /data /outputs participant --participant_label ${Subject} \
--fs-license-file ${HOME}/license.txt -w /work  --stop-on-first-crash --ignore slicetiming \
--cifti-output --output-spaces fsnative fsaverage MNI152NLin6Asym:res-2

