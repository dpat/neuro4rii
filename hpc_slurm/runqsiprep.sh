#!/bin/bash


############################################################
############# BEGIN SLURM CONFIGURATION ######################

#SBATCH --partition=standard

### Walltime is clock time
### --time HH:MM:SS (below, 4 hours)
#SBATCH --time=4:00:00

### Specify the number of nodes, cpus and memory
#SBATCH --nodes=1 --ntasks=16

### Specify a name for the job
#SBATCH --job-name=runqsiprep

### Specify the group name
#SBATCH --account=dkp

### standard error and standard out are joined by default

#SBATCH --mail-type=ALL
#SBATCH --mail-user=dkp@arizona.edu


############################################################
############## BEGIN SCRIPT TO RUN #########################

# Run this script from the MRIS directory so that logs will be created there.

# Note the following can be defined in your .bash_profile, but to be safe we'll define it here as well.
# This one should take precedence if the other is incorrect or not defined.
export SIF=${SIF:-/contrib/singularity/shared/neuroimaging}

# Which container do you want to run
export APP=${SIF}/qsiprep_v0.19.0.sif

# Define variables to point to the directories you want to work in
# bids and derivatives directories are siblings in this scheme under a directory referred to as MRIS
export MRIS=/groups/dkp/Batch/CAM003_bids_data
export DATA=${MRIS}/data

# Define the output directories used by this app:
# qsiprep will create a separate subdirectory under derivatives
export APP_DERIV_DIR=${MRIS}/derivatives
export WORK_DIR=${MRIS}/qsiprep_work

# Just to be safe, create the derivatives subdirectory if it does not exist.
if [ ! -d ${APP_DERIV_DIR} ]; then 
    mkdir -p ${APP_DERIV_DIR}
fi

# Just to be safe, create the work directory if it does not exist. 
# This should be outside the bids input dir.
if [ ! -d ${WORK_DIR} ]; then 
    mkdir ${WORK_DIR}
fi

# For an array job, remove the following line to avoid conflicting definitions of subject
Subject=${sub}

# Use --cleanenv to prevent problems with singularity grabbing variables, libraries etc.
# from your HPC directories.  You want the environment to be defined
# by what is inside the container.
# --bind just like docker, we want to use /data defined inside the container.
# We need to bind the directory on the HPC (e.g., ${MYSTUFF}) to /data:
# We specify the name of the singularity container to run.
# We need to bind three directories 

singularity run --cleanenv --bind ${DATA}:/data:ro  --bind ${APP_DERIV_DIR}:/out  \
--bind ${WORK_DIR}:/work ${APP} /data /out participant --participant_label \
${Subject} --fs-license-file ${HOME}/license.txt --output-resolution 1.3 --n_cpus 16 -w /work -v -v

