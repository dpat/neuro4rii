#!/bin/bash

############################################################
############# BEGIN SLURM CONFIGURATION ######################

#SBATCH --partition=standard

### Walltime is clock time
### --time HH:MM:SS (below, 2 hours)
#SBATCH --time=2:00:00

### Specify the number of nodes, cpus 
### For 32 dir 2x2x2 single shell: 16 cpus=44 minutes walltime instead of 62 minutes with 8 cpus
#SBATCH --nodes=1 --ntasks=8

### Specify a name for the job
#SBATCH --job-name=mrtrix3_preproc

### Specify the group name
#SBATCH --account=dkp

### standard error and standard out are joined by default

#SBATCH --mail-type=ALL
#SBATCH --mail-user=dkp@arizona.edu

############################################################
############## BEGIN SCRIPT TO RUN #########################
###    Run me on el gato ###

# Run this script from the MRIS directory so that logs will be created there.

# Note the following can be defined in your .bash_profile, but to be safe we'll define it here as well.
# This one should take precedence if the other is incorrect or not defined.
export SIF=/contrib/singularity/shared/neuroimaging

# Define variables to point to the directories you want to work in
# This is not strictly necessary, but may be helpful
# bids and derivatives directories are siblings in this scheme unde a directory called MRI
export MRIS=/groups/dkp/BIDS

# Just to be safe, create the derivatives subdirectory if it does not exist.
if [ ! -d ${MRIS}/derivatives ]; then 
    mkdir ${MRIS}/derivatives
fi


Subject=${sub}

# This seems to run
singularity run --cleanenv --bind ${MRIS}/Nifti:/data:ro --bind ${MRIS}/derivatives:/outputs ${SIF}/mrtrix3_connectome.sif /data /outputs preproc --participant_label ${Subject} --output_verbosity 4