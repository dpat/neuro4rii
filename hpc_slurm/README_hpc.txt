Description: Scripts created for running neuroimaging Singularity containers on the University of Arizona HPC.
Date: Oct 16, 2021
Author: Dianne Patterson, Ph.D.
TO_DO: recheck and confirm all instructions here actually work!!

scripts: These are example scripts. They should be modified by anyone copying them to point to appropriate directories and to charge the correct user group.

run* scripts: Scripts intended to be called by sbatch
array* scripts: Scripts intended to be called by sbatchr and used with a list of subjects to process.
array scripts are easier on the HPC resources than for loops.  CPU, memory and other resources need only be specified for a single run, then those resources are allocated for each subject.
Once you have determined what changes need to be made using a run script, you can try making the same modifications to the appropriate array script.  This should allow you to run lots of subjects at once.

Example run commands:

sbatch --export sub=219 runmrtrix3_participant_none.sh

Example array commands:

sbatchr Scripts/arraybip2setup.sh SubjectLists/plante111-334.subjects
sbatchr Scripts/arraydwiqc.sh SubjectLists/plante253-334.subjects
