#!/bin/bash

############################################################
############# BEGIN PBS CONFIGURATION ######################

#PBS -q standard
#PBS -l select=1:ncpus=16:mem=96gb

### Specify a name for the job
#PBS -N mrtrix3_v0.5_participant_none

### Specify the group name
#PBS -W group_list=dkp

### Walltime is created by cputime divided by total cores.
### This field can be overwritten by a longer time
#PBS -l walltime=20:00:00
#PBS -l place=free:shared

### Joins standard error and standard out
#PBS -j oe

### The mailing options are set using the -m and -M arguments for a Non-Array job 
### The -m argument sets the conditions under which the batch server 
### will send a mail message about the job and -M will define the users 
### that emails will be sent to (multiple users can be specified in a list seperated by commas). 
### The arguments for the -m argument include:
### a: mail is sent when the job is aborted.
### b: mail is sent when the job begins.
### e: main is sent when the job ends.

#PBS -m ae
#PBS -M dkp@email.arizona.edu

############################################################
############## BEGIN SCRIPT TO RUN #########################
###    Run me on el gato ###

# Run this script from the MRIS directory so that logs will be created there.

# Note the following can be defined in your .bash_profile, but to be safe we'll define it here as well.
# This one should take precedence if the other is incorrect or not defined.
export SIF=/groups/dkp/shared/singularity-images

# Define variables to point to the directories you want to work in
# This is not strictly necessary, but may be helpful
# bids and derivatives directories are siblings in this scheme unde a directory called MRI
export MRIS=/groups/dkp/MRI

# Just to be safe, create the derivatives subdirectory if it does not exist.
if [ ! -d ${MRIS}/derivatives ]; then 
    mkdir ${MRIS}/derivatives
fi

# Just to be safe, create the scratch directory if it does not exist. 
if [ ! -d ${MRIS}/mrtrix_scratch]; then 
mkdir ${MRIS}/mrtrix_scratch
fi

module load singularity

Subject=${sub}

singularity run --cleanenv --bind ${MRIS}/Nifti:/data:ro --bind ${MRIS}/derivatives:/outputs ${SIF}/mrtrix3_connectome.sif /data /outputs participant --participant_label ${Subject} --scratch ${MRIS}/mrtrix_scratch --parcellation none

# Nov 25, 2020: output level 3 is not generating all the correct files, waiting for some code revisions and fresh Docker container
#singularity run --cleanenv --bind ${MRIS}/Nifti:/data:ro --bind ${MRIS}/derivatives:/outputs ${SIF}/mrtrix3_connectome.sif /data /outputs participant --participant_label ${Subject} --scratch ${MRIS}/mrtrix_scratch --parcellation hcpmmp1 --output_verbosity 3 