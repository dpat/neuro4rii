#!/bin/bash

############################################################
############# BEGIN PBS CONFIGURATION ######################

#PBS -q standard

### Walltime is cputime divided by total cores.
### --time HH:MM:SS (below, 1 minute)
#PBS -l walltime=0:01:00

### Specify the number of nodes, cpus and memory
#PBS -l select=1:ncpus=1:mem=1gb

### Specify a name for the job
#PBS -N array_pbs

### Specify the group name
#PBS -W group_list=dkp

### Joins standard error and standard out
#PBS -j oe

############################################################
#### BEGIN STUFF TO ADD TO ARRAY SCRIPT CALLED BY QSUBR ####

### Change to the directory where the PBS script was submitted. This is harmless. 
cd $PBS_O_WORKDIR

### This is critical for any call to qsubr. It gets the subject names from the list.
### Note that the subject list can be specified, but defaults to subjects.txt in the current directory if it is not specified. 
### Pull filename from line number = PBS_ARRAY_INDEX
Subject="$( sed "${PBS_ARRAY_INDEX}q;d" "${SUBJS:-subjects.txt}")"

### The following is useful for documentation purposes. 
### The array index and subject number get echoed to every output file produced by qsub.
### Print job information to each output job
pwd;hostname;date
loginfo="JOBNAME=$PBS_JOBNAME, JOB ID: $PBS_JOBID, Array Index: ${PBS_ARRAY_INDEX}, Subject: sub-${Subject}"

### Also create a log file for the job that echos the subject number and index of each subject job to a single log file.
echo ${loginfo} >>${PBS_JOBNAME}.log
echo ${loginfo}

#### END STUFF TO ADD TO ARRAY SCRIPT CALLED BY QSUBR ####
############################################################
############## BEGIN SCRIPT TO RUN #########################

touch sub-${Subject}

