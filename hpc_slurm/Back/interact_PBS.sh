#!/bin/bash

: <<COMMENTBLOCK

-I # puts us in interactive mode
-N interact # name the job interact
-W provide the group to be charged
-q # standard or windfall job queue. Use windfall when you might run out of standard.
CPUtime required in hhh:mm:ss. Leading 0's can be omitted e.g 48:0:0 sets 48 hours
-l cput=4:00:00
Walltime is created by cputime divided by total cores.
-l walltime=1:00:00
ncpus # number of cpus
mem  # requested memory. Up to 6 gb for each CPU.
Joins standard error and standard out
#PBS -j oe

COMMENTBLOCK


if [ $# -lt 1 ]; then
    echo "this script will put you in interactive mode with 4 cpus,"
    echo "24 gb of ram and one hour of walltime"
    echo "You must enter the group to be charged"
    echo "e.g., interact.sh dkp"
    echo "============================="
    echo "These are the groups you belong to"
    va
    exit
fi

group=$1

echo "starting interactive session with 8 cpus, 24 gb of ram and 1 hour of wall time"
qsub -I -N interact -W group_list=${group} -q standard -l select=1:ncpus=8:mem=24gb -l walltime=1:00:00 -j oe
