#!/bin/bash

############################################################
############# BEGIN SLURM CONFIGURATION ######################

#SBATCH --partition=standard

### Walltime is clock time
### --time HH:MM:SS (below, 1 minute)
#SBATCH --time=0:01:00

### Specify the number of nodes, cpus and memory
#SBATCH --nodes=1 --ntasks=1 

### Specify a name for the job
#SBATCH --job-name=runcow

### Specify the group name
#SBATCH --account=dkp

### standard error and standard out are joined by default

#SBATCH --mail-type=ALL
#SBATCH --mail-user=dkp@arizona.edu

############################################################
############## BEGIN SCRIPT TO RUN #########################


singularity run ${SIF}/lolcow.sif