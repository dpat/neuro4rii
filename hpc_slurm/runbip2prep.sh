#!/bin/bash

############################################################
############# BEGIN SLURM CONFIGURATION ######################

#SBATCH --partition=standard

### Walltime is clock time
### --time HH:MM:SS (below, 3 hours)
#SBATCH --time=3:00:00

#########
# Choose a configuration based on which cluster you run on: 
# Instead of ##SBATCH, uncomment the appropriate line to get #SBATCH

### Specify the number of nodes and cpus (Ocelote has 28 cpus per GPU)
##SBATCH --nodes=1 --ntasks=28 --gres=gpu:1 

### Specify the number of nodes and cpus (ElGato has 16 cpus per GPU)
##SBATCH --nodes=1 --ntasks=16 --gres=gpu:1 

### Specify the number of nodes and cpus (Puma has 94 cpus per GPU)
##SBATCH --nodes=1 --ntasks=94 --gres=gpu:1 

############

### Specify a name for the job
#SBATCH --job-name=runbip2prep

### Specify the group name
#SBATCH --account=dkp

### standard error and standard out are joined by default

#SBATCH --mail-type=ALL
#SBATCH --mail-user=dkp@arizona.edu

############################################################
############## BEGIN SCRIPT TO RUN #########################

# Note the following can be defined in your .bash_profile, but to be safe we'll define it here as well.
# This one should take precedence if the other is incorrect or not defined.
export SIF=/groups/dkp/shared/singularity-images

# Define variables to point to the directories you want to work in
# This is not strictly necessary, but may be helpful
# bids and derivatives directories are siblings in this scheme under a directory called MRI
export MRIS=/groups/dkp/BIDS


Subject=${sub}

# --cleanenv prevents inappropriate library conflicts.  It does not seem to interfere with --nv, but it makes it possible to run on ElGato
# --nv allows flexible binding of internal and external gpu driver libraries
singularity run --cleanenv --nv ${SIF}/bip2.sif --bind ${MRIS}/Nifti:/data:ro ${MRIS}/derivatives/fsl_dwi_proc participant --participant_label ${Subject} --stages prep --gpu yes --skip_bids_validator
