#!/bin/bash


############################################################
############# BEGIN SLURM CONFIGURATION ######################

#SBATCH --partition=standard

### Walltime is clock time
### --time HH:MM:SS (below, 2 hours)
#SBATCH --time=2:00:00

### Specify the number of nodes, cpus and memory
#SBATCH --nodes=1 --ntasks=16

### Specify a name for the job
#SBATCH --job-name=runqsiprep_nofs

### Specify the group name
#SBATCH --account=dkp

### standard error and standard out are joined by default

#SBATCH --mail-type=ALL
#SBATCH --mail-user=dkp@arizona.edu


############################################################
############## BEGIN SCRIPT TO RUN #########################

# Run this script from the MRIS directory so that logs will be created there.

# Note the following can be defined in your .bash_profile, but to be safe we'll define it here as well.
# This one should take precedence if the other is incorrect or not defined.
export SIF=/contrib/singularity/shared/neuroimaging

# Define variables to point to the directories you want to work in
# This is not strictly necessary, but may be helpful
# bids and derivatives directories are siblings in this scheme unde a directory called MRI
export MRIS=/groups/dkp/BIDS

# Just to be safe, create the derivatives subdirectory if it does not exist.
if [ ! -d ${MRIS}/derivatives ]; then 
    mkdir ${MRIS}/derivatives
fi

# Just to be safe, create the work directory if it does not exist. 
if [ ! -d ${MRIS}/qsi_work ]; then 
mkdir ${MRIS}/qsi_work
fi

Subject=${sub}

# Use --cleanenv to prevent problems with singularity grabbing variables, libraries etc.
# from your HPC directories.  You want the environment to be defined
# by what is inside the container.
# --bind just like docker, we want to use /data defined inside the container.
# We need to bind the directory on the HPC (e.g., ${MYSTUFF}) to /data:
# We specify the name of the singularity container to run.
# We need to bind three directories, data, derivatives, and work.  Without work, the reports won't be generated. 


singularity run --cleanenv --bind ${MRIS}/Nifti:/data:ro  --bind ${MRIS}/derivatives/nofs:/out  --bind ${MRIS}/qsi_work_nofs:/work ${SIF}/qsiprep.sif /data /out participant --participant_label ${Subject} --fs-license-file ${HOME}/license.txt --output-resolution 1.3 -w /work -v -v

