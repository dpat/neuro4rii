## Resources

To load datalad on the HPC:
```bash
module load contrib
module load python-virtualenvs/datalad
```



# OpenNeuro

✅ Install a dataset (install is a superset of clone):
```bash
datalad install https://github.com/OpenNeuroDatasets/ds003550.git
```
✅ Navigate into the directory:
```bash
cd ds003550
```
✅ How big is it?
```bash
du -s -h
```

### Answer the question on OpenClass

1) What does `datalad install` do?



✅ Learn about its history (--oneline keeps the log output small):

```bash
git log --oneline
```
✅ How big is the original online dataset?
```bash
datalad status --annex
```


### Answer the question on OpenClass

2. Can you use git to learn about your DataLad directory?



## datalad get

✅ Get some of the actual image data. Navigate into the sub-sc001 directory and get everything:
```bash
cd sub-sc001
datalad get .
```
✅ How big is the directory now?
```bash
cd ..
du -s -h
```

✅ Open and view one of the images for sub-sc001. The image is still a link to data in the .git/annex directory, BUT, the link is no longer broken!
✅ Download just the anatomical files for each subject with sub-sc00 in the directory name:

```bash
for s in sub-sc00*; do datalad get ${s}/anat; done 
```
### Answer the questions on OpenClass

3. What does `datalad get`  do?
4. What command would I use to retrieve **the_most_interesting_file.pdf**, in my cloned dataset?



## .gitconfig

If you don't have a .gitconfig file in your home directory, datalad will complain.  This is just a warning, but to placate datalad, you can create the file and populate it.

```bash
cd ~; touch .gitconfig
```

Open your new `.gitconfig` file and add appropriate content (just change the name and email address to be your own)

```bash
[user]
        name = Dianne Patterson
        email = dkp@arizona.edu

[alias]
    co = checkout
    ci = commit
    cnf = config
    merge = merge --no-commit --no-ff
    mergelast2 = rebase -i HEAD~2
    noskip = update-index --no-skip-worktree
    skip = update-index --skip-worktree
    slog = log --pretty=format:'%h [%cn] %cd (%cr): %s'
```



### Answer the question on OpenClass

5. Is .gitconfig required to work with DataLad?



## datalad drop

Reverse the effects of get with drop, by running the following command from ds003550:
```bash
datalad drop .
```


### Answer the question on OpenClass

6. What does `datalad drop` do?
   

## 

## Nested Datasets

Goal: Understand what DataLad nesting is and why it is so important: It is really important to prevent a DataLad dataset from becoming too large, but by nesting DataLad datasets you keep the datasets small and responsive.

✅ Install the abide dataset from the Datalad repository.  This retrieves the structure of the top-level dataset
```bash
datalad install https://datasets.datalad.org/abide
```
✅  However, the dataset includes subdatasets, and the repository information on those subdatasets has not yet been installed. Use the -r option to install all subdatasets:
```bash
datalad install -r https://datasets.datalad.org/abide
```
## Top level Commits
✅ **Navigate to the top level of the abide directory**, and use git to view the history of the dataset:
```bash
git log --oneline
```
You may need to use `q` to quit the displayed repository information.

The *abide* dataset includes sub-datasets, and the repository information on those sub-datasets has not yet been installed. Use the `-r` option to install all sub-datasets:

> One aspect of nested datasets is that any lower-level DataLad dataset (the subdataset) has a stand-alone history. The top-level DataLad dataset (the superdataset) only stores which version of the subdataset is currently used" The DataLad Handbook 1.5 — Dataset nesting

This means that `git log` will list different commits depending on which dataset you query.

## abide/Derivatives Commits
✅ **Navigate to the subdataset abide/Derivatives** and view the git history:
```bash
git log --oneline
```
The commits are different because the dataset was stored as a separate nested subdataset.

## Summary: Nesting

The use of nesting is critically important to keeping dataset handling fast: If you store everything in one dataset, commits gets slower and slower!

### Answer the question on OpenClass

7. Can you have DataLad datasets inside of other DataLad datasets?

## OpenNeuro and Datalad Repository: Summary

Not all of the public datasets are available through DataLad, but when they are, it is very fast to download them and you can control exactly the files you actually want.

---

---

# TemplateFlow 

✅  **Navigate back up to the main `dlad` directory**.

✅ Download the templates with Datalad (see TemplateFlow Archive):
```bash
datalad install -r ///templateflow
```

✅ **Navigate into the new templateflow directory** and list its contents with `ls`.
## Understand Storage in .git/annex
Each templateflow directory contains links to binary image files that you don't have:
```bash
ls -lR tpl-MNI152Lin/*nii.gz
```

This long listing reveals that the binary files are links deep into the .git/annex directory.  Links are small and the actual binary contents of the files have not yet been downloaded.

✅ Use datalad get to retrieve an actual file.   
```bash
datalad get tpl-MNI152Lin/tpl-MNI152Lin_res-01_PD.nii.gz
```
## Follow Mango Instructions on OpenClass!



### Answer the question on OpenClass

8. What statement best describes the relationship between git-annex and DataLad?

   

## Extract files from datalad with cp

To properly extract an individual file from DataLad (git-annex) control, use `cp` which operates on the binary file the link points to, and not just the link.


✅ Copy the file using the command line: the actual binary data rather than the link should be copied. 

```bash
cp tpl-MNI152Lin/tpl-MNI152Lin_res-01_PD.nii.gz ..
```
✅ Create a target directory:
```bash
mkdir ../test_cp_folder_contents
```

✅ Copy multiple files:
```bash
cp -R tpl-MNI152Lin/* ../test_cp_folder_contents
```
The files and subdirectories are recursively extracted from datalad control and placed into the target folder.

## View file with FSLeyes

✅ Just so you get to try another viewer, right-click `tpl-MNI152Lin_res-01_PD.nii.gz` on the Desktop and choose `Open With fsleyes` (be patient, this is slow, especially the first time you open FSLeyes). 

Because it is now free of git-annex, the image displays just fine!

### Answer the question on OpenClass

9. How can you copy an individual file from a DataLad directory?
9. In fsleyes, what happens when you click the eye in the overlay list?



## Extract a dataset from datalad with export-archive

✅ First, from the templateflow superdataset, use datalad get to retrieve all the binary files in the small tpl-MNI152Lin dataset:
```bash
datalad get tpl-MNI152Lin
```

✅ From the templateflow superdataset, export the dataset as an archive: 
```bash
datalad export-archive -d tpl-MNI152Lin ../export_test
```

✅ After navigating to the location of the tar file,  untar it:  
```bash
 tar xvf export_test.tar.gz
```
All of the `nii.gz` files have been converted from links into stand-alone files. The `.git` directory has been removed.



### Answer the question on OpenClass

11. If you hate DataLad, can you extract the data from git-annex and make it a regular directory?

## View Metadata with JQ
Although the large binary image files have not been downloaded, JSON files are text and therefore present in the dataset. For example, each template directory contains a JSON file: `template_description.json` with the informative fields Identifier and Name.

✅ Navigate back to the `templateflow` directory.

✅ Use `jq` to display just those fields, thus demonstrating that they exist, and getting some useful information about the template flow dataset.

✅ Use `jq` to display just those fields, thus demonstrating that they exist, and getting some useful information about the template flow dataset.

```bash
for t in tpl*; do echo "==="; echo ${t}; jq '.Identifier, .Name' ${t}/template_description.json; done
```

---

## Cleanup

✅ **Navigate to your home directory** and remove the dlad directory. You don't need it anymore. 
Because many files are protected, you can't remove everything unless you first change the permissions, (be patient, there are a lot of little files in here).

```bash
chmod a+rwx dlad; rm -fr dlad
```

### Answer the question on OpenClass

12. Are there any tricks involved in removing a datalad directory?