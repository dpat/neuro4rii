# Tutorial: BIDS ASL Conversion

## Cloud Shell Lessons

Data (1)   
gdcm (2)    
Dicom to BIDS Conversion (3)   
Add JSON Fields (4)   
BIDS Validation (5)   
aslcontext files (6)    

-----

### Prerequisites

* This tutorial assumes you've followed the [Google Cloud Shell doc](https://docs.google.com/document/d/1QGAlohst_kezlvrHKZ0I439Ywv_ddIsNUDbH0SnL1OI/edit?usp=sharing) to set up the environment.
* You should also have completed: tutorial_04_unix_scripting.md, tutorial_heudiconv.md, tutorial_jd.md, and tutorial_jq.md.
*  Completing these tutorials gaurantees you have tree, jadd, jd, jdel, hdc1.sh and hdc2.sh installed in your bin directory. 

------------

## Upload Data to Cloudshell (1)

The directory ASL2BIDS_tutorial_data can be downloaded from OSF using wget: 

```bash
wget https://osf.io/hxz8t/download -O ASL2BIDS_tutorial_data.zip; unzip ASL2BIDS_tutorial_data.zip; rm ASL2BIDS_tutorial_data.zip
```

---

## ASL2BIDS_tutorial_data Directory Contents (1)

-  These data are a subset of the dataset described in ASL_Siemens_PASL_protocol.pdf. 
-  The `ASL2BIDS_tutorial_data` directory contains `dicom` and `bids` subdirectories: 

```terminal
├── bids
│   └── code
└── dicom
    └── CAM002
        ├── 3D_PASL_M0_2800_6_PAIRS_0008
        ├── FIELD_MAP_0010
        ├── FIELD_MAP_0011
        ├── PASL_M0_0005
        ├── PASL_PRODUCT_10ARRAY_0007
        └── T1_SAGITTAL_MPRAGE_0002
```

## DICOMS (1)

DICOM data in dicom/CAM002 includes: 

-  a T1w anatomical file for registration, 
-  field maps for distortion correction, and 
-  three different ASL datasets. 
-  The characteristics of the three ASL sequences are reviewed next and can be found in the protocol (ASL_Siemens_PASL_protocol.pdf). 

## 1) Single TI ASL: 3D_PASL_M0_2800_6_PAIRS_0008 (1)

-  This is a 3D pASL scan with a single-TI time at 2800 ms, an initial M0scan, and 6-control-label pairs. It was acquired PA.
- Voxel Size: x: 4.0 mm; y: 4.0 mm; z: 5.0 mm, 
- Acquisition Time (TA) 1.48 minutes; 
- Bolus Duration (BD) 700 ms, 
- Inversion Time (TI) = 2800 ms,
- TR = 4 sec

## 2) Separate M0scan: PASL_M0_0005 (1)

-  This is a separate M0scan for use with the multi-TI acquisition.  As such, its parameters are closely matched to the multi-TI sequence. It was acquired PA.
-  Voxel Size: x: 4.0 mm; y: 4.0 mm; z: 5.0 mm, 
-  Acquisition Time (TA) 28 sec,
-  TR = 4 sec 

## 3) Multi TI ASL: PASL_PRODUCT_10ARRAY_0007 (1)

-  This is a 3D pASL scan with multiple TIs. It was acquired PA.
-  Voxel Size: x: 4.0 mm; y: 4.0 mm; z: 5.0 mm, 
-  Acquisition Time (TA) 5.24 minutes,
-  Bolus Duration (BD) 700 ms, 
-  Control-Label pairs have the following inversion times (260 ms apart):  
-  Repetiton 1 (pairs 1-10): 920, 1180, 1440, 1700, 1960, 2220, 2480, 2740, 3000, 3260
-  Repetiton 2 (pairs 11-20): 920, 1180, 1440, 1700, 1960, 2220, 2480, 2740, 3000, 3260
-  TR = 4 sec

## BIDS (1)
-  The `bids` directory contains a `code` subdirectory with scripts to facilitate conversion from DICOM (`heuristic1.py`), and subsequent manipulation of the  json key-value pairs (`jadd*`). 

----

## gdcm (2)

- gdcm tools are command-line tools for working with DICOM files.  
- Of particular interest is the command-line tool [gdcmdump](https://manpages.ubuntu.com/manpages/xenial/man1/gdcmdump.1.html) which can be used to view information that SIEMENS stores in private tags (0x0029,0x10,"SIEMENS CSA HEADER"). 

---

### Install gdcm tools (2)

-  Install the gdcm tools. The tools are in `/usr/bin`. 
  *You will be warned that the installation won't last past the current session*.  
  
    ```bash
    sudo apt-get install libgdcm-tools
    ```
  
-  Type `gdcmdump` to ensure it is available (you should see a usage message).

---

## Use gdcmdump (2)

-  Navigate to the DICOM directory containing the subject sequences: 
    ```bash
    cd ~/ASL2BIDS_tutorial_data/dicom/CAM002
    ```

-  Review the information buried in the DICOM header for the the Single TI ASL DICOM data:

   ```bash
   gdcmdump -i 3D_PASL_M0_2800_6_PAIRS_0008/CAM002*87.IMA --csa-asl --print
   ```

-  `--csa-asl` means *print decoded SIEMENS CSA MR_ASL (base64)*.

-  The following DICOM_NAME fields are of interest to BIDS: ASL Crusher Flag, ASL Bolus Cut-off Flag, ASL Bolus Cut-off Technique, ASL Bolus Cut-off Delay Time, and ASL Slab Thickness. 

-  See the example output in the next step.  In each case the description of the value is a comment listed **after** the value and the comment may wrap to the next line.

## Example output (2)

   ```bash
   # Dicom-Meta-Information-Header
   # Used TransferSyntax:
   
   # Dicom-Data-Set
   # Used TransferSyntax: Unknown Transfer Syntax
   (0018,9251) SQ (Sequence with undefined length) # u/l,1 MR Arterial Spin Labeling Sequence
     (fffe,e000) na (Item with undefined length)
       (0018,9252) LO [3D pulsed ASL (TGSE)]      # 20,1 ASL Technique Description
       (0018,9257) CS [LABEL ]                    # 6,1 ASL Context
       (0018,9259) CS [NO]                        # 2,1 ASL Crusher Flag
       (0018,925c) CS [YES ]                      # 4,1 ASL Bolus Cut-off Flag
       (0018,925d) SQ (Sequence with undefined length) # u/l,1 ASL Bolus Cut-off Timing Sequence
         (fffe,e000) na (Item with undefined length)
           (0018,925e) LO [Q2TIPS] # 6,1 ASL Bolus Cut-off Technique
           (0018,925f) IS (UL) [2790] # 4,1 ASL Bolus Cut-off Delay Time
         (fffe,e00d)
       (fffe,e0dd)
       (0018,9260) SQ (Sequence with undefined length) # u/l,1 ASL Slab Sequence
         (fffe,e000) na (Item with undefined length)
           (0018,9253) US 1                            # 2,1 ASL Slab Number
           (0018,9254) FD 156                          # 8,1 ASL Slab Thickness
           (0018,9255) FD 0\-0\1                       # 24,3 ASL Slab Orientation
           (0018,9256) FD 0\-2.71186\-41.4831          # 24,3 ASL Mid Slab Position
           (0018,9258) IS (UL) [10]                    # 2,1 ASL Pulse Train Duration
         (fffe,e00d)
       (fffe,e0dd)
     (fffe,e00d)
   (fffe,e0dd)
   ```

----

### Think about This (2)

How would you find the BolusCutOffDelayTime for the MultiTI ASL sequence?

----

## Solution: View BolusCutOffDelayTime Information (2)

Simply select DICOMS in a different directory.  You may also pipe (|) the results through `grep` to reduce the output to something more manageable:
```bash
gdcmdump -i PASL_PRODUCT_10ARRAY_0007/CAM002*37.IMA --csa-asl --print | grep Cut-off
```

----

## DICOM to BIDS Conversion (3)

From the main ASL2BIDS_tutorial_data folder, run heudiconv (the Docker container will be downloaded if it isn't found).  This command requires you to have `hdc2.sh` installed in your `bin` directory!  See the prerequisites.
  ```bash
  cd ~/ASL2BIDS_tutorial_data/
  hdc2.sh dicom bids heuristic1.py CAM002
  ```

-  This command uses the conversion script in `~/ASL2BIDS_tutorial_data/bids/code/heuristic1.py` to convert the DICOM data into a BIDS data structure.  
-  Docker downloads the [heudiconv](https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/heudiconv.html) container if it is not already present.

----

## Understand the DICOM Conversion (3)

-  Use the editor to open `heuristic1.py` so you can see its contents:
  ```bash
  edit bids/code/heuristic1.py
  ```
  
-  In between the lines of code are comments (in green).  Read these to understand what the code is doing. If you have completed the heudiconv lesson, then the structure of `heuristic1.py` should seem familiar, but provide the details of of converting asl data.

---

## Add JSON Fields (4)

Three scripts are provided in `bids/code`directory to facilitate adding the correct JSON fields: `jadd_m0.sh`, `jadd_multi.sh`, and `jadd_sngl.sh`. You must have `jadd` installed in your `bin` directory to use these scripts (Ensure you have completed the prerequisites for this practicum).

Navigate to the `perf` subdirectory:
```bash
cd bids/sub-CAM002/perf
```

## Run the following three scripts to update the values in the ASL and M0 JSON files:
```bash
../../code/jadd_sngl.sh sub-CAM002_acq-snglti_dir-PA_asl.json
```

```bash
../../code/jadd_multi.sh sub-CAM002_acq-multiti_dir-PA_asl.json
```

```bash
../../code/jadd_m0.sh sub-CAM002_acq-multiti_dir-PA_m0scan.json
```

---

The JSON files are read-only by default, so you will need to confirm the changes by entering `y`, e.g.:

````bash
mv: replace 'sub-CAM002_acq-snglti_dir-PA_asl.json', overriding mode 0444 (r--r--r--)? y
````

---

## Check the JSON sidecar for the ASL file (4)

-  Display all three json files in the editor:

   ```bash
   edit *.json
   ```

-  Scroll to the bottom of each and ensure that approriate values have been added.

-  For example, in the single TI data you should see:

```json
  "PostLabelingDelay": [
    0,
    2.1,
    2.1,
    2.1,
    2.1,
    2.1,
    2.1,
    2.1,
    2.1,
    2.1,
    2.1,
    2.1,
    2.1
  ],
  "BackgroundSuppression": true,
  "BolusCutOffFlag": true,
  "M0Type": "Included",
  "RepetitionTimePreparation": 4,
  "VascularCrushing": false,
  "LabelingSlabThickness": 156,
  "AcquisitionVoxelSize": [
    4,
    4,
    5
  ],
  "BolusCutOffTechnique": "Q2TIPS",
  "BolusCutOffDelayTime": 2.79
```

## Close the JSON files, and view the scripts in the editor:

```
edit ../../code/jadd*.sh
```

-  Such scripts are a quick way to accurately update all shared values.  Crucially, the command line is a little different if the value in the JSON file is a number, an array, a Boolean, or a string.  You'll need to understand these differences to use the scripts effectively for your own data.

----

## Create the aslcontext.tsv file for the Single-TI Sequence (4)

The BIDS specification for ASL requires an [aslcontext.tsv](https://bids-specification.readthedocs.io/en/stable/99-appendices/12-arterial-spin-labeling.html#_aslcontexttsv-three-possible-cases) file. aslcontext files list the number and order of scans (m0, label and control).  

Create the aslcontext file: 

```bash
edit sub-CAM002_acq-snglti_dir-PA_aslcontext.tsv 
```

Here's the correctly specified singleTI `sub-CAM002_acq-snglti_dir-PA_aslcontext.tsv` file listing a value for each of the 13 volumes: the m0+6 control-label pairs. Copy and paste the values into the editor and save.

```terminal
volume_type
m0scan
control
label
control
label
control
label
control
label
control
label
control
label
```



-----

## BIDS Validation (5)

-  To verify that the BIDS validation errors have been handled, zip the bids directory.

-  First, navigate up three levels so you are above the bids directory

   ```bash
   cd ../../..
   ```
- Now zip the file
   ```bash
   zip -r bids.zip bids
   ```
- Download it (right-click to download).  Then unzip it on your local machine.
-  In your web browser, open https://bids-standard.github.io/bids-validator/.
-  Select `Choose File` and select the bids directory you just downloaded.  The directory should now be valid.
