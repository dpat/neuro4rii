# Tutorial Dicom Deidentification

The goal of this tutorial is to provide a solution for preparing DICOMS for public sharing.  This means removing any PHI (Protected Health Information), but without destroying crucial DICOM information.

## Cloud Shell Lessons

Introduction (1)   
GDCM (2)   
Defacing (3)    
Summary and Resources (4)   



---

## DICOM Header Information (1)

-  Compared to clinical scans, research scans are largely deidentified by default.  
-  They don't include Patient Names, addresses or IDs.
-  They sometimes include birthdate (it would be better if they didn't), and 
-  They typically include the scan date in half a dozen fields. 
-  Deidentification can be accomplished by replacing the date values with legitimate values that only accurately represent the year.

## Where do you find the DICOM codes tags (1)

To remove or repace DICOM values, you need the DICOM tags. Here are several options for finding those tags:

-  Consult the [DICOM Library](https://www.dicomlibrary.com/dicom/dicom-tags/), which is a searchable list of DICOM tags online.
-  Use `gdcmdump` with grep to explore the Field names.  
-  Use [Horos](https://horosproject.org/) to display and search metadata for any DICOM sequence.



---

## Questions

-  1.1 What is the DICOM Library?

-  1.2 Do research scans have as many deidentification issues as clinical scans?

   

---



## DICOM Defacing (1)

It is relatively easy to find tools that deface NIfTI T1w images.  Some defacing tools even handle T2 or FLAIR defacing. It is much more difficult to find tools that deface and return usable DICOMS.  

Milchenko's mohanar/facemasking:v1.12 Docker container defaces and returns DICOM files!  

It is used on the XNAT platform, but can also be used as a stand-alone Docker container. 

The defacing algorithm can be used on MR (including T1w, T2w, and FLAIR) and CT.

Together, the replacement of dates and the defacing of anatomical scans make it possible to share DICOM data that is truly deidentified!

## Download Data and Scripts (1)

-  Download the scripts `dicom_deidentify.sh`  and `facemask.sh` to your bin directory and make them executable:

    ```bash
    wget https://bitbucket.org/dpat/tools/raw/master/LIBRARY/dicom_deidentify.sh -O bin/dicom_deidentify.sh; chmod a+x bin/dicom_deidentify.sh 
    ```

    ```bash
    wget https://bitbucket.org/dpat/tools/raw/master/LIBRARY/facemask.sh -O bin/facemask.sh; chmod a+x bin/facemask.sh
    ```

- Download the 417 MB **dataset**: [dicom_deid.zip](https://osf.io/h9tg3/) then unzip it and navigate to the MRIS directory

   ```bash
   wget https://osf.io/h9tg3/download -O dicom_deid.zip; unzip dicom_deid.zip; rm dicom_deid.zip; cd dicom_deid
   ```

    The data is freely released and thus not subject to HIPAA, making it perfect for learning to use these tools. The data should look like this:

    ```terminal
    dicom_deid/
    └── dicom
        └── 219
            ├── Bzero_verify_PA_17
            ├── DTI_30_DIRs_AP_15
            ├── field_mapping_20
            ├── field_mapping_21
            ├── Post_TMS_restingstate_30
            ├── restingstate_18
            └── T1_mprage_1mm_13
    ```

If you don't have `tree`, you can install it in your bin directory now:

```bash
wget https://bitbucket.org/dpat/neuro4rii/raw/main/binaries/tree  -O ~/bin/tree; chmod a+x ~/bin/tree
```



---

## GDCM (2)

[GDCM](http://gdcm.sourceforge.net/) tools are command-line tools for working with DICOM files.  Of particular interest are `gdcmdump` and `gdcmanon`.  

-  `gdcmdump` displays DICOM header information.  
-  `gdcmanon` is for anonymizing the data.

### Install gdcm tools (2)

-  Install the gdcm tools. The tools are in `/usr/bin`. 
   *You will be warned that the installation won't last past the current session*.  

     ```bash
   sudo apt-get install libgdcm-tools
     ```

-  Type `gdcmdump` to ensure it is available (you should see a usage message).

## Use gdcmdump (2)

`gdcmdump` displays information in the DICOM header. Try it out:

```bash
gdcmdump dicom/219/T1_mprage_1mm_13/IM-0001-0010.dcm
```

This should display a long list of information from the DICOM header.  

**Note**: *To reduce the total output, we have selected a single DICOM file to represent the group. This is appropriate because all of the values of interest should be the same for every file in this entire directory of DICOM files. Otherwise, you end up seeing the information repeated for every single file!*

Restrict that list to just Date information with grep:

```bash
gdcmdump dicom/219/T1_mprage_1mm_13/IM-0001-0010.dcm | grep Date
```

This displays a more manageable seven values. gdcmanon can be used to replace these values.  The reported birthdate is 19640706 (July 6, 1964) :

```terminal
(0008,0012) DA [20180706]           # 8,1 Instance Creation Date
(0008,0020) DA [20180706]           # 8,1 Study Date
(0008,0021) DA [20180706]           # 8,1 Series Date
(0008,0022) DA [20180706]           # 8,1 Acquisition Date
(0008,0023) DA [20180706]           # 8,1 Content Date
(0010,0030) DA [19640706]           # 8,1 Patient's Birth Date
(0040,0244) DA [20180706]           # 8,1 Performed Procedure Step Start Date
```

## Understand the Output of gdcmdump (2)

-  In parentheses is the **tag**, e.g., (0008,0012): 

>  a tag that identifies the attribute, usually in the format (XXXX,XXXX) with hexadecimal numbers, and may be divided further into DICOM Group Number and DICOM Element Number;
>
>  https://www.dicomlibrary.com/dicom/dicom-tags/

-  A [value representation (VR)](https://dicom.nema.org/medical/dicom/2017a/output/chtml/part05/sect_6.2.html), e.g., DA, describes the data type and format of the values. For example *DA* means *date*, *PN* means *person name*, etc.
-  The actual content of the field, 
   -  e.g., `256`, or  `[20180706]`

-  A comment containing a description of the field, e.g., 
   -  `# 14,1 Series Time` or `# 8,1 Study Date`


## Use gdcmanon (2)

`gdcmanon` is used to anonymize the DICOM data. 

In this example, use `gdcmanon` to change the Birth Date recursively in the entire DICOM directory.   Change it so that nothing more specific than the year is correctly encoded: this complies with the HIPAA rules.  

```bash
gdcmanon --dumb --replace 10,30,19640101 --input dicom/219 --output test --recursive
```

### Check the output (2)

```bash
gdcmdump test/T1_mprage_1mm_13/IM-0001-0010.dcm | grep Birth
```

You should see:

```terminal
(0010,0030) DA [19640101]             # 8,1 Patient's Birth Date
```

## Understand the `gdcmanon` command (2)

gdcmanon can run in **dumb** mode (used here) or **PS 3.15** mode (which requires a certificate):

Dumb Mode: 

>  Improper use of this mode may cause an important tag to be emptied/removed/replaced resulting in an illegal/invalid DICOM file. If you delete a Type 1 attribute, chances are that your DICOM file will be not accepted in most DICOM third party viewer.

>  http://gdcm.sourceforge.net/html/gdcmanon.html

---

Here we are running in dumb mode to avoid the certificate process.

**Options** are `--replace` or `--remove`.  In either case, the DICOM tag must then be supplied.   
The tag is two comma separated fields (careful, **no** spaces!).   
It is legitimate to supply the DICOM tag with or without leading zeros, i.e., the tag 0010,0020 is the same as 10,20. 

`--replace` requires an additional comma separated value.  It is up to you to ensure this is a legal value! For example, a date should be fully specified (YearMonthDay), e.g., `19641027`, but dumb mode would allow you to enter `1964` or `fred` into the field, neither of which is actually a legal DICOM value for this field. 

Supply the input directory after `--input`, the output directory after `--output` and to apply the operation to all of the dicom files in the directory, supply the `--recursive` flag (without the `--recursive` flag, no files will be anonymized)

Examples of remove and replace:

```terminal
# With replacement
gdcmanon --dumb --replace 10,30,19640101 --input dicom/219 --output test --recursive
# With removal instead
gdcmanon --dumb --remove 10,30 --input dicom/219 --output test --recursive
```

## Questions (2)

-  2.1 Look in [DICOM Library](https://www.dicomlibrary.com/dicom/dicom-tags/)  to find the Dicom Tag for `Institution Address`.    
-  2.2 What is the DICOM tag for Institution Address?     
-  2.3 What are two legitimate ways to supply this tag in a `gdcmanon` command?    

---

## Use a Script for Anonymizing Dates (2)

Use [dicom_deidentify.sh](https://bitbucket.org/dpat/tools/src/master/LIBRARY/dicom_deidentify.sh), which you have already downloaded to your bid directory to do all the date replacements at once. Type the name of the script to ensure it is available (You should see a usage message):

```bash
dicom_deidentify.sh
```

Supply the dicom input directory and the revised scan and birthdates:

```bash
dicom_deidentify.sh dicom 20180101 19640101
```

A new directory, `dicom_anon` is created and the DICOMS are revised scan and birthdates.

The script runs `gdcmdump` for the dates in a sample output DICOM file, so you can check that the values are correct:

```terminal
dicom_anon has these revised date values:
(0008,0012) DA [20180101]           # 8,1 Instance Creation Date
(0008,0020) DA [20180101]           # 8,1 Study Date
(0008,0021) DA [20180101]           # 8,1 Series Date
(0008,0022) DA [20180101]           # 8,1 Acquisition Date
(0008,0023) DA [20180101]           # 8,1 Content Date
(0010,0030) DA [19640101]           # 8,1 Patient's Birth Date
(0040,0244) DA [20180101]           # 8,1 Performed Procedure Step Start Date
```

---

### Questions (2)

-  2.4 What is the advantage to running the `dicom_deidentify.sh` script?

-  2.5 If you run the `dicom_deidentify.sh` script, do you also need to explicitly run the gdcmanon commands described previously?

-  2.6 What advantage does `gdcmanon` provide compared to `dicom_deidentify.sh`?

---

## Summary (2)

If you wish to make additional changes to the DICOM headers, the script provides an example that you may wish to revise.  In the next section, you'll work with DICOM defacing for high resolution anatomical files.


-----------

## Defacing: Download the Container and View Options (3)

Download the 6.3 GB container

```bash
docker pull mohanar/facemasking:v1.12
```

---

### View facemasking Options (3)

```bash
docker run --rm -it mohanar/facemasking:v1.12 /bin/bash -c "source /opt/facemasking_launch_script/maskface_setup.sh && mask_face_nomatlab"
```



---

## Run Defacing (3)

The tool `mask_face_nomatlab` gets run on the series directory, e.g., `T1_mprage_1mm_13`. 

Navigate to the new `dicom_anon` directory:

```bash
cd dicom_anon
```

Run the facemasking Docker container:

```bash
docker run --rm -it -v ${PWD}:/tmp mohanar/facemasking:v1.12 /bin/bash -c "cd /tmp && pwd && source /opt/facemasking_launch_script/maskface_setup.sh && mask_face_nomatlab 219/T1_mprage_1mm_13 -b 1 -e 1 -m normfilter"
```

## Understand the Docker Command (3)

-  The present working directory gets mounted to `/tmp` (which exists in the container)
-  `-c` precedes the string that bash should run as a command.  In this case, the command includes navigating to the `tmp` directory, identifying and sourcing the correct files internal to the container, passing in the path to the T1w series to deface, and the relevant flags. 
-  The path from `tmp` to the series to be deidentified is specified:  `219/T1_mprage_1mm_13`
-  Unless you specify otherwise with `-o`, output is placed in the mounted `tmp` directory. 
-  Unless you specify otherwise `-v 0` is the default, which prioritizes speed over intermediate output.
-  The suggested `-b 1` uses FSL bet to identify brain
-  `-e 1` masks the ears
-  `-m normfilter` uses the recommended `normfilter`

## Output (3)

DICOM defacing takes about 15 minutes.

Subdirectories are produced in the present working directory `dicom_anon`: 

-  `DICOM_DEFACED` contains the defaced T1w DICOMS.  This is the directory you want!
-  `maskface`  includes pngs to examine, and intermediate `*.img` and `*.hdr` files. 
-  The empty directories `hsperfdata_root` and `.felix`

```terminal
dicom_anon
├── 219
│   ├── Bzero_verify_PA_17
│   ├── DTI_30_DIRs_AP_15
│   ├── Post_TMS_restingstate_30
│   ├── T1_mprage_1mm_13
│   ├── field_mapping_20
│   ├── field_mapping_21
│   └── restingstate_18
├── DICOM_DEFACED
│   └── T1_mprage_1mm_13
├── hsperfdata_root
└── maskface
```

-  An additional nine intermediate  T1 `img`, `hdr` and `mat` files are produced in `dicom_anon`
-  Although the original DICOM series was ~415 MB, the final directory is ~1 GB, with most of that in the `maskface` directory! 
-  The DICOM headers have changed (as revealed by `diff`), but the DICOMS are still quite usable for BIDS conversion to NIfTI and do not appear to have lost any crucial information.
-  Unless you want to track the intermediate steps, you could  save space by removing everything except the `*.png` files in `maskface` and the `DICOM_DEFACED` directory.  

## View the png files (3)

Google Cloud shell can display the images for you, so you can see the quality of the defacing.

```bash
edit maskface/*.png
```

The `*surf*` images show a 3D reconstruction in which it is clear that facial features have been blurred (Don't worry if they are upside down).

The  `*normfilter*`  images display a lightbox view which shows a thin blurred line in place of ear and facial features. See if you can find it.

## Remove the output of face masking (3)

-  Remove the folders and files you just created so you can try the `facemask.sh` script.    
-  You need to use the `sudo` command because Docker output on Linux belongs to root by default, and not to you! 
-  On cloud shell, you have root permission, so this is not a huge problem, but on other Linux systems you might not have root permission and then it IS a problem!

```bash
sudo rm -fr .felix hsperfdata_root DICOM_DEFACED maskface; sudo  rm *.img *.hdr *.mat
```

## Try facemask.sh (3)

`facemask.sh` solves the permissions problem, cleans up intermediate files, and encapsulates the Docker command so you don't have to specify it every time.  

Try [facemask.sh](https://bitbucket.org/dpat/tools/raw/master/LIBRARY/facemask.sh):

```bash
facemask.sh 219/T1_mprage_1mm_13
```

This works well for other modalities as well (e.g., FLAIR and T2w images)!

## Question (3)

-  3.1 What is the difference between `facemask` and `gdcmanaon`?
-  3.2 `facemask.sh` solves the permissions problem on Linux.  Look at the script and figure out how it does this.

---

## Summary (4)

In this tutorial you have learned to use two tools: `gdcm` and `maskface`. 

`gdcm` is used to examine information on the DICOM header and remove or replace any protected health information. 

`maskface` is used to anonymize the face on T1w images.

Together these tools can be used to prepare DICOM data for public sharing.

Although this tutorial attempts to identify any issues of concern for HIPAA, rules change and alternative interpretations may find this process incomplete in some way.  It is up to you to interpret HIPAA rules and decide whether these procedures are sufficient for deidentification.

## Resources (4)

-  [DICOM Library](https://www.dicomlibrary.com/dicom/dicom-tags/) 

-  [GDCM site](http://gdcm.sourceforge.net/) 

-  Milchenko M, Marcus D (2013) Obscuring surface anatomy in volumetric
   imaging data. Neuroinformatics 11: 65-75.
-  Documentation for maskface is available at
   https://wiki.xnat.org/xnat-tools/face-masking/mask_face-usage-manual

