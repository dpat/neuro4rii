

# Tutorial 5: Conda

Conda is a package manager, which means you use it to install programs.  It is available as [Anaconda](https://www.anaconda.com/) and as [miniconda](https://docs.conda.io/en/latest/miniconda.html).  Miniconda is small, and only requires a command-line interface.  If you install miniconda in your home directory, you can install many tools that would otherwise be difficult or impossible for a non-root user to install. 

In this tutorial, you'll install miniconda and datalad.  Datalad is the subject of a separate tutorial. Anything you install here, you can also install the same way with miniconda on your local computer or on the HPC.

----------
## Cloud Shell Lessons
  * Install miniconda (1)
  * Install Datalad (2)
  * Understand the Conda Environment (3)

-------------------
## Part 1: Install Miniconda (1)
* Conda is a package manager that facilitates installing many software tools, especially Python-based tools.

* Miniconda is a small distribution of conda.

* An advantage of conda is that everything is installed in your home directory and with your permissions. This means that on Google Cloud Shell, your installations will still exist the next time you log in.  On the HPC, it means you can install tools that would otherwise require root privileges.

* Conda is available for Mac, Windows, and Linux.

* Finally, conda creates sandboxed environments where software is installed.  This means the software cannot corrupt your system Python.

* Create a directory called `miniconda`

   ```bash
   mkdir miniconda
   ```

* Download the Miniconda installation script for Linux (Google Cloud Shell runs on a Linux virtual machine): 
  ```bash
  wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
  ```

* Run the Miniconda installation script (`-b` to be able to run unattended, which means that all of the agreements are automatically accepted without user prompt. `-u` updates any existing installation in the directory of install if there is one. `-p` is the directory to install into):
  ```bash
  bash Miniconda3-latest-Linux-x86_64.sh  -b -u -p ~/miniconda
  ```
  
* Add this conda initialization incantation to your .bashrc:
  
  ```bash
  ~/miniconda/bin/conda init bash
  ```
  
* After the install is complete, source your **.bashrc** to refresh the terminal with information about conda:
  
  ```bash
  source .bashrc
  ```
  
* This will start the conda environment. Notice that your command prompt now includes **(base)**, e.g.,:
  ```
  (base) dkp@cloudshell:~$
  ```
  
* If you don't want conda to start every time you open a terminal, simply add the following line at the end of your .bashrc:

   ```bash
   conda deactivate
   ```

* You can still run conda commands and activate or deactivate the environments, as you'll see in parts 2 and 3.

---

## Question (1)

-  1.1 Do you need root permisson to install conda?

-----------------

## Add the conda-forge channel (1)

A conda channel is the location where packages are stored.

[Conda-forge](https://conda-forge.org/) is a large and well-maintained channel where many useful tools are available as conda repositories. Adding conda-forge to miniconda as a channel allows miniconda to find and download those tools. 

- List the current conda channels:

   ```bash
   conda config --show channels
   ```

- The current channels are listed:
  ```terminal
  channels:
    - conda-forge
    - defaults
  ```
- If conda-forge is not in the list, then add the conda-forge channel:

   ```bash
   conda config --add channels conda-forge
   ```

---

### Question

-  1.2  What is a conda channel?

---

## Part 1: Summary (1)

-  Conda is a useful package manager.  It works on Windows, Mac, and Linux. 
-  You have learned to install the conda package manager, and configure your `.bashrc`. 
-  You added the conda-forge channel, where many valuable packages are available. 
-  This gives you the freedom to install many tools even without root permissions. 

----------------
## Part 2: Install Datalad (2)
Datalad is a large package with many dependencies. It provides a revision control system for data, and was designed with neuroimaging datasets in mind.  You will learn more about it later when you study revision control.  Right now, you can install it with conda (though this is not the only option for installing Datalad).

* Create an environment where datalad will be installed.  Call it something meaningful.
  ```bash
  conda create -n datalad
  ```
* Activate your new environment
  ```bash
  conda activate datalad
  ```
* Install datalad in this new environment  (follow the prompts and answer yes)

  ```bash
  conda install -c conda-forge datalad
* Ensure that Datalad is installed:
  ```bash
  which datalad
  ```
* You should see something like this (but for your username):  
  ```terminal  
  /home/dkp/miniconda/envs/datalad/bin/datalad`
  ```
* Check the datalad version:
  ```bash
  datalad --version
  ```
* You should see something like:  
  ```terminal
  datalad 0.17.6
  ```


## Part 2: Summary (2)

-  You created a conda environment and installed datalad into that environment. 

-  Next you'll learn how to activate and deactivate that environment, update a package, remove an environment and its packages, and run conda clean to keep things tidy.

---

### Question (2)

-  2.1 If you wanted to install cookiecutter with conda, what would the steps be?

------------

## Part 3: Understand the Conda Environment (3)

`datalad` works as long as it is in the search path.  When you **activate** a conda environment, the tools you installed in that environment are in your search path.  When you **deactivate** the conda environment, those tools can no longer be found.  This is the point of an isolated environment.

You already have two useful aliases for activating and deactivating conda environments (from tutorial_03_unix_configuration): 

- alias cact='conda activate'
- alias cdact='conda deactivate' 

## Deactivate conda (3)

```bash
cdact
```

* This exits the environment where datalad is installed.

-  Check for datalad again:

* ```bash
   which datalad
   ```

* Datalad cannot be found.

--------

## Activate the conda environment (3)

```bash
cact datalad
```

* This changes your prompt to include **(datalad)** and enters the environment where datalad is installed. 

* Check for datalad, you should find it now:

   ```bash
   which datalad
   ```

* Tools you install with conda are isolated from the rest of the system in their own little sandboxes.  This is a good thing because it prevents them from conflicting with each other. In this example, you installed `datalad` into its own conda environment.



---

### Question (3)

-  3.1 If you are not IN your Datalad environment, is `datalad` in your path?

----------

## Update a Package (3)

- Datalad is undergoing rapid development, so you may want to update it from time-to-time.
- From the conda environment where datalad is installed:
  ```bash
  conda update -c conda-forge datalad
  ```
- Check the datalad version:
  ```bash
  datalad --version
  ```
-------------
### Question (3)

-  3.2 If you are concerned that your environment is old and needs updating, what can you do?

------------

## Clean up Disk space (3)

You have limited space on Google Cloud Shell (5 GB in your home directory). 

-  See how much space you have in your home directory

   ```bash
   df
   ```

-  To clean up unused packages, tarballs and caches, run `conda clean`:

  ```bash
  conda clean --all
  ```

-  Again, use `df` to check how much space you have in your home directory.
-  You can run it any time.  The tools you want will still be there.

-  See how much space you have in your home directory

---

### Question (3)

-  3.3 Is it safe to run `conda clean -all`?

---

### Remove an Environment and its Packages (3)

You don't want to retain environments you are not using. The following command removes an environment and its packages. 

**Warning**: Don't remove your datalad environment!  You'll need it later!

```bash
conda remove -n somepackage --all
```

This will not clean up unused files, so you still need to run conda clean.

---

### Question (3)

-  3.4 How would you remove the conda cookie environment?

---------

## Part 3: Summary (3)

You learned that datalad is only available in the path when the conda environment is active. That is, conda environments are isolated from each other and the rest of the operating system.

**TIP**: Although this does not always work well for all packages, you can create an **alias** that starts the conda environment and then invokes the tool, e.g.,: 

````bash
alias datalad='conda activate datalad; datalad'
````

Datalad is a large package that undergoes frequent updates, so you learned how to update a package. You also learned how to remove an environment and its packages, and how to get conda to clean up files it is not using. 

-----------
## Conclusion
You learned to install the miniconda package manager.  This allows you to install tools even in an environment like the HPC where you do not have administrator permissions.

Conda is frequently used to create separate environments for tools (especially Python tools) that might otherwise conflict with each other or the operating system. As you install more tools, conda provides a way to experiment safely without destroying your operating system.

---







