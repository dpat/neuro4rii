

# Tutorial for JSON Manipulation: JQ and JD

## Cloud Shell Lessons

-  JQ: View Files (1)
-  JQ: Delete Properties (2)
-  JQ: Add and Modify Properties (3)
-  JQ: Useful Scripts (4)
-  JD: Compare files (5)
-  JD: Patch files (6)


-----------------------
### Setup

In your home directory, download and unzip the sample dataset:

```bash
wget https://bitbucket.org/dpat/neuro4rii/raw/main/data/json_tutorial_data.zip; unzip json_tutorial_data.zip; rm json_tutorial_data.zip
```

Get the `jd` binary and set it up:

```bash
wget https://github.com/josephburnett/jd/releases/download/v1.4.0/jd-amd64-linux -O bin/jd; chmod a+x bin/jd
```

Download useful scripts (jdel and jadd):

```bash
wget https://bitbucket.org/dpat/tools/raw/master/LIBRARY/jdel -O bin/jdel; chmod a+x bin/jdel; wget https://bitbucket.org/dpat/tools/raw/master/LIBRARY/jadd -O bin/jadd; chmod a+x bin/jadd
```

---

## Check Installations

Check that each tool is available by typing its name.  You should get a help message (rather than a complaint that it cannot be found):

```bash
echo ""
echo ""
echo "##########################################"
echo "JD HELP"
jd
echo "##########################################"
echo "JDEL HELP"
jdel
echo "##########################################"
echo "JADD HELP"
jadd
```

---

## JQ: View Files (1)

- Navigate to `json_tutorial_data/bids_sidecars`
  ```bash
  cd json_tutorial_data/bids_sidecars
  ```

- Use the dot operator to pretty print a JSON file:
  ```bash
  jq . sub-test_asl.json
  ```
  
- Display the pretty printed JSON file, sorted alphabetically with `-S`:
  ```bash
  jq -S . sub-test_asl.json
  ```
  
- Display the JSON file printed in compact mode `-c`:
  ```bash
  jq -c . sub-test_asl.json
  ```

---------

## Filter the values (1)

- Follow the dot with a key to view just the value associated with that key:

  ```bash
  jq .EchoTime sub-test_asl.json 
  ```

- View values associated with multiple keys using a comma-separated list protected by single quotes:
  ```bash
  jq '.EchoTime, .RepetitionTime' sub-test_asl.json 
  ```

- View properties in a valid JSON object `{}` protected by single quotes: The output includes the keys and values wrapped up as a JSON object:
  ```bash
  jq '{EchoTime, RepetitionTime}' sub-test_asl.json  
  ```

----------

## Use built-in jq functions (1)

- View the **length** of a value (This may be especially handy for arrays):

  ```bash
  jq '.PostLabelingDelay | length' sub-test_asl.json 
  ```

- Determine **whether a key exists** in the JSON file:
  ```bash
  jq '(has("IntendedFor"))' sub-test_asl.json 
  ```
  will report `false` because the key is not in the file.  However,
    ```bash
  jq '(has("Modality"))' sub-test_asl.json 
    ```
  will report `true`.

-  View just the **keys**:

   ```bash
   jq keys sub-test_asl.json 
   ```

-  Or, view just **string** values:

   ```bash
   jq '.[]|strings' sub-test_asl.json
   ```

- Just like viewing strings, you can also view booleans, arrays, or numbers.

-----

### Think about this (1)

How would you view: 

-  just booleans? 
-  just arrays? 
-  just numbers?

-------------------

## Solution (1)

-  View just boolean values:

   ```bash
   jq '.[]|booleans' sub-test_asl.json
   ```

- Or, view just array values:

   ```bash
   jq '.[]|arrays' sub-test_asl.json
   ```

- Or, view just number values:

   ```bash
   jq '.[]|numbers' sub-test_asl.json
   ```

--------------------
## Part 1: Summary (1)

-  JQ is ubiquitous on Unix systems.  
-  You'll find it on Google Cloudshell and the U of A HPC (for the latter, you have to be in an interactive session).  
-  Embed `jq` commands in bash scripts to loop over JSON files, checking individual properties.

------------------
## JQ: Delete Properties (2)

-  The built-in function `del` removes a key and its corresponding value.  Because the output of `jq` goes to the terminal, instead of changing the actual file, you can continue to experiment on sub-test_asl.json.  That is, `jq` will show you results but will not actually change the underlying file. 

-  Look at the output to confirm the expected changes are being made (`Modality` is at the top of my file).

-  Both the dot operator and parentheses are used in delete statements:
   ```bash
   jq 'del(.Modality)' sub-test_asl.json
   ```
   
- Delete multiple properties by putting them in a comma-separated list inside the parentheses:
  ```bash
  jq 'del (.MagneticFieldStrength, .Modality, .PostLabelingDelay )' sub-test_asl.json
  ```

-----------

## Pass a bash variable to `jq` (2)

- To try this, define a variable:

  ```bash
  myvar=Modality
  ```

-  Check that the variable contains the value`Modality`:

   ```bash
   echo $myvar
   ```

-  Wrap the variable in single quotes, surrounded by double quotes: `"'"$myvar"'"` and delete the Modality property (note you'll have to copy and paste this one because teachme can't handle all the quote marks):

    ```terminal
    jq 'del(."'"$myvar"'")' sub-test_asl.json
    ```

-  Okay, that doesn't seem very exciting because you did the same thing earlier when you deleted the Modality.  However, this is going to facilitate a more flexible scripted solution where you can pass any key to the command.

---

### Think about this (2)

-  How could you change `myvar` to be `MagneticFieldStrength`?
-  How would you test that you could use the variable to delete `MagneticFieldStrength`?

--------------

## Solution: (2)

- Redefine the variable:

   ```bash
   myvar=MagneticFieldStrength
   ```

-  Check the variable:

   ```bash
   echo $myvar
   ```

-  Delete the variable, the same way as before:

   ```terminal
   jq 'del(."'"$myvar"'")' sub-test_asl.json
   ```



-------------

## Part 2: Summary (2)

jq makes it relatively easy to delete properties from the JSON file.

----

## Part 3: Add and Modify Properties with JQ  (3)

- Revise the value of an existing property by assigning the new value with `=`:
  ```bash
  jq '.Modality="MR_New"' sub-test_asl.json
  ```
- Or, add a new key and value:
  ```bash
  jq '.Fred="new_value"' sub-test_asl.json
  ```
- To modify multiple properties at once, use the pipe `|` (if you use a comma, only the first property gets changed):
  ```bash
  jq '.MagneticFieldStrength = 6 | .Modality="HyperMR" ' sub-test_asl.json
  ```

--------------

### Think about this (3)

How would you change three values at once?

--------------

## Solution: (3)

Unsurprisingly, you can keep adding pipes to the statement:
```bash
jq '.MagneticFieldStrength = 6 | .Modality="HyperMR" | .Manufacturer="Fred"' sub-test_asl.json
```

-------------

## Part 3: Summary (3)

-  In addition to viewing and deleting properties from a JSON file, `jq` allows you to add values.  
-  If you have a lot of changes to make, this approach is faster than uploading each file to the JSON-Editor Interactive Playground, and less error-prone than making the changes manually in a text editor.  

-  But, stay tuned for the next lesson on `jd` to learn more tricks!

## Part 4: Useful Scripts for JQ (4)

- By default, `jq` returns results to **standard output** (i.e., the terminal).  
  
- Often, what you want is to modify the input file. To facilitate using variables to add and delete properties in the input file, I've provided two scripts, `jdel` and `jadd` which you've already set up.
  
- Copy `sub-test_asl.json` to `test.json` before working with actual transformations:

   ```bash 
   cp sub-test_asl.json test.json
   ```

- `jdel` deletes a named property (key and value) from the JSON file specified:

   ```bash
   jdel Modality test.json
   ```

- Confirm that `test.json` is now missing the Modality property!

- `jadd` adds or modifies a property in the JSON file specified.  Here, it adds the missing property back into `test.json`:

  ```bash
  jadd Modality 3 test.json 
  ```

- Confirm that `test.json` now has the Modality property (it will be at the bottom of the JSON file)

- `jadd` can also add a property with a string value to the JSON file specified, by adding an additional `s` argument specifying that the value is a string:

  ```bash
  jadd Modality MRIscanner test.json s
  ```

--------------

### Think about this (4)
-  `jadd` can handle other values, like Booleans and arrays.  
-  How would you use `jadd` to add an array of numbers? an array of strings? a boolean?

--------------

## Solution: (4)

`jadd` provides help to explain each possible case.        

Try each command, and then confirm that `test.json`has changed:    

- Add/modify a **number** value, e.g.,
  ```bash
  jadd Modality 3 test.json
  ```
  
- Add/modify a **boolean**, e.g.,
  
  ```bash
  jadd Modality true test.json
  ```
  
- Add/modify an **array of numbers** (wrap the array in single quotes), e.g.,:
  ```bash
  jadd Modality '[1,3,5]' test.json
  ```
  
- Add/modify an **array of strings**: wrap the array in single quotes, and double quote the strings, e.g.,
  ```bash
  jadd Modality '["a","b","c"]' test.json
  ```
  
- Add/modify a string, by adding the 4th argument, s:
  ```bash
  jadd Modality MRI test.json s
  ```


-------------

## Part 4: Summary (4)

- JQ is quite sophisticated and the syntax can get complex.  
- In general, output gets printed to the terminal, but often you want to modify an actual file: The scripts `jdel` and `jadd` facilitate revising a JSON file by deleting or adding and modifying the properties you specify.
- The `jq` lesson focused on the kinds of JSON manipulations that might be handy for editing BIDS sidecars. 
- This should get you started with viewing and filtering JSON data, querying for particular fields or the length of a value, deleting properties and adding or modifying properties. 
- Two scripts are provided to make the processing easier.

------------
## JD: Compare Files (5)

- `jd` compares two JSON files, much like Unix `diff`, but `jd` understands the structure of JSON files. 
  ```bash
  jd sub-test_asl_orig.json sub-test_asl.json
  ```

- This lists what has changed in file2 (sub-test_asl.json) relative to file1 (sub-test_asl_orig.json).

- Each key is listed with an initial `@`.

- Values that have been added are on a line prefixed with a `+`.

- In this case, the comparison only adds values.

- However, the comparison could be more complex and include deleted properties or modified values:
  ```bash
  jd sub-test_asl_orig.json sub-test_asl2.json
  ```

- In this more complex case:    
   1)  MagneticFieldStrength was modified from `3` to `12`,        
   2)  `Modality` was deleted,                
   3)  `"BackgroundSuppression": true` was added.   

----

### Think about this (5)

Add values with a plus: `+`.  How are modification and deletion indicated?

## Solution (5)

-  `-` minus is used to indicate deletion.
-  `-` and `+` are used together to indicate modification.

--------------------
## Part 5: Summary (5)

-  `jd` is very similar to Unix `diff`, except `jd` understands the structure of JSON files.  
-  This is important because `diff` is dumb...it compares lines of text without any understanding of their content. 
-  And because JSON files are inherently unordered, `diff` does not work reliably to identify real changes in content. 

------------------
## Patch Files (6)

-  JD allows you to revise a set of properties (additions, deletions, and modifications) in a single step.  
-  It requires a patch file containing the differences between two files.  
-  This difference file (patch) can then be used to modify a third file.

-------------

### Create a Patch File (6)

-  You  generated the contents of the patch when you compared two files.  
-  Use the output flag `-o` to save that content to a file you can use as a patch (called `patch` here, but you can call it anything you want):
    ```bash
    jd -o patch sub-test_asl_orig.json sub-test_asl.json
    ```
- Confirm that the file `patch` was created and contains the diffs.
-----
## Apply a patch (6)

- Before applying the patch, copy the file so you can compare afterwards. 
  ```bash
  cp sub-1006_T1w.json sub-1006b_T1w.json
  ```
  
- The patch file can be used to revise any other json file: Follow the `-p` flag with the name of the patch file and then the file-to-be-revised:
  ```bash
  jd -p patch sub-1006b_T1w.json
  ```
  
- By default, just like `jq`, `jd -p patch sub-1006b_T1w.json` outputs compact results to the terminal (standard out).  

- This is NOT VERY SATISFYING!

- To actually revise the file, you need to use the same trick we used with `jq`.
  
- Output the file to a temporary new file and then, in a second step, move that temp file to the file-to-be-revised (yes, override permissions if necessary):
  
  ```bash
  jd -p patch sub-1006b_T1w.json >> tmp.$$.json && mv tmp.$$.json sub-1006b_T1w.json
  ```
-----

### Think about this (6)

How can you confirm that sub-1006_T1w.json and sub-1006b_T1w.json are now different?

## Solution (6)

```bash
jd sub-1006_T1w.json sub-1006b_T1w.json
```

-----

### There is a catch (6)

-  A patch will fail if the file-to-be-revised is missing a property that the patch is designed to delete or modify! 
-  For example, if the patch was designed to delete the property `Modality`, but that property was already missing from the file-to-be-revised, then `jd` would fail:

    ```terminal
    2022/01/04 16:41:15 Found  at [Modality]. Expected "MR".
    ```

-------------
## JD: Summary (6)

- In some circumstances, like conforming to the BIDS metadata specification for ASL files, it may be necessary to add several JSON properties. 
- This could be done one at a time, manually, but it is faster and more reliable to generate a patch containing the modifications and then apply that patch to all the relevant files.

- `jd` is a valuable tool for comparing and modifying JSON files. 
- Together with knowledge about JSON schemas and `jq`, knowledge of `jd` can facilitate the process of modifying BIDS sidecar files.

---

