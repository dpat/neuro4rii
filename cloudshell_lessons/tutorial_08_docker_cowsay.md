# Tutorial 8: Docker Cowsay

**cowsay** is one of the simplest and smallest Docker containers you can download, run and build. When you run it, it produces a weird saying from its collection of sayings.

## Cloud Shell Lessons
* Install and Run a Docker Container: godlovedc/lolcow (1)
* Images vs containers (2)
* Dockerfile recipe (3)
* docker build (4)



---

### Docker Links (3)

* [Dockerhub](https://hub.docker.com/)  
* [Play with Docker](https://www.docker.com/play-with-docker)

-------------------

## Docker Containers (1)
* Docker containers are like tiny virtual machines, and most are freely available from [dockerhub](https://hub.docker.com/).    

* **Caution**: Anyone can put an image on DockerHub, so the official images are safest! 

* Docker help is always available. Type the following:
  ```bash
  docker
  ```
  
* In cloud shell, the Docker files you install will be gone the next time you restart because they are installed in a hidden system area. 

---

## Question

-  1.1 How can you display docker help? 



------------------

## Download and Run the lolcow Docker image (1)

* Download and run a Docker **image** in one step:
  ```bash
  docker run -it --rm godlovedc/lolcow
  ```
* You see an ascii cow and strange saying.  
* Run the command again to see the cow produce another saying (hit the up arrow and then enter to look through your history and rerun a command).

-------------
### lolcow References (1)
* [dockerhub: lolcow](https://hub.docker.com/r/godlovedc/lolcow)   
* [github: lolcow](https://github.com/GodloveD/lolcow/blob/master/Dockerfile)  

----------------
### Understand the Docker command you ran (1)
* **docker run** first looks at locally available Docker images.  If it does not find the requested image (godlovedc/lolcow), then it downloads it.
  * By default, docker downloads from Dockerhub, which is like an **app store for Docker images**!
* **-it** runs interactively, and **--rm** cleans up afterwards. These are almost always the right choices.

----------------
## Docker Images vs Containers (2)

* Docker distinguishes between the **container image** (a.k.a **image**) and the **container instance** (a.k.a **container** or **instance**). I'll use the terms **image** and **container** because Docker commands use those terms:
* Docker images are like items on a menu. 
  * If you know the name of the item on Dockerhub, use **run** to order the item. 
  * You can also list the Docker images that you have already downloaded.
* When you **run** an image, you are ordering from the menu. You can order as many containers as you want without conflict, just like you could order as many cheeseburgers as you like.
* When you finish your container, it is sitting there (like a dirty plate on the table). You need to make sure the container is removed, or you'll fill up your storage space with old containers (like a pile of dirty plates on the table).  

---

## Question

-  2.1 What is a Docker image?

-  2.2 What is a Docker container?



----------------

## Listing and Handling Images and Containers (2)

* List the Docker **images** you have available:
  ```bash
  docker image ls
  ```
* At the moment, there is only one image. This is like the item on the menu. 
* In the previous section, you invoked `docker run` and then you removed the finished container afterwards by using the `--rm` flag. This is like cleaning the dirty plates off the table.
* List the Docker **containers** :
  ```bash
  docker container ls -a
  ```
* There are no containers because you removed them after running them. That's good, very tidy!
* Run *lolcow* without removing the container:
  ```bash
  docker run -it godlovedc/lolcow
  ```
* Recheck for containers that have not been removed:
  ```bash
  docker container ls -a
  ```
* You should see one. This is like a dirty plate on the table. 
  It'll look something like this:
  ```terminal
  CONTAINER ID  IMAGE  COMMAND  CREATED   STATUS   PORTS   NAMES
  0b2debf97a27   godlovedc/lolcow   "/bin/sh -c 'fortune…"   
  9 seconds ago   Exited (0) 8 seconds ago    frosty_vaughan
  ```
* Mine is named **frosty_vaughan**.  
* It has the CONTAINER ID: **0b2debf97a27**
* **Yours will have a different NAME and ID**.
* Remove the container using either its NAME or CONTAINER ID, e.g., 
  ```terminal
  docker container rm frosty_vaughan
  ```
  or  
  ```terminal
  docker container rm 0b2debf97a27
  ```

* **Note**: In this command you use **rm**, but in the *docker run* command, you had to use **--rm**. The effect is the same, but the flags are a little different. 
* Make sure the container has been cleared away:
  ```bash
  docker container ls -a
  ```
* Good job. You have downloaded and run a container.  You have explored the difference between Docker images and Docker containers. Next, you'll build a Docker image from a recipe.  

---

## Question

-  2.3 What is the problem with this command `docker run -it godlovedc/lolcow`?

--------------
## Create a Dockerfile Recipe (3)
* Docker images are created from a recipe.  The recipe is a text file named **Dockerfile**
* To build a Docker image from a recipe, first, ensure you are in a fresh directory. 
* Docker takes stock of everything in the build directory, so it will take a long time to build if the directory contains lots of files and subdirectories.
* Create a new directory **cow** and *cd* to it:
  ```bash
  mkdir cow
  cd cow
  ```
* Create Dockerfile and edit it:
  ```bash
  touch Dockerfile
  edit Dockerfile
  ```
* Copy and paste the following [Docker recipe](https://bitbucket.org/dpat/neuro4rii/raw/main/cloudshell_lessons/Dockerfile) into the editor. 
* **Save** the file.

---
## Understand the Dockerfile Recipe (3)

* Dockerfiles use their own syntax described in the [Dockerfile Reference](https://docs.docker.com/engine/reference/builder/).
* This Dockerfile has 5 lines: 
* The first line, **FROM ubuntu:16.04**, tells Docker to build on top of an existing Docker image, a copy of the Linux operating system, Ubuntu version 16.04. The Ubuntu image is also available on Dockerhub.
* The **RUN** line tells Docker to use the Ubuntu package manager, **apt-get**, to add any security updates and add several game packages (fortune, cowsay, and lolcat) to the Docker image.
* The **ENV** lines set environment variables: 
  * The **PATH** is set to include the games you just installed. 
  * **LC_ALL=C** sets the *locale* to use the default ASCII English characters. This is especially important fro drawing the cow correctly.
* The **ENTRYPOINT** line tells *docker run* what program to run:  
  * **fortune | cowsay | lolcat** means run the *fortune* program, and then pipe **|** its input into the *cowsay* program, and then pipe **|** those results into *lolcat*.

---

## Question

-  3.1 What is a Dockerfile?

-  3.2 Do Dockerfiles have their own special syntax?

------------------

## Build the Docker Image from the Dockerfile Recipe (4)

* The **build** command generates a lot of messages as it builds! 
* Build the recipe with the following command to Docker
  ```bash
  docker build -t mycow .
  ```
* You will need to authorize the build. 
* By default, **docker build** looks for the file called **Dockerfile** and builds that. 
* **-t mycow** tags the Docker image with the name *mycow*, but you could tag the image with any name you like. 
* The final dot `.` tells Docker it should look in the *present working directory* to find the Dockerfile to build.

------------------
* Run mycow:
  ```bash
  docker run -it --rm mycow
  ```
* List the Docker **images** you have available:
  ```bash
  docker image ls
  ```
  You should see something like:
```terminal
REPOSITORY        TAG      IMAGE ID      CREATED         SIZE
mycow             latest   14f64585ecff  12 seconds ago  242MB
godlovedc/lolcow  latest   577c1fe8e6d8  3 years ago     241MB
```
* You now have at least two local Docker images: The one you downloaded and the one you built locally.

--------------
## Summary

You've learned:
* To run and build a Docker image 

* To distinguish between Docker images and Docker containers

* To understand the internal structure of the Dockerfile recipe

* To build a docker image from a recipe (Dockerfile)

   

---

