# Tutorial 02: Unix File and Directory Manipulation

## Cloud Shell Lessons

- File and Directory Manipulation (1)
- Recursion and More Directory Manipulation (2)
- Naming (3)
- History and Command Completion (4)
- Permissions (5)
- Pattern Matching: Globbing (6)



--------------
## Prerequisites

* This tutorial assumes you've followed the [Google Cloud Shell doc](https://docs.google.com/document/d/1c99PwEr_DDRryl-bvoEzAJe3LBV3QuL__bxm7X_eUJ0/export?format=pdf) to set up the environment.

* When you remove, create or rename files and directories, use **ls** to confirm the changes were made.
  * Use `ls -1` if you prefer that format. 
  * Use `ls -R` to list nested directories and files.
  
* When you navigate, use `pwd` to check where you are.

* This tutorial assumes you have completed Unix 1 and Unix 2 OpenClass lessons, or equivalent. In particular, you should understand the directory tree.

* Remember, if you are meant to run a command there will be a copy icon on the upper right, like this:

   ```bash
   pwd
   ```

   On the other hand, if text is just set off in a box, you are meant to look at it, but not try to run it:

   ```terminal
   pwd
   ```

   

---

## Class Credit

If you are taking COGS 500, you need to record your terminal session and turn it in.

```bash
script unix2_${USER}_${DATE}.txt
```

-----------------
## Go Home! 
* Use `cd` with no arguments to go home: 
  ```bash
  cd
  ```
  
* Because you have just used `cd`, you should print you present working directory.

* Clear the terminal window to keep things tidy:
  ```bash
  clear	
  ```

-------------
## Part 1: File and Directory Manipulation (1)

* `touch` creates a file if it does not exist, or updates its timestamp if a file or directory with that name does exist.
* `mkdir` (make directory) makes a directory.
* `mv`  (move) is  used to rename a file (without necessarily moving it anywhere).   
* `mv` also moves a file from one location in the directory tree to another. 
* `cp` (copy) copies a file from one location in the directory tree to another. 
* `rm` removes a file permanantly!  Use with caution....there is no trashcan!

---------------------

## Make a File (1)

* Create a new file with `touch`:
  ```bash
  touch readme.txt
  ```
* List the files to confirm that **readme.txt** was created.

----------------------
### Think about This (1)

How would you create two more files: **HOWTO.md** and **CHANGELOG.md**?

------------------

## Solution: Create Files (1)

You can issue two separate commands: 

```bash
touch HOWTO.md
touch CHANGELOG.md
```

You can even do this in one command! (*If you run this, touch just changes the timestamp on the files, because you have already created the files*)

`````bash
touch HOWTO.md CHANGELOG.md
`````

-----------

## Make a Directory with `mkdir` (1)

Use `mkdir` (make directory) to make a directory called **tooltest**:

```bash
mkdir tooltest
```

---------

### Think about this (1)

How would you make another directory, **code**, inside **tooltest**?

## Solution: Make Directory (1)

There are several ways to do this:

- Navigate to tooltest: `cd tooltest` and  make a new directory: `mkdir code`

- Make the directory without navigating to tooltest: `mkdir tooltest/code`

- If you had not yet created **tooltest**, then you could use the `-p` flag to create both directories at once!

  ```bash
  mkdir -p tooltest/code
  ```

--------------

## Rename a File with `mv` (1)

- Oops, most people capitalize README and make it a markdown file instead of a `*.txt` file.

- Use `mv` to rename **readme.txt** to **README.md**:
  ```bash
  mv readme.txt README.md
  ```

- List the files to confirm that **readme.txt** is gone and **README.md** was created.  

- If **README.md** had already existed, it would be overwritten and gone forever!!

--------

## Copy a File with `cp` (1)

- You need a **README.md** in the **tooltest** directory. 

- Use `cp` (copy) to copy  **README.md** into the subdirectory **tooltest**. 

   ```bash
   cp README.md tooltest
   ```

-----------

### Think about this (1)

If you are in your home directory (the parent of the **tooltest** directory), how could you copy the **README.md** into the **code** subdirectory??

----------

## Solution: Copy (1)

Again, there are several solutions, here are two of them:

You can issue two separate commands (this will put you IN the tooltest directory): 

```terminal
cd tooltest
cp README.md code
```

You can also do this in one command (this will leave you in your home directory--do this now):

`````bash
cp tooltest/README.md tooltest/code
`````

-----------------
## Move a File to a Different Directory (1)

- In addition to renaming a file, `mv` (move) can move a file.

- `mv` is similar to `cp`, except it does not leave the original.  From your home directory, you can run this:

  ```bash
  mv HOWTO.md tooltest
  ```

----------

### Think about This (1)

- How would you move **CHANGELOG.md** into the **tooltest** directory?
- What would have happened to **HOWTO.md** if **tooltest** did not exist?

------------

## Solution: Move Files (1)

- From your home directory, move **CHANGELOG.md**:

  ```bash
  mv CHANGELOG.md tooltest 
  ```

- What would have happened if tooltest did not exist?
  **HOWTO.md** would have been renamed **tooltest**!
  
- *If you were in the tooltest directory, then to move **CHANGELOG.md**, you could have done this:*
  
  ```terminal
  mv ../CHANGELOG.md .
  ```

-  This says: move CHANGELOG.md from my parent directory to here. Other solutions exist.

-  Use `ls -R` to list the files in `tooltest`

   ```bash
   ls -R tooltest
   ```
- You should see this:
   ```terminal
   tooltest/:
   CHANGELOG.md  code  HOWTO.md  README.md
   
   tooltest/code:
   README.md
   ```
   
   

## Remove a File with `rm` (1)

*  From the **tooltest** directory, remove a file with `rm` (remove):
   
*  **NOTE: this means you have to navigate to the tooltest directory!!**  If you are in your home directory, you will see an error message.
   
    ```bash
    rm README.md
    ```
  
* List the contents of the tooltest directory to confirm that **README.md** is gone.

* **Warning**: `rm` is permanant! When you remove something at the Unix commandline, you'd better be sure. There is no trashcan.

* Create a new **README.md**

  ```bash
  touch README.md
  ```

  

--------------
### Think about this (1)
- Choices you make at the command line are not easy to undo (and sometimes impossible). So, you need to adopt strategies that minimize risk.

* Given this, is it safer to copy and then remove, or to move files?

--------------
## Solution: Minimize Risk (1)
* Although it takes extra steps, copying is safer than moving files, because copying leaves the original.   
* After you confirm a successful copy, you can remove the original. 

---------------
### Stay Safe with `-i` (1)
* `mv` and `cp` will overwrite files if the destination file already exists.  
* `rm` will remove a file without asking and a directory even if it is not empty.
* Use the `-i` (interactive) flag: `cp` and `mv` allow you to add the interactive flag to your command so you are always prompted before overwriting a file or directory at the destination.
* Copy a file using the interactive flag:
  ```bash
  cp -i README.md R2.md 
  ```
* The first time, it copies without incident. This is because the destination file does not exist. Try it again:
   ```bash 
  cp -i README.md R2.md 
  ```
* The second time, the destination file DOES exist, and you should see a warning:
  ```terminal
  cp: overwrite 'R2.md'?
  ```
* Type `n` for *no*. (`y` is *yes*)

---------------
## Part 1: Summary (1)
* `clear` cleans up the terminal so it is more pleasant to view.
* `touch` creates a file if it does not exist, or updates the timestamp of a file or directory if it does exist. 
* `mkdir` makes a directory.
* `mv` is frequently used to rename a file (without necessarily moving it anywhere). It also moves a file from one location in the directory tree to another. 
* `cp` copies a file from one location in the directory tree to another.
* `rm` removes a file permanantly!  Use with caution....there is no trashcan.
* `-i`, the interactive flag, prompts you before overwriting a file at the destination
  * Use `-i` for both `cp` and `mv`.  
  * You can even use `-i ` for `rm`, but this gets tedious if you want to remove lots of files.

----------------
## Part 2: Recursion and More Directory Manipulation (2)
* `mkdir` makes a directory
* `mv` renames a directory (not just a file)
* `cp -R` copies a directory (`-R` means recursive)
* `rm -R ` removes a directory (`-R` means recursive)

----------------
## Make a Nested Directory (2)
* Navigate to your **tooltest** directory.

* Use `mkdir` with the `-p` (parent) option to create a whole directory structure. Intermediate directories are created as needed:
  ```bash
  mkdir -p data/inpputs
  mkdir -p data/outputs
  ```

* Yes, I know there's a typo!  You will see how to correct it in the next step.

* List the contents of **tooltest** recursively to see the nested directory structure.

  
--------------------
## Rename a Directory with `mv` (2)
* Just like renaming a file,  `mv` can rename a directory.  Correct the previous typo:
    ```bash
    mv data/inpputs data/inputs
    ```
* Check that the directories are named correctly:
    ```bash
    ls -R
    ```
--------------------
## Copy a Directory Recursively with `cp -R` (2)
* Unlike copying a file, you need the `-R` (recursive) flag to copy a directory, because copying always applies to both the directory AND all of its nested contents.
  ```bash
  cp -R data data2
  ```
* Use `ls -R` to confirm that **data** and **data2** both contain the same subdirectories:

  ```terminal
  tooltest$ ls -R
  .:
  CHANGELOG.md  code  data  data2  HOWTO.md  R2.md  README.md
  
  ./code:
  README.md
  
  ./data:
  inputs  outputs
  
  ./data/inputs:
  
  ./data/outputs:
  
  ./data2:
  inputs  outputs
  
  ./data2/inputs:
  
  ./data2/outputs:
  ```

## Move a Directory and its Contents (2)

* Move **data2** inside the **data** directory:

  ```bash
  mv data2 data
  ```

* Confirm that data2 and its subdirectories are now nested inside data: 

  ```terminal
  ls -R
  .:
  CHANGELOG.md  code  data  HOWTO.md  R2.md  README.md
  
  ./code:
  README.md
  
  ./data:
  data2  inputs  outputs
  
  ./data/data2:
  inputs  outputs
  
  ./data/data2/inputs:
  
  ./data/data2/outputs:
  
  ./data/inputs:
  
  ./data/outputs:
  ```

- If the **data** directory did not exist, **data2** would have been renamed instead.
- Interestingly, `mv` does not have a recursive option, but still works with directories.

## Remove a Directory Recursively with **rm -R** (2)

```bash
rm -fR data/data2
```
------------

### Think about this

* What do the flags `-fr` mean to the `rm` command?  
* How would you find out?

-----------------

## Solution: Look up flags (2)

* Look at the `man` page for `rm`

  ```bash
  man rm
  ```

* Find the flags `-f `and `-r` in the manual pages.

* Use &uarr; and &darr; to navigate the `man` page and `q` to quit.  

* **Note**: Some flags available to `rm` on the Linux man page displayed by Google Cloud Shell (e.g., the capital i:  `I`) are not available on the Mac because these are slightly different versions of Unix!

* Google is your friend, you can look up the `rm` command in a browser.

----------------------------

## Think about this (2)

* What does it mean for a command to be recursive?
* Does it make sense for all commands to have a recursive option?
* What happens when you try to remove a directory without the recursive flag?
* Why is `cp` recursive but  `mv` is not recursive?

--------------------
## Recursiveness (2)
* A recursive command is one that is repeated. For example, if you list the contents of your home directory **recursively**, then you list every nested level under the present working directory instead of just listing the directories and files at the top-level.

* The recursive flag is usually `-R` and occurs in commands like   
  `cp` (copy); `ls` (list); `rm` (remove).
  
* What happens when you try to remove a directory without the recursive flag
  
  ```bash
  rm data2
  ```
  ```terminal
  rm: cannot remove 'data2': Is a directory
  ```
  
* Why is `cp` but not `mv` recursive?

  I found the following [explanation](https://unix.stackexchange.com/questions/46066/why-unix-mv-program-doesnt-need-r-recursive-option-for-directories-but-cp-do) helpful:
  *A true copy of a cup of coffee is not an empty cup - you have to recursively copy not only the cup, but all of its contents (the coffee) too. However, when you move a cup of coffee you don't have to move the contents separately - the contents move with the container naturally.*

----------------------
## Part 2: Summary (2)
* Unlike files, directories can contain nested content.
* `mkdir` makes a directory
* Commands that operate on this nested content use the `-R` (recursive) option.
* `cp -R` copies a directory (`-R` means recursive)
* `rm -R` removes a directory (`-R` means recursive)
* Interestingly, `mv` renames or moves a directory (as long as the specified destination does not exist) but `mv`is not recursive.

----------------------
## Part 3: Naming (3)
To avoid frustration, follow these two rules:
* **DO NOT use spaces in names of files or directories**!
* Be aware that Linux is case-sensitive, although other operating systems may not be.

------------------------
## Names with Spaces (3)
* Whether you are making a file or a directory, you should **NEVER PUT SPACES IN THE NAMES**. To see why, try to make a file with a space in the name:
  ```bash
  touch antonym spreadsheet
  ```
-------------------

### Think about this (3)

Did a file called *antonym spreadsheet* get created? What happened?

------------------------
## Solution: Spaces in Names (3)
* The present working directory now contains one file called **antonym** and one called **spreadsheet **but not a file called "antonym spreadsheet"!
* This happened because the space is meaningful!  Linux thought you wanted to create two separate files.

------------------------
## Case-sensitive Names (3)
* Make files with different capitalization:
  ```bash
  touch dataset Dataset DATASET
  ```
* This works on Linux!
* This does NOT work on Windows.
* In the default configuration, this also does not work on Mac. 
* Do not rely on case to distinguish files and directories, because you are likely to move data between Linux machines and systems that are not case-sensitive.

-------------------
### Think about this (3)

From the tooltest directory, try the following command.  What happens and why? 

  ```bash
  cd Code
  ```
------------------
## Solution: Linux is case sensitive! (3)
* You should see a message like this:
  ```terminal
  -bash: cd: Code: No such file or directory
  ```
* Why? Because the directory is called **code** NOT **Code**, and Linux cares about capitalization!
* If you try the same command on a mac, the directory IS found, because the Mac is not case-sensitive.

-----------------
## Think about this (3)
* How do you remove the five extra files you created?    
*antonym  dataset Dataset DATASET spreadsheet*

-----------------
## Solution: Remove Extra Files (3)
The most efficient solution is to remove all of the files with one command. But you could remove them one at a time, or in groups if you wanted to.
```bash
rm antonym dataset Dataset DATASET spreadsheet
```
-----------------
## Part 3: Summary (3)
* Don't use spaces in file or directory names!
* Linux is case-sensitive. But, don't rely on differences in case or you will have trouble on other operating systems.
* Be aware of case when you specify a name: *Computing4ResearchLabs* vs *computing4researchlabs*.

--------------------
## Part 4: History and Command Completion (4)
What the terminal knows depends on:
* The version of Unix
* The command-line interpreter you are using (the shell)
* Your configuration options  
  

Learn to take advantage of some common features to reduce typing 

-----------------
## History (4)
* Retrieve a list of all the commands you typed recently.
  ```bash
  history 
  ```
* The number of commands in the history is determined by the variable *HISTSIZE* in the **.bashrc** configuration file. 

-----------------
* Rerun a command by typing **!** (bang) and the command number from your history, e.g.:
  ```bash
  !37
  ```
  
* Use the &uarr; and &darr; arrows to navigate through your history, one command at a time at the command prompt.  

* Stop when you get to the command you want. 

* You can edit the command before running it.

---------------------
## Command Completion (4)
* Make two directories with a really long name: 

  ````bash
  mkdir this_name_is_way_too_long
  mkdir this_name_is_also_long
  ````

* Navigate to the new directory like this:

* Start typing the name and **then hit the tab key**:

  ```bash
  cd th
  ```

* The terminal will complete the name (at least, up to the point where it can guess what comes next)

  ```bash
  cd this_name_is_
  ```

* Add characters to disambigaute the entry (e.g., `w` ) and hit **tab** again to complete the name.

  

-----------------
## Part 4: Summary (4)
* History and command completion make working at the command line easier.
* View all of the commands you typed with **history**
* Select a command from the history with `!` and the command number from the history list.
* Retrieve your history using the &uarr; and &darr; arrows.
* Command completion helps you finish a partially typed file or directory name by hitting **tab**.



-----------------
## Part 5: Permissions (5)
* When working on Unix systems (e.g., Mac or Linux), permisions are likely to be an issue. This is especially true if you share data with other people.
* List permissions with `ls -l` (lower case **L** for the long listing)
* Change permissions with **chmod** (change mode)

---------------
## List Permissions (5)
* From **tooltest**, use the long listing recursively to list permissions for your nested directory structure:
* **Note: You must navigate to the tooltest directory!!**
  
  ```bash
  ls -lR 
  ```
* If you are in the correct directory, you should see something like the following (but you will be the owner):
  ```terminal
  tooltest$ ls -lR
  .:
  total 12
  -rw-r--r-- 1 dkp dkp    0 Nov 27 19:27 CHANGELOG.md
  drwxr-xr-x 2 dkp dkp 4096 Nov 27 19:27 code
  drwxr-xr-x 4 dkp dkp 4096 Nov 27 19:24 data
  -rw-r--r-- 1 dkp dkp    0 Nov 27 19:27 HOWTO.md
  -rw-r--r-- 1 dkp dkp    0 Nov 27 19:27 README.md
  drwxr-xr-x 2 dkp dkp 4096 Nov 27 19:28 this_name_is_also_long
  drwxr-xr-x 2 dkp dkp 4096 Nov 27 19:28 this_name_is_way_too_long
  
  ./code:
  total 0
  -rw-r--r-- 1 dkp dkp 0 Nov 27 19:27 README.md
  
  ./data:
  total 8
  drwxr-xr-x 2 dkp dkp 4096 Nov 27 19:24 inputs
  drwxr-xr-x 2 dkp dkp 4096 Nov 27 19:24 outputs
  
  ./data/inputs:
  total 0
  
  ./data/outputs:
  total 0
  
  ./this_name_is_also_long:
  total 0
  
  ./this_name_is_way_too_long:
  total 0
  ```

-------------

## Read the Long Listing for README.md (5)

* The long listing uses the first character, **-**, to indicate that this is a file (not a directory or link etc.)
* You (the user) have **r**ead and **w**rite permissions on the file (**rw-**)
* The group has read permissions (**r--**)
* Others have read permissions (**r--**)
  ```terminal
  -rw-r--r-- 1 dkp dkp 0 Dec 18 02:46 README.md
  ```
--------------------
### Understand Permissions for README.md (5)
* **read** Everyone can view the file contents (cat)
* **write** No one except you can change the file.  
* **execute** No one can execute this file. That is, you cannot run this file as a script.

----------------------
## Read the Long Listing for the data  Directory (5)
```terminal
drwxr-xr-x 2 dkp dkp 4096 Dec 18 02:55 data
```
---------------------

### Understand Permissions for the data Directory (5)
* The long listing uses the first character, **d**, to indicate that this is a directory.

* **read** Everyone can view the directory contents (ls)
* **write** No one except you can make files or subdirectories in this directory.  
* **execute** Everyone can `cd` to this directory.

-----------------------
## Think about this (5)
* The permissions on the files **README.md** and **HOWTO.md** are the same!
* The permissions on the directories **code** and **data** are the same, but differ from the permissions on the files.
* Why does this happen?

----------------------

## Solution: Understand the umask (5)
* Default permissions are set by configuration files, and are generally not very permissive! 
* These permissions are listed in octal by default (see this handy [permissions calculator](http://permissions-calculator.org/) to learn more).
  ```bash
  umask
  ```
  ```terminal
  0022
  ```
 * You can see permissions in the form you have learned here by adding the **-S** flag:  
    ```bash
    umask -S
    ```
    ```terminal
    u=rwx,g=rx,o=rx
    ```
* You can set the default permissions in your shell configuration file.

-----------------------
## Change Permisions with `chmod` (5)
* Change permissions of individual files or directories with **chmod** (change mode).

---------------
### Common Examples of Changing Permissions (5)
* Make a file read-only, even for yourself, so that you don't overwrite it (List the permissions before and after the change).
  ```bash
  chmod ugo-w README.md
  ```

### chmod

**chmod** takes two arguments for a file: 

1) Desired permissions: 

* Who the permission changes apply to
  * u=user
  * g=group
  * o=other
* Whether permissions are being added or subtracted
  * permissions are removed with a minus **-** 
  * permissions are added with a plus **+**
* The type of permissions to add or remove:
  * r=read
  * w=write
  * x=execute

2) The file to change: **README.md**

The command above means: for user, group, and others, remove write permissions on **README.md**

**NOTE**: Instead of typing `ugo` you can use `a` for `all`.

----------------

### Think about This (5)

Because the only user had write permissions in the first place, there is another way to specify the desired permissions.  What do you think that would look like?

------------

## Solution: Simpler chmod statement (5)

- In this case, you would have gotten the same results if you'd simply removed the write permissions for the user:
  ```bash
  chmod u-w README.md
  ```
---------
### Think about This (5)
How would you remove Read permissions from CHANGELOG.md for everyone except yourself?

----------
## Solution: Remove Read Permissions (5)
Remove Read permissions from CHANGELOG.md for everyone except yourself?
```bash
chmod go-r CHANGELOG.md
```

------------------------

## Change Directory Permissions with `chmod` (5)

* Change permissions on a directory without changing the permissions on the contents:

  ```bash
  chmod ugo+w data
  ```

* The command above means: for user, group and others, add write permissions on the directory **data**.

* List the permissions to see the change: **data** is different,  but permissions on **inputs** and **outputs** were not changed

* To apply `chmod` recursively to a directory and its contents, add the `-R` (recursive) option

  ```bash
  chmod -R ugo+w data
  ```
--------
### Think about This (5)
How would you remove the write permissions for everyone except yourself on the directories and subdirectories of **data**?

---------
## Solution: Remove Directory Permissions (5)
Remove write permissions for everyone except yourself on the directories and subdirectories of **data**

```bash
chmod -R go-w data
```

------------------------
## Part 5: Summary (5)
* Permissions are likely to be an issue, especially if you share a project with other people. 
* In this section, you learned to list permissions with the long listing `ls -l` and change permissions with `chmod`.
* There are other ways to set permissions with numerical codes, but I  find this method most transparent. 
* Visit this handy [permissions calculator](http://permissions-calculator.org/) to learn more.

------------------------
## Part 6: Pattern Matching: Globbing (6)
Use special symbols to filter, create, and manipulate files (or directories) and reduce typing. The Bash pattern matching symbols overlap with regular expressions.

----------------

### The Most Common Pattern matchers (6)

The most common patterns are the star (a.k.a splat): `*` and the question mark `?`. These are helpful for filtering results.

- **Star**:  The star matches any number of characters. In the ** tooltest** directory:
  
  ```bash
  ls *.md
  ```
  will list only those files that have some number of characters followed by `.md`:
  
  ```terminal
  CHANGELOG.md  HOWTO.md  README.md
  ```

- **Question mark**: represents a single character, so it could be used to list files with 5 characters followed by `.md`:

  ```ls ?????.md```

  This will list only **HOWTO.md** because tthe other names have more characters.

The star and question mark are the patterns you'll use the most.

-------------
### Think about This (6)

How would you list all files starting with `R`?

How about all files that include the letter `O`?

----
## Solution (6)

```bash
ls R*
ls *O*
```

-------------------
## Less Common Pattern Matchers: Square Brackets [] (6)

* **Square brackets** `[]` match any one of the enclosed characters.   For example, if you only wanted to list markdown files containing an `E` or `A`, you could do this:

  ```bash
  ls *[E,A]*.md
  ```

  This says list `md` files with  an `E` and/or `A`, in any order, surrounded by any number of other characters. Results are:

  ```bash
  CHANGELOG.md  README.md
  ```
--------------
### Think about This (6)

How would you specify a pattern that would return **HOWTO.md**. **R2.md**,  and **README.md**, but not **CHANGELOG.md**?

------------
## Solution (6)

Here is one possible solution. This will list any file containing zero or more characters, followed by W or R, followed by zero or more characters and `.md`
```bash
ls *[W,R]*.md
```
------------

## Less Common Pattern Matchers: Curly Braces {} (6)

- **Curly Braces** `{}` match filenames with more than one pattern (word). Separate the words (patterns) with commas and no spaces. 
  
- Together with `mkdir -p` curly braces would allow you to create the entire tooltest directory structure in a single command! So, the curly braces function similarly to a for-loop.
  
  ```bash
  mkdir -p tooltest/{code,data/{inputs,outputs}}
  ```
  
- You could also create all of the files using `{}`
  ```bash
  touch {HOWTO,README,CHANGELOG,code/README}.md
  ```
-----------
### Think about This (6)
How would you make a directory structure like this using mkdir -p and {}?
```terminal
MRI
├── bids
│   └── code
└── derivatives
```

----------
## Solution (6)
Make a directory structure:
```bash
mkdir -p MRI/{bids/code,derivatives}
```

----------------------
## Part 6: Summary (6)
* You learned about the star, question mark, square brackets, and curly braces.
* Of these, the star is the most common.
* There are many other glob patterns to explore! See the [Bash Globbing Tutorial](https://linuxhint.com/bash_globbing_tutorial/)

---------------------

## Conclusion 

In this tutorial you learned about: 

- file and directory manipulation: moving, copying, and removing.
- recursion
- naming: case-sensitivity and avoiding spaces in names.
- history
- permissions
- pattern matching

Together with the commands and concepts you learned in Unix 1, this should be enough to use the command line successfully. 

The next Unix lessons will focus on configuration (which helps you understand how to set up your environment) and scripting (which helps you learn to use control structures, like for loops and conditionals).



---

## Class Credit

-  If you are taking COGS 500, you need to record your terminal session and turn it in. Hopefully, script has been recording all this time, and now you need to exit the recording:

   ```bash
   exit
   ```

-  A file should have been created named `unix1` followed by your username and the date.  For example, `unix2_dkp_20220625.txt`.  

-  Check that there is something in the file:

   ```bash
   cat unix2*.txt
   ```

-  Download this file from Google Cloud shell and submit it for the unix2 practicum assignment.
