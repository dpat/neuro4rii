# Tutorial 10: Docker CSVKIT
* csvkit is a Python tool to manipulate and query *comma-separated-value* (*.csv) files. 
* Critically, the csvkit Docker container has to read, and possibly write, to a directory on your computer. 
* For the Docker container to access a directory on your computer, you must **bind-mount** the computer directory to a directory inside the container. 
* Understanding bind-mounts is critically important to being able to use neuroimaging containers.

## Cloud Shell Lessons
* Set up csv data (1)
* Bind Mount the input (2)
* Make sure you (not root) owns the output (3)
* Using redirection (4)
* CSVkit Summary (5)



-----------------
* Download the Docker container, `oliverlorenz/csvkit`, to your computer using **pull**.  This just makes a local copy, but does not try to run it yet.

* It does not matter what directory you are in when you do a docker pull!
  
  ```bash
  docker pull oliverlorenz/csvkit
  ```
-----------------
### CSVKIT Links 
* [dockerhub: csvkit](https://hub.docker.com/r/oliverlorenz/csvkit)  
* [csvkit: read the docs](https://csvkit.readthedocs.io/en/latest/)  

--------------
## Set up the CSV Dataset (1)

* To keep things tidy, work in a new directory.
* **TIP**: Don't worry about where the Docker images are! They are not located in your working directories! Instead, Docker images are maintained in a secret area on the system. 
* **Warning**: Unfortunately, when you restart cloud shell, your Docker images will be gone.
* Make the directory **csv** in your home directory, and navigate to it.
* Retrieve demog.csv:
  ```bash
  wget https://bitbucket.org/dpat/neuro4rii/raw/main/data/demog.csv
  ```
* You can now run commands described on [csvkit: read the docs](https://csvkit.readthedocs.io/en/latest/).

-----------------

## Bind Mount the **csv** directory to access demog.csv (2)
* Print column names: 
  ```bash
  docker run -it --rm -v $(pwd):/workspace oliverlorenz/csvkit csvcut -n  demog.csv
  ```
* This example introduces the bind flag **-v** which bind-mount a volume on the host computer into tthe container. 
* The bind mount, **-v $(pwd):/workspace**, is necessary to process data on your computer (i.e., the host) with a Docker container. Otherwise, the container is isolated from the host operating system. 
* The bind statement joins the host directory to a container directory using a **:** to separate the two:
* The order is always **host:container**. 
* The paths are always absolute.
* In the example above **$(pwd)** is the computer (host) directory.
* **/workspace** is the directory inside the container.
* When the container looks into its **/workspace** directory, it is actually seeing the contents of your current host directory.
* `oliverlorenz/csvkit` is the name of the Docker image. This is one of several Docker images that make csvkit available. The images are available from Dockerhub and get downloaded if you don't have a local copy or run if you do.
* `csvstat demog.csv` is the command you want csvkit to run.

  * The `csvcut` command is inside the container. It takes the **-n** flag which tells it to *display column names and indices from the input CSV and exit*.

* The `csvcut` command only works if it can access the **csv** file.
* The output file, `demog.csv` should be depositied in your present working directly `${PWD}`
* Next, you'll try a command that both reads and saves a file to your bind-mounted directory.

---

## Questions (2)

-  2.1 Does order matter when specifying a bind-mount?
-  2.2 Does Docker allow relative paths in the bind-mounts?

-----------------------
## Make sure you, not Root, owns the output on Linux (3)
* On Linux you run into an extra problem: If Docker creates files, they belong to root (not to you)!
* To avoid this issue, you must tell Docker who the output should belong to, using the **-u** user flag.
* This problem doesn't occur on Mac or Windows.

----------------------
* Print statistics for each column and save them to a file, **stats.txt**
  ```bash
  docker run -it --rm -v $(pwd):/workspace -u $UID:$GID oliverlorenz/csvkit csvstat demog.csv > stats.txt
  ```
* Make sure **stats.txt** exists, that you own it, and that it contains your output:

---------------
## Understand the Docker Command

`docker run` is invoked as interactive `-it` and the used container will be removed when it is done running `--rm`.  These are very typical choices. 

`-v` introduces a bind mount, as explained before.

`oliverlorenz/csvkit` is the name of the Docker image. This is one of several Docker images that make csvkit available. The images are available from Dockerhub and get downloaded if you don't have a local copy or run if you do.

`csvstat demog.csv` is the command you want csvkit to run.

The output file, `stats.csv` should be depositied in your present working directory `${PWD}`

---

## Understand Defining the Owner (3)

* The flag **-u** defines the user (otherwise Docker defaults to root). 
* The arguments **$UID:$GID** are environment variables that retrieve your user ID and group ID respectively.  
* The user ID variable is always defined by the system, but you added the group ID variable to the **.bashrc** configuration file in tutorial_03_unix_configuration.   
* If you run Docker on Linux, this is an important step.
* Docker on Mac and Docker on Windows do not require you to define the owner. On Mac and Windows any files you create using Docker belong to you.

---

## Questions (3)

-  3.1 Does the host operating system affect who owns the files that Docker generates?
-  3.2 `-u $UID:$GID` is relevant for which operating system?

---------------

**TIP**:   

* If you accidentally create a file or directory on Cloud shell that belongs to root, use **sudo** to remove it or change its permissions.  
  ```
  sudo rm -fr /home/dkp/saves
  ```
* **sudo** allows you to assume root privileges.  Use it sparingly.

--------------

### Use Redirection to create the Output File (4)

* By default, **cvsstat** displays its results to standard output (i.e., you see it on the screen). 
* To save the output, use redirection, as you can with any bash command.
* You see this redirection at the end of the Docker command: **> stats.txt**  
* It means *Send the output to a file called stats.txt* (**stats.txt** will be created if it does not exist). 

--------------

## CSVkit Summary (5)

In this practicum, you learned about: 
* **bind-mounts**, which allow a Docker container to process data on your computer.

* output file **permissions** which are only necessary when using Docker on Linux.  

* Docker on Mac and Windows does not require you to define the owner. On Mac and Windows, any files you create using Docker belong to you.

   

---

## Questions (5)

-  5.1 What is csvkit?

---

