

# Tutorial for JQ: JSON Filtering Tool

## Cloud Shell Lessons

-  View Files (1)
-  Delete Properties (2)
-  Add and Modify Properties (3)
-  Useful Scripts (4)


-----------------------
### Data

If you do not already have it, download and unzip the sample dataset:

```bash
wget https://bitbucket.org/dpat/neuro4rii/raw/main/data/json_tutorial_data.zip
```

------------
## View Files (1)

Navigate to `json_tutorial_data/bids_sidecars`

- Use the dot operator to pretty print a JSON file:
  ```bash
  jq . sub-test_asl.json
  ```
  
- Display the pretty printed JSON file, sorted alphabetically with `-S`:
  ```bash
  jq -S . sub-test_asl.json
  ```
  
- Display the JSON file printed in compact mode `-c`:
  ```bash
  jq -c . sub-test_asl.json
  ```

---------

### Filter the values (1)

- Follow the dot with a key to view just the value associated with that key:

  ```bash
  jq .EchoTime sub-test_asl.json 
  ```

- View values associated with multiple keys using a comma-separated list protected by single quotes:
  ```bash
  jq '.EchoTime, .RepetitionTime' sub-test_asl.json 
  ```

- View properties in a valid JSON object `{}` protected by single quotes: The output includes the keys and values wrapped up as a JSON object:
  ```bash
  jq '{EchoTime, RepetitionTime}' sub-test_asl.json  
  ```

----------

## Use built-in jq functions (1)

- View the **length** of a value (This may be especially handy for arrays):

  ```bash
  jq '.PostLabelingDelay | length' sub-test_asl.json 
  ```

- Determine **whether a key exists** in the JSON file:
  ```bash
  jq '(has("IntendedFor"))' sub-test_asl.json 
  ```
  will report `false` because the key is not in the file.  However,
    ```bash
  jq '(has("Modality"))' sub-test_asl.json 
    ```
  will report `true`.

-  View just the **keys**:

   ```bash
   jq keys sub-test_asl.json 
   ```

-  Or, view just **string** values:

   ```bash
   jq '.[]|strings' sub-test_asl.json
   ```

- Just like viewing strings, you can also view booleans, arrays, or numbers.

-----

### Think about this (1)

How would you view: 

-  just booleans? 
-  just arrays? 
-  just numbers?

-------------------

## Solution (1)

-  View just boolean values:

   ```bash
   jq '.[]|booleans' sub-test_asl.json
   ```

- Or, view just array values:

   ```bash
   jq '.[]|arrays' sub-test_asl.json
   ```

- Or, view just number values:

   ```bash
   jq '.[]|numbers' sub-test_asl.json
   ```

--------------------
## Part 1: Summary (1)

-  JQ is ubiquitous on Unix systems.  
-  You'll find it on Google Cloudshell and the U of A HPC (for the latter, you have to be in an interactive session).  
-  Embed `jq` commands in bash scripts to loop over JSON files, checking individual properties.

------------------
## Delete Properties (2)

-  The built-in function `del` removes a key and its corresponding value.  Because the output of `jq` goes to the terminal, instead of changing the actual file, you can continue to experiment on sub-test_asl.json.  

-  Look at the output to confirm the expected changes are being made.

-  Both the dot operator and parentheses are used in delete statements:
   ```bash
   jq 'del(.Modality)' sub-test_asl.json
   ```
   
- Delete multiple properties by putting them in a comma-separated list inside the parentheses:
  ```bash
  jq 'del (.MagneticFieldStrength, .Modality, .PostLabelingDelay )' sub-test_asl.json
  ```

-----------

### Pass a bash variable to `jq` (2)

- To try this, define a variable:

  ```bash
  myvar=Modality
  ```

-  Check the variable:

   ```bash
   echo $myvar
   ```

-  Wrap the variable in a single quotes, surrounded by double quotes: `"'"$myvar"'"` and delete the variable:

    ```bash
    jq 'del(."'"$myvar"'")' sub-test_asl.json
    ```

--------------
### Think about this (2)

-  How could you change `myvar` to be `MagneticFieldStrength`?
-  How would you test that you could use the variable to delete `MagneticFieldStrength`?

--------------

## Solution: (2)

- Redefine the variable:

   ```bash
   myvar=MagneticFieldStrength
   ```

-  Check the variable:

   ```bash
   echo $myvar
   ```

-  Delete the variable, the same way as before:

   ```bash
   jq 'del(."'"$myvar"'")' sub-test_asl.json
   ```



-------------

## Part 2: Summary (2)

jq makes it relatively easy to delete properties from the JSON file.

----

## Part 3: Add and Modify Properties  (3)

- Revise the value of an existing property by assigning the new value with `=`:
  ```bash
  jq '.Modality="MR_New"' sub-test_asl.json
  ```
- Or, add a new key and value:
  ```bash
  jq '.Fred="new_value"' sub-test_asl.json
  ```
- To modify multiple properties at once, use the pipe `|` (if you use a comma, only the first property gets changed):
  ```bash
  jq '.MagneticFieldStrength = 6 | .Modality="HyperMR" ' sub-test_asl.json
  ```

--------------

### Think about this (3)

How would you change three values at once?

--------------

## Solution: (3)

Unsurprisingly, you can keep adding pipes to the statement:
```bash
jq '.MagneticFieldStrength = 6 | .Modality="HyperMR" | .Manufacturer="Fred"' sub-test_asl.json
```

-------------

## Part 3: Summary (3)

-  In addition to viewing and deleting properties from a JSON file, `jq` allows you to add values.  
-  If you have a lot of changes to make, this approach is faster than uploading each file to the JSON-Editor Interactive Playground, and less error-prone than making the changes manually in a text editor.  

-  But, stay tuned for the next lesson on `jd` to learn more tricks!

## Part 4: Useful Scripts  (4)

- By default, `jq` returns results to **standard output** (i.e., the terminal).  
  
- Use Unix redirection, `>` to send that output to a new file:
  
  ```bash
  jq 'del(.Modality)' sub-test_asl.json > nomodality.json
  ```
  
- Confirm that `nomodality.json` no longer has the Modality property.

- Often, what you want is to modify the input file. 

- Copy `sub-test_asl.json` to `test.json` before working with actual transformations:

   ```bash 
   cp sub-test_asl.json test.json
   ```

- Unfortunately, you cannot transform the input by redirecting the output into it!  

- Instead, you must redirect the output to a new file, and then in a second step, rename the new file to the input filename:
  ```bash
  jq 'del(.Modality)' test.json > tmp.$$.json && mv tmp.$$.json test.json
  ```

- Confirm that `test.json` no longer has the Modality property.
  
- To facilitate using variables to add and delete properties in the input file, retrieve these scripts (put them in your bin directory, and ensure they are executable):
  
  ```bash
  wget https://bitbucket.org/dpat/tools/raw/master/LIBRARY/jdel
  ```
  and
  ```bash
  wget https://bitbucket.org/dpat/tools/raw/master/LIBRARY/jadd
  ```
  
- `jadd` adds or modifies a property in the JSON file specified.  Here, it adds the missing property back into `test.json`:
  
  ```bash
  jadd Modality 3 test.json 
  ```
  
- Confirm that `test.json` now has the Modality property.
  
- `jdel` deletes a named property (key and value) from the JSON file specified:
  
  ```bash
  jdel Modality test.json
  ```
  
- Confirm that `test.json` is now missing the Modality property, again!
  
- `jadd` can also add a property with a string value to the JSON file specified, by adding an additional `s` argument specifying that the value is a string:
  
  ```bash
  jadd Modality MRIscanner test.json s
  ```

--------------

### Think about this (4)
-  `jadd` can handle other values, like booleans and arrays.  
-  How would you use `jadd` to add an array of numbers? an array of strings? a boolean?

--------------

## Solution: (4)

`jadd` provides help to explain each possible case.        

Try each command, and then confirm that `test.json`has changed:    

- Add/modify a **number** value, e.g.,
  ```bash
  jadd Modality 3 test.json
  ```
  
- Add/modify a **boolean**, e.g.,
  
  ```bash
  jadd Modality true test.json
  ```
  
- Add/modify an array of numbers (wrap the array in single quotes), e.g.,:
  ```bash
  jadd Modality '[1,3,5]' test.json
  ```
  
- Add/modify an array of strings: wrap the array in single quotes, and double quote the strings, e.g.,
  ```bash
  jadd Modality '["a","b","c"]' test.json
  ```
  
- Add/modify a string, by adding the 4th argument, s:
  ```bash
  jadd Modality MRI test.json s
  ```


-------------

## Part 4: Summary (4)

- JQ is quite sophisticated and the syntax can get complex.  
- In general, output gets printed to the terminal, but often you want to revise a file.  
- The scripts `jdel` and `jadd` facilitate revising a JSON file by deleting or adding and modifying the properties you specify.

-----------------
## Conclusion

- The `jq` lesson focused on the kinds of JSON manipulations that might be handy for editing BIDS sidecars. 
- This should get you started with viewing and filtering JSON data, querying for particular fields or the length of a value, deleting properties and adding or modifying properties. 
- Two scripts are provided to make the processing easier.

---



