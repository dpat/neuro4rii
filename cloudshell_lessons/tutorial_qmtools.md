# Tutorial: QMtools

This project provides several programs to visualize, compare, and review the image quality metrics (IQMs) produced by the MRIQC program. MRIQC extracts no-reference IQMs from structural (T1w and T2w) and functional (BOLD) MRI data.

------

## Setup

- Retrieve the Docker container

  ```bash
  docker pull hickst/qmtools
  ```

- Retrieve the support project, which includes sample data and the correct directory structure.

  ```bash
  git clone https://github.com/hickst/qmtools-support.git qmtools-support
  ```

- Navigate to the qmtools-support directory, and run tree to see the directory structure:  

   ```bash
   cd qmtools-support; tree
   ```

- It's directory structure looks like this (using tree):

```terminal
qmtools-support
├── docs
│   ├── Fetcher.md
│   ├── LICENSE
│   ├── Queries.md
│   ├── TrafficLight.md
│   └── Violin.md
├── fetched
│   └── sample_fetched.tsv
├── inputs
│   └── sample_bold.tsv
├── Makefile
├── qmfetcher
├── qmfetcher_hpc
├── qmtraffic
├── qmtraffic_hpc
├── qmviolin
├── qmviolin_hpc
├── queries
│   └── samples
│       ├── hasinstructions.qp
│       ├── manmafmagskyra.qp
│       ├── metrics.qp
│       ├── philips.qp
│       └── siemens7T.qp
├── README.md
└── reports

```



------

## qmtraffic

qmtraffic normalizes a set of MRIQC image quality metrics and creates a tabular HTML report, visualizing how much each image's metrics deviate from the mean for all the images in that set.

-  Get help with qmtraffic:

```bash
./qmtraffic -h
```

```terminal
Usage: ./qmtraffic [-h] [-r REPORT_DIR] modality group_file
  where:
     modality   = Modality of the MRIQC group file. Must be one of: bold, T1w, or T2w.
     group_file = Path to MRIQC group file in "inputs" subdirectory.

  options:
    -h, --help     Show this help message and exit.
    -r REPORT_DIR, --report-dir REPORT_DIR
                           Optional name of report subdirectory in main reports directory.

  example 1: ./qmtraffic bold inputs/sample_bold.tsv
  example 2: ./qmtraffic bold inputs/sample_bold.tsv -r sample_traffic_report

```

-  Run qmtraffic on the sample data:  `-r bold_traffic` names the output report directory `bold_traffic`

````bash
./qmtraffic bold inputs/sample_bold.tsv -r bold_traffic
````

```terminal
(qmtraffic): Processing MRIQC group file 'inputs/sample_bold.tsv' with modality 'bold'.
(qmtraffic): Produced reports in reports directory 'reports/bold_traffic'.
(qmtraffic): To see the report: open 'reports/bold_traffic/bold.html' in a browser.
```

To view HTML pages in Google Cloudshell start a simple python web server. 

I suggest you run the following command in a separate terminal window, so you can leave it running while you do the rest of the lesson in the first terminal.

```bash
cd reports
python -m http.server 8080
```

✅ On the upper right, choose the web-preview icon and "Preview on port 8080" (The web preview icon is a rectangle with a diamond inside, just to the right of the terminal icon).

✅ In the directory listing, select *bold_traffic* to navigate to that directory.  Then select *bold.html* from the listing.

---

## Questions

- 1) Which table contains the fber IQM?

- 2) In the *Negative values are better* table, which row has the best `dvars_vstd` value?

---



✅  tsv files are also created. View *pos_bad_bold.tsv* file in Google Cloud shell editor by selecting it in the explorer.

---

## Questions

- 3) Which table in bold.html corresponds to `pos_bad_bold.tsv`?

---------

## qmfetcher

qmfetcher queries the MRIQC database server to fetch image quality metrics (IQMs) for images previously processed by neuroimaging groups all over the world. A query may specify multiple image metadata parameters to select the IQMs to be returned. As query parameters are read from user-created files, queries may be easily and consistently repeated.

The use of content-specific query parameters is a very powerful capability, potentially allowing insight into many of the factors which affect image quality. Using QMTools, in conjunction with the MRIQC server, allows image quality metrics to be queried and compared across a variety of BIDS metadata and provenance properties.

-  Get help with qmfetcher:

```bash
./qmfetcher -h
```

```terminal
usage: ./qmfetcher [-h] [-v] [-n NUM_RECS] [-o filename] [-q filepath] [--use-oldest] [--url-only] modality
  where:
    modality       Modality of the MRIQC group file. Must be one of: bold, T1w, or T2w.

  options:
    -h, --help     Show this help message and exit.
    -v, --verbose  Print informational messages during processing.
    -n NUM_RECS, --num-recs NUM_RECS
                   Number of records to fetch (maximum) from a query.
    -o filename, --output-filename filename
                   Optional name of file to hold query results in the fetched directory.
    -q filepath, --query-file filepath
                   Path to a query parameters file in or below the current directory.
    --use-oldest   Fetch oldest records.
    --url-only     Generate the query URL and exit program.

  example 1: ./qmfetcher -v bold
  example 2: ./qmfetcher -v T1w -n 27 --use-oldest -o T1w_oldies
  example 3: ./qmfetcher -v T1w -n 27 --use-oldest --url-only
  example 4: ./qmfetcher -v bold -o Siemens7T.tsv -q queries/samples/siemens7T.qp
  example 5: ./qmfetcher -v bold -o Siemens7T.tsv -q queries/samples/siemens7T.qp --url-only
```

Run qmfetcher to get data from the MRIqc server: 

-  `-v` makes the output verbose.

-  `-o skyra_sample.tsv` allows you to provide a meaningful name for the output file.  

-  `-q queries/samples/manmafmagskyra.qp` specifies an optional query parameter file that limits the results retrieved from the server.

 ```bash
./qmfetcher -v bold -o skyra_sample.tsv -q queries/samples/manmafmagskyra.qp
 ```

```termianl
(qmfetcher): Querying MRIQC server with modality 'bold', for 100 records.
(qmfetcher): Querying MRIQC server with modality 'bold', for 50 records.
(qmfetcher): Fetched 50 records out of 1375.
(qmfetcher): Saved query results to 'fetched/skyra_sample.tsv'.
```

Optional query parameters are defined in a text file, in this case `queries/samples/manmafmagskyra.qp`:

```terminal
bids_meta.Manufacturer == "Siemens"
bids_meta.MultibandAccelerationFactor < 3
bids_meta.MagneticFieldStrength >= 3
bids_meta.ManufacturersModelName == "Skyra"
```

In the above example, the query will limit the output to "Siemens" in the manufacturer field.  Note that if the manufacturer field contains different capitalization, e.g., "SIEMENS" that will not be returned.  To address that issue, one can include multiple spellings in the query file, e.g.:

```terminal
bids_meta.Manufacturer == "Siemens"
bids_meta.Manufacturer == "SIEMENS"
```

Several example query files are provided which you can copy, alter and mix. Sometimes fetcher cannot find any data that meets your criteria.  This probably means you have specified the criterial incorrectly.

By default, qmfetcher gets 50 records.  Use the number flag to request more data, e.g.,  `-n 100` requests 100 records. The more records you request, the longer it takes, so start with the default.

To write your own queries, consult [queries.md](https://github.com/hickst/qmtools-support/blob/main/docs/Queries.md)



---

## Questions

- 4) In the directory `queries/samples` what line indicates the results should include 7T data in  the file `Siemens7T.qp`?

- 5) If I wanted a criterion "not equals", how would I specify that? 
- 6) The query has `instructions.qp` illustrates a special case. What is it?



-----------

## qmviolin

qmviolin compares two sets of MRIQC image quality metrics and creates an HTML report, using Violin plots to visualize how the two groups compare for each IQM. The two datasets to be compared can both be user-generated OR both fetched from the MRIQC database OR one of each.

-  Get help with qmviolin:

```bash
./qmviolin -h
```

```terminal
Usage: ./qmviolin [-h] [-r REPORT_DIR] modality fetched_file group_file
  where:
    modality       Modality of the MRIQC group file. Must be one of: bold, T1w, or T2w.
    fetched_file   Path to the file, in "fetched" directory, to be compared to the group file.
    group_file     Path to MRIQC group file, in "inputs" subdirectory.

  options:
    -h, --help     Show this help message and exit.
    -r REPORT_DIR, --report-dir REPORT_DIR
                           Optional name of report subdirectory in main reports directory.

  example 1: ./qmviolin bold fetched/sample_fetched.tsv inputs/sample_bold.tsv
  example 2: ./qmviolin bold fetched/sample_fetched.tsv inputs/sample_bold.tsv -r sample_reports
```

-  Run qmviolin and provide the optional report directory name `-r bold_violin` to distinguish it from the qmtraffic report.

```bash
./qmviolin bold fetched/skyra_sample.tsv inputs/sample_bold.tsv -r bold_violin
```

```terminal
(qmviolin): Comparing MRIQC records with modality 'bold'.
(qmviolin): Compared group records against fetched records.
(qmviolin): Produced violin report to 'reports/bold_violin'.
(qmviolin): To see the report: open 'reports/bold_violin/violin.html' in a browser.
```

✅  Open the resulting *violin.html* under `reports/bold_violin` to see how the sample data compares to data from the MRIqc server.



---

## Questions

- 7) In the first violin plot, what IQM is represented?
- 8) What color is used for the fetched group in the violin plots?



----

## Final Thoughts

-  In this tutorial, you ran a set of tools that works with Docker and with Apptainer.  You ran the Docker scripts here, but there are scripts for the hpc as well (e.g. qmtraffic is for Docker, qmtraffic_hpc is for the hpc).  
-  The scripts expect inputs in an *inputs* directory, fetched files in a *fetched* directory and queries in a *queries* directory, **so it is a good idea to work in the *qmtools-support* directory, even with your own files in the future, and even on the HPC!**

---

