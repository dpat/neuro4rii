# Tutorial 7: Datalad Revision Control

## Cloud Shell Lessons
* Introduction to Datalad
  * Datalad create (2)
  * Datalad save (3)
  * Datalad download (4)
  * Datalad nesting (5)
  * Datalad run (6)
  * Remove files from Datalad Control (7)
  * Summary (8)



--------------
## Version Control with DataLad (1)

This tutorial is distilled down from the 400+ page Datalad Handbook, so, obviously, it leaves a lot out.

PROS   
* DataLad extends the capabilities of GIT to data. 
* You can use any GIT commands in a Datalad dataset. They work together.
* Text files, like scripts, are still handled entirely by GIT. 

CONS  
* DataLad is much younger and less mature than GIT.
* If you remove the GIT repository from a Datalad directory, all binary files are completely removed! Ouch!
* If you drag a binary file around in your Datalad directories, you can break its connection to the underlying data!  Ouch!  
  * This is because all binary files in a DataLad directory are just pointers to the real data buried in the **.git** repository.

* Return to your home directory:

```bash
cd
```
* **You should already have DataLad installed (from the conda lesson)!**



---

## Questions

-  1.1 How does Datalad differ from GIT?

-  1.2 If you are just backing up your scripts, which tool would you use? 

----------------------

## DataLad Links (1)

* [DataLad Handbook](http://handbook.datalad.org/en/latest/) 
* [DataLad YouTube Channel](https://www.youtube.com/playlist?list=PLEQHbPfpVqU5sSVrlwxkP0vpoOpgogg5j)
* [ABCD YouTube Channel: DataLad lecture](https://www.youtube.com/watch?v=udLVUyZQanw&feature=emb_logo)
---------------

## Datalad Create (2)

This section closely follows part of the excellent [DataLad Handbook](http://handbook.datalad.org/en/latest/). If you are serious about Datalad, you should check out the handbook.

-  Activate the datalad environment

```bash
conda activate datalad
```

* Create a directory under Datalad control:

```bash
datalad create -c text2git -c yoda -f DataLad-101
```

* Datalad reports its progress.  You should see something like:

```terminal
[INFO   ] Creating a new annex repo at /home/dkp/DataLad-101
[INFO   ] Scanning for unlocked files (this may take some time)
[INFO   ] Running procedure cfg_text2git
[INFO   ] == Command start (output follows) =====
[INFO   ] == Command exit (modification check follows) =====                                 
create(ok): /home/dkp/DataLad-101 (dataset)                
```

--------------

* Examine the results:

```bash
cd DataLad-101
ls -a
```
* `datalad create` is analogous to `git init`.  In both cases, the infrastructure for version control is set up, but nothing is committed yet.
* It contains two hidden directories, **.git**, **.datalad**. 
* The primary repository is **.git**. 
* The flag and argument `-c text2git` ensures that your scripts and text files will be stored exactly as they are stored by git.  That is, history, but not the actual files will be stored in the repository.
* The flag and argument `-c yoda` creates a code subdirectory, README.md, and CHANGELOG.md.  All are configured to be tracked by git as text files.
* Unlike GIT, binary files will be stored under **.git/annex**. If you delete the **.git** repository, all binary files will be gone, and all you will have is links (shortcuts) in your directory that point nowhere! Git treats the annexed files as a separate branch in the **.git** repository.
* The **.gitattributes** and **.datalad** store some additional configuration information.

---

## Questions (2)

-  2.1 What does `datalad create` do?

-  2.2 `datalad create` is analogous to what git command?

--------------------

## Populate the Datalad Dataset (2)

* Create a subdirectory and cd into it:

```bash
mkdir books
cd books
```
* At this point, there is nothing here.
* Get a a book from the Internet:

```bash
  wget https://sourceforge.net/projects/linuxcommand/files/TLCL/19.01/TLCL-19.01.pdf/download -O TLCL.pdf
```
* Get another book: 

```bash
  wget https://homepages.uc.edu/~becktl/byte_of_python.pdf -O byte-of-python.pdf
```
* Make sure you got the books:

```bash
ls -la
```
* You should see two books:

```terminal
drwxr-xr-x 2 dkp dkp    4096 Feb  8 23:02 .
drwxr-xr-x 5 dkp dkp    4096 Feb  8 23:02 ..
-rw-r--r-- 1 dkp dkp 2693891 Nov  3  2016 byte-of-python.pdf
-rw-r--r-- 1 dkp dkp 2120211 Jan 28  2019 TLCL.pdf
```

-----------------------

## Get Datalad Status (2)

* Check the state of the dataset contents with `datalad status`:

```bash
datalad status
```
* You should see something like:  
```terminal
untracked: books (directory)
```
* The `datalad status` command is analogous to the `git status` command.  In fact, datalad understands either one.

---

## Questions (2)

-  2.3 What does `datalad status` do?

-  2.4 `datalad status` is analogous to which git command?

-----------------------
## Datalad Save (3)

* Unlike git, datalad combines `add` and `commit` into a single `save` command.
* Save the books to Datalad with a descriptive message:

```bash
datalad save -m "added books on Python and Unix to read later"
```
* Datalad reports that it has added the two books and saved the dataset:

```terminal
add(ok): books/TLCL.pdf (file)                                                                      
add(ok): books/byte-of-python.pdf (file)                                                            
save(ok): . (dataset)                                                                               
action summary:                                                                                     
  add (ok: 2)
  save (ok: 1)
```


---

## Questions (3)

-  3.1 What does `datalad save` do?

-  3.2 `datalad save` is analogous to which git command?

----------------

## Examine the History and Contents of the Datalad Dataset (3)

* Look at the history of the repository:

```bash
git log
```
* You may need to hit `q` for quit to exit git log.
* You see three commits, with the most recent first.  The first two commits were generated by the process of creating the dataset.

```terminal
commit 41413d78febdcc14b9fe8b3ed75dffbd703f0f2 (HEAD -> master)
Author: dkp <dkp@email.arizona.edu>
Date:   Mon Feb 8 23:03:43 2021 +0000

    added books on Python and Unix to read later

commit 74113937a02aa6e9c32f7be6ebc0232678806b7e
Author: dkp <dkp@email.arizona.edu>
Date:   Mon Feb 8 23:01:43 2021 +0000

    Instruct annex to add text files to Git

commit c25eb6c552dd89dedb1a547bdd1702b9842696ee
Author: dkp <dkp@email.arizona.edu>
Date:   Mon Feb 8 23:01:42 2021 +0000

    [DATALAD] new dataset
```
* Reminder: You may need to hit `q` for quit to exit git log.
* PDFs are binary files, so Datalad moves the files into **.git/annex** and labels them with a special key that uniquely identifies the file and its state. If the file were to change, Datalad would know.
* List the contents of the books directory to see that now they are links (`ls -l` lists the first letter of the permissions as `l` meaning *link*):

```bash
ls -la 
```
* You should see something like this:

```terminal
drwxr-xr-x 2 dkp dkp 4096 Feb  8 23:03 .
drwxr-xr-x 5 dkp dkp 4096 Feb  8 23:02 ..
lrwxrwxrwx 1 dkp dkp  131 Nov  3  2016 byte-of-python.pdf -> ../.git/annex/objects/P5/qK/MD5E-s2693891--e61afe4b25d76c849c4e61f6547ed03.pdf/MD5E-s2693891--e61afe4b25d76c849c4e61f6547ed03.pdf
lrwxrwxrwx 1 dkp dkp  131 Jan 28  2019 TLCL.pdf -> ../.git/annex/objects/jf/3M/MD5E-s2120211--06d1efcb05bb2c55cd039dab3fb28455.pdf/MD5E-s2120211--06d1efcb05bb2c55cd039dab3fb28455.pdf
```
---

## Questions (3)

-  3.3 Can you use git in a Datalad repository?

--------------

## Datalad save a single file (3)

* Although Datalad combines add and commit, you can still selectively save particular files.
* Download another book in the books directory: 

```bash
wget https://github.com/progit/progit2/releases/download/2.1.154/progit.pdf
```
* Look at the datalad status:

```bash
datalad status
```
* You should see one untracked book.
* Save just the new book:  

```bash
datalad save -m "add reference book about git" progit.pdf
```

-------------

## Datalad Download (4)

Datalad can retrieve remote data in several ways depending on your goals:

-  `datalad download-url`
-  `datalad clone`
-  `datalad get`

---

## Add a file with datalad download-url (4)

* Datalad can also retain information about the URL of the source data in its repository using the `download-url` command. 
* Go up one level to change directory to **DataLad-101**:

```bash
cd ..
```
* Use `pwd` to make sure you are in the **DataLad-101** directory:

```bash
pwd
```

* Download an xkcd comic and save it, in one step:

```bash
datalad download-url -m "xkcd git comic" https://imgs.xkcd.com/comics/git.png
```
* Note that unlike **wget**, *datalad download-url* retrieves the file and does a *datalad save* in one step!



---

## Question (4)

-  4.1 How is `datalad download-url`better than `wget` followed by `datalad save`?

---------------

## Install existing Datasets with datalad clone (4)

* One strength of Datalad is that you can download a large dataset of links in seconds.  Subsequently, you can browse those files and decide which ones you actually want the corresponding binary files for. 

```bash
mkdir recordings
```

* Retrieve a dataset from the internet and store it inside your current dataset:

```bash
datalad clone --dataset . https://github.com/datalad-datasets/longnow-podcasts.git recordings/longnow
```
* List the contents of the longnow recordings subdataset.  It has 236 links:

```bash
ls -R recordings
```
* Because these are just the links, they download fast, and you can browse them. 

---------------
## Datalad Get and Drop (4)

* Download an mp3 file now with `datalad get`:

```bash
datalad get recordings/longnow/Long_Now__Conversations_at_The_Interval/2017_06_09__How_Digital_Memory_Is_Shaping_Our_Future__Abby_Smith_Rumsey.mp3
```
* You should see a message like:

```terminal
get(ok): recordings/longnow/Long_Now__Conversations_at_The_Interval/2017_06_09__How_Digital_Memory_Is_Shaping_Our_Future__Abby_Smith_Rumsey.mp3 (file) [from web...]                                                                  
action summary:
  get (notneeded: 1, ok: 1)
```
* The file has now been downloaded! 
* You can get all the files at once with `datalad get .` but **don't do that** because there are a lot of big files!
* Drop the file:

```bash
datalad drop recordings/longnow/Long_Now__Conversations_at_The_Interval/2017_06_09__How_Digital_Memory_Is_Shaping_Our_Future__Abby_Smith_Rumsey.mp3
```
* The link remains, but you no longer have to store the file. You should see a message like this: 

```terminal
drop(ok): /home/dkp/DataLad-101/recordings/longnow/Long_Now__Conversations_at_The_Interval/2017_06_09__How_Digital_Memory_Is_Shaping_Our_Future__Abby_Smith_Rumsey.mp3 (file) [checking http://podcast.longnow.org/interval/redirect/interval-020160329-rumsey-podcast.mp3...]
```

---

## Questions

-  4.2 What does `datalad get` do?

-  4.3 What does `datalad drop` do?

-  4.4 What does `datalad clone` do?

---------------

## Nesting Datasets in Datalad (5)

* A dataset with LOTS of files (even small ones) makes Datalad slow. To keep Datalad speedy, you need to nest datasets. 
* Look at your version control history for the superdataset (**DataLad-101**) and the subdataset (**DataLad-101/recordings/longnow**) to confirm that you have two datasets with different histories.
* You should be in the directory **DataLad-101**. 
* Look at your version history (this is the same as the alias `gitls`). 

```bash
git log --oneline 
```
* Change directories:

```bash
cd recordings/longnow
```
* Look at the version history again, using the alias:

```bash
gitls
```
* **Datalad-101** and **recordings/longnow** are different **datasets**, not just different **directories**!

-----------------
### Datalad Nesting Summary (5)

* You can nest directories in a dataset without making them subdatasets (e.g., **books** is a subdirectory of DataLad-101, but **books** is not a subdataset).
* You can also create subdatasets from directories.

---

### Question (5)

-  5.1 When is datalad nesting advantageous?

-----------------
## Datalad Run (6)

* `datalad run` invokes a shell command of your choosing and record its impact on a dataset. This means Datalad provides you with a record of precisely what you did.
* Go up two levels to change directory to **Datalad-101**:

```bash
cd ../..
```
* Make sure you are in **Datalad-101**:

```bash
pwd
```
* Navigate to the code directory:

```bash
cd code
```

* Create a script in your code directory:

```bash
touch list_titles.sh
```

* Give the script permission to execute:

```bash
chmod a+x list_titles.sh
```

----------------
##  Create the Script for a Datalad Run (6)

* Edit the script:

```bash
edit list_titles.sh
```

* **Copy and paste** the contents of list_titles.sh into the editor.  You can find the contents if you go to this link: [list_titles.sh](https://bitbucket.org/dpat/neuro4rii/src/main/bash_scripts/list_titles.sh). It is also reproduced below for your convenience:

```bash
#!/bin/bash

for i in recordings/longnow/Long_Now__Seminars*/*.mp3; do
  # get the filename
  base=$(basename "$i");
  # strip the extension
  base=${base%.mp3};
  # date as yyyy-mm-dd
  printf "${base%%__*}\t" | tr '_' '-';
  # name and title without underscores
  printf "${base#*__}\n" | tr '_' ' ';
done
```


* **Save** *list_titles.sh* in the editor.

* Save your work with Datalad:

```bash
datalad save -m "Add a short script to list podcast speakers"
```
* You are now ready to try `datalad run`.

--------------
## Datalad run (6)

* Change to the *DataLad-101* directory from the *DataLad-101/code* subdirectory:

```bash
cd ..
```
* Use `pwd` to make sure you are in the **DataLad-101** directory:

```bash
pwd
```

* Run the script and save the results in one step with `datalad run`:

```bash
datalad run -m "create podcast title list" "code/list_titles.sh >podcasts.tsv"
```
* The `datalad run` command takes a message (`-m "something"`) and then a command to run: `code/list_titles.sh >podcasts.tsv`
* Open **podcasts.tsv** and take note of how many lines it has (click on the file in the cloudshell explorer to view it). 
* Unfortunately it only got one subdirectory of the **longnow** dataset. This is because the glob pattern on line #3 is incorrect.

-----------------
## Datalad Rerun (6)

* You can rerun any command you have run previously with `datalad run`. This allows you to revise the script, if you discover you made a mistake. Using `datalad run` instead of unlocking and changing files outside of Datalad control thus provides an important advantage: The processing is documented and can be repeated with `datalad rerun`.
* If **list_titles.sh** is not open in the editor, then open it:

```bash
edit code/list_titles.sh
```

* The script only lists podcasts in one subdirectory. 
* Change the following line to remove the word **Seminars**:

```terminal
for i in recordings/longnow/Long_Now__Seminars*/*.mp3; do
```

becomes
```terminal
for i in recordings/longnow/Long_Now*/*.mp3; do
```
---------------

* This pattern should correctly capture the contents of both *Long_Now* subdirectories

* **Save** the script revisions in your Datalad version history:

```bash
datalad save -m "rev: list both directories content" code/list_titles.sh
```
* Retrieve the commit number of the [DATALAD RUNCMD] command:

```bash
gitls
```
* You should see something like this identifying the run command:

```terminal
882db42 [DATALAD RUNCMD] create podcast title list
```

* Rerun the command like this, but **USE YOUR COMMIT NUMBER!** as shown below:

```bash
datalad rerun 882db42
```

* Reminder: You may need to hit `q` for quit to exit git log.
* Open **podcasts.tsv** again and take note of how many lines it has (click on the file in the cloudshell explorer to view it). 
  * There should now be 234 lines because all of the files in both **Long_Now** directories are listed.
* Datalad rerun is an efficient way to run the same command over again, and you can use it with any command in the commit history and improve reproducibility. 
---

## Questions (6)

-  6.1 What does `datalad run` do?

-  6.2 Why is `datalad run` advantageous? 

-----------------
## Remove files from Datalad control (7)

### Export a Binary File from Datalad (7)

* From the **DataLad-101** directory, type the following to copy and unlock a file:

```bash
cp git.png ../git_xkcd.png
```

* This will copy the unlocked git cartoon into your home directory. It also gives it a different name, just to make it easier to talk about. 
* Click **git_xkcd.png** in the Cloud shell explorer window, or just confirm that it is now a file and not linked:

```bash
ls -l ../git_xkcd.png
```

* You should see:

```terminal
-r--r--r-- 1 dkp dkp 48767 Feb 15 20:37 ../git_xkcd.png
```
* The permissions start with a dash (-) and not lower case L (l) for link. This copy of the cartoon is a regular file permisions and not a link.
* Compare this to the **git.png** file in the DataLad-101 directory:

```bash
ls -l git.png
```

-------------

## Unlock and Archive a Datalad Dataset (7)

* From the **DataLad-101**, type the following to create an unlocked archive of the entire Dataset:

```bash
datalad export-archive ../NoDataLad
```
* This will create an archive file in your home directory with all of the binary files unlocked and the **.git** repository removed.  
* Go to your home directory and untar **NoDataLad**:

```bash
cd
tar xvf NoDataLad.tar.gz
```

**Note**: **tar** is similar to **zip**. 

* List the contents of the NoDataLad directory recursively.  The links are gone and the files have been restored:

```bash
ls -lR NoDataLad
```
* The **NoDataLad** directory contins unlocked copies of the files added to the DataLad-101 dataset.
* The **NoDataLad** directory does not contain the contents of the subdataset.

----------------
## How to Remove a Datalad Dataset (7)

* Because Datalad locks all binary files to protect them, extra steps are required to give yourself permission to remove a Datalad dataset entirely:

```bash
chmod -R a+rwx DataLad-101/
```

* After changing the permisions recursively on everything in the directory, you are free to remove it just like any other directory (`rm -fr DataLad-101`)...but don't do that right now.

   

---

## Questions (7)

-  7.1 Are there special steps involved in extracting data from datalad?

-  7.2 Are there permission problems related to removing a Datalad dataset?

------------------
## Datalad Summary (8)

* DataLad is similar to GIT and they work well together. DataLad adds support for large binary files that GIT cannot handle. 
* DataLad is very promising, but it is immature and changing. 
* Use it with caution. Make regular backups.

---------------------

* In this section you installed miniconda so you could use the **conda** command-line to install Datalad.
  * Conda is an extremely useful for working with Python tools.

----------------------

* You learned to **create** and **save** a DataLad dataset.
----------------------

* You learned that you can **download** large DataLad datasets without worrying about filling up your disk, because you are only downloading links and not the actual files.  
* You learned that you can download a file with download-url and save iinformation about its origin.
  * Subsequently, you can **get** files of interest, inspect and use them, and then **drop** them again.
-----------------------

* You learned that it is important to **nest** datasets to keep DataLad speedy and responsive.
* You learned to **run** and **rerun** DataLad commands.
-----------------------

* You learned about **exporting** data from a DataLad dataset.
* You learned how to **remove** a DataLad Dataset.

-----------------

* You have finished the DataLad section of this tutorial.

