

# Tutorial 01: Unix

## Cloud Shell Lessons

  * The Command Prompt (1)

  * Where are you and what's here? (2)

  * Command Options and Help (3) 

  * Command Arguments and Paths  (4)

  * Navigation: Leaving Home (5)


---

## The Command Prompt (1)
* In the terminal window, you see a typical command prompt, e.g.,: 
    ```terminal
    dkp@cloudshell:~$
    ```
    
* It shows your username, the "machine" you are working on, your present working directory, and a prompt symbol `$`

--------------------
From the command prompt displayed above, you can see that:
* My username is **dkp**   
* I am working on a **cloudshell** machine    
* My present working directory is my **home directory**.  
  (Home is the default starting directory, denoted by a tilde **~**)        
* The `$` is the last character displayed in the prompt.
* You can type a command after the prompt.   
* Although frequently displayed in examples of commands you should type, you should not actually type `$`   

--------------------
## Part 1: Summary (1)

Know this about the command prompt:

* Do NOT type the prompt `$` itself!
* The **first** thing you type at the prompt should be always be a known **command** (e.g., `ls`, `pwd` ). 
* The command can be followed by appropriate options and arguments, but **you must start with a known command**!
* If the terminal is not displaying the command prompt, your command will be ignored. This can happen when the system is still processing the previous command.



---

## Your First Two Commands: Where am I? What's Here? (2)

Learn where you are in the directory tree and list what files and directories are here: 
* `pwd`  print working directory     
* `ls` list (that is a lower case L, not the number 1!!)

----------------
## Where are You? (2)
Print the absolute path of the present (current) working directory:
  ```bash
  pwd
  ```

-----------

### Your Home directory (2)
* On Linux, all users have a directory under **/home**. 
* By default, the terminal opens in your home directory, so `pwd` shows you the **absolute path** to your home directory. Mine looks like this:
  ```
  /home/dkp
  ```
  **NOTE:**: On a Mac, your **home** directory is under **/Users** not **/home**!

--------------
### Questions (2)

* 2.1 What is the absolute path? 
* 2.2 What is the relative path?      

* 2.3 How would you represent the relative path to parent of your parent directory (your grandparent)?       

--------------
## What's Here? (2)

* Type the following list command (`ls` uses a lower case L, not the number 1!): 
  ```bash
  ls
  ```
* By default, `ls` lists the contents of the present working directory. 
* In this case, this is your home directory.
* Notice that the files and directories you see at the command prompt are just like the ones you see in the Explorer window.  These are just two different ways to display the same information.  

----------------

### Hidden folders (a.k.a directories) and files (2)
* It looks like there are not many files or directories here, but you can't see all the hidden ones! 
* Hidden files and directories are named with an initial dot (e.g., **.bashrc**) and usually contain configuration information. 
* By default, `ls` does not show you the hidden files and directories!

----------------

### Question (2)

* 2.4 Why would you want to hide configuration files and directories?      

-------------

## Part 2: Summary (2)

It can be a little scary to get used to the command line, but everything else you do in Unix or with containers will depend on using the command line and understanding the directory tree and absolute and relative paths. In addition to determining where you are in the directory tree with `pwd`, you should be able to list the files and directories in your current location with `ls`. Use `ls` and `pwd` often to ensure that know where you are and what is here! 

* `pwd` (print working directory) displays the absolute path to your location in the directory tree.  
* `ls`  lists the contents of the present working directory.    

----------------------
## Part 3: Command Options and Help (3)
* An option (a.k.a flag) is a command modifier, usually a dash and character, separated from the command by a space.  
* For example, to see all files and directories in your current location, including the hidden ones, use `ls` with the `-a` (all) option: 
  ```bash
  ls -a
  ```
-----------------
* Always separate the option from the command with a space!
* In every directory, there are two hidden directories: 
  * here: `.`
  * my parent:  `..`    
    `..`  turns out to be quite useful, as you'll see later.  

----------------------
## Combine Options (3)
* Use `ls` with the ``-a` (all) AND `-l` options to see a long listing of all files and directories.  
* There are two different ways to do this.  You can list each option separately:
    ```bash
    ls -l -a
    ```
-------------
* You can also combine options:
    ```bash
    ls -la
    ```
----------------
* Option order does NOT matter!

-------------------
### Lower case L (l) vs one (1) (3)

* Use  `ls`  with the `-1` (one) flag to list one item on each line. Although the lower case L (l) looks a lot like the one (1), and both work as options for  `ls` , they do different things!
  ```bash
  ls -1
  ```
------------

## Get Help: Manual Pages (3)

* Manual pages provide command help in your terminal. 
* For example, on. many unix systems (but as of Fall 2024, not Google cloud shell), you can learn about the options for  `ls`  by displaying its manual page:
    ```bash
    man ls
    ```
* When you display a manual page, **you are no longer at the command prompt!**
* Use the *up* and *down* arrow keys to navigate through the manual page. 
* You can quit the man page with **q** to return to the command prompt.

-------------------
### Questions (3)

- 3.1 If Google cloudshell supported man pages, how would you display the man page for `pwd`?
- 3.2 Check the Google  page for `ls`. How would you use  `ls`  to sort the list by time?

-------------------------
## Part 3: Summary (3)

- A command option, like  `-a` or `-l` for the  `ls`  command, modifies the command.
- Most Unix commands have an assortment of option flags available. Understand that these options exist, are useful, and you can view the manual page using `man`. 
- Sometimes the available options differ by operating system (e.g., some commands available on Linux may not be available on mac and vice-versa). For example,  `man ls` on my Mac displays a reduced set of options compared to Google Cloud Shell!

-------------

## Part 4: Arguments to a Command (4)

* In addition to options, a command may take arguments.  
* When you type  `ls`  it lists the contents of the current directory.
* What if you want to list the contents of a different directory?
* If  `ls`  is like a verb, then an argument to  `ls`  is like its direct object. 
* Arguments to commands are ofter paths, so you must understand the directory tree.

------------

### Questions (4):

* 4.1 How would you list the contents of the root `/` (slash) directory?        

* 4.2 **/bin** is an important directory where most commands are stored. How would you list the contents of the **/bin** directory?         

* 4.3 **/home** is the parent of your personal home directory. How would you list the contents of the **/home** directory with an **absolute** path? 
* 4.4 How would you list the contents of the **/home** directory with a **relative** path?       

* 4.5 Assuming you are in your home directory, how would you list the contents of the hidden **.config** directory that exists there?

   

--------------

## View File Contents (4)

The commands `file` and `cat` both operate on file arguments to tell you something about the contents of the files.

* To look inside a file, use **cat** (concatenate), **if it is a text file**.
* The name of the file is the argument to each of the following commands.

------------------

* Determine whether **.bashrc** is a text file:
    ```bash
    file .bashrc
    ```
* The command `file` operates on the argument **.bashrc** and confirms that it is text. 
* View the file with the command  `cat`!
    ```bash
    cat .bashrc
    ```



## Questions (4)

- 4.6 What is in your .bashrc file? Copy and paste the contents of your .bashrc into the OpenClass question.



---

## Part 4: Summary (4)

In addition to options, a command may take arguments. The arguments are typically the files or directories that the command will operate on.  

- For example, `ls /bin` lists the contents of the **/bin** directory. **/bin** is the argument.
- `file .bashrc` displays the file type of the the argument **.bashrc**
- `cat .bashrc` displays the contents of the the argument **.bashrc**

* **Spaces are meaningful!** Spaces separate a command from its options and arguments. 
* Most commands have a **man** page that provides command help.  
    * Use **q** to quit the man page and return to the command prompt.

---------------------
In addition to commands, you must understand the directory tree, especially absolute and relative paths:

* **Absolute paths**: 
  * An absolute path to some target directory is always the same, regardless of where you are in the directory tree.
  * The absolute path is always specified with an initial slash `/` to indicate that it is specified from the root of the file system. 
  * The absolute path to your present directory is displayed with `pwd`

----------------------
* **Relative paths**: 
  * A relative path never starts with `/`.
  * A relative path varies depending on your current location in the directory tree.
    * A relative path to a target directory *may* be shorter than the absolute path because it doesn't have to trace the entire directory tree:
    * For example, `..` refers to your current parent directory.
------------------
## Part 5: Navigation: Leaving Home (5)
* So far, you have never moved from your home directory! 
* The command used for navigation is `cd` (change directory). 
* `cd` needs an argument that tells it where you want to go. 
* Your command prompt changes when you change directory to help you keep track of where you are.
* Without any argument, `cd` takes you home,   
(like Dorothy in the *Wizard of Oz* clicking her heels together).  

------------------------
## Practice changing directory (5)
* Change directory to the **root** directory:
    ```bash
    cd /
    ```
* It is a good habit to check where you are (pwd) and what's here (ls):
    ```bash
    pwd
    ls 
    ```
* In future lessons, you will need to remember these two commands ( `ls`  and `pwd`) and use them frequently.

-----------------------
### Question (5)

* 5.1 Why did your command prompt change from `~` to `/`?

----------------------
## Practice changing directory again (5)
* Change directory to **/bin**:
    ```bash
    cd /bin
    ```

* Check where you are (pwd) and what's here (ls):
* Look at your command prompt: **/** has changed to **/bin**!

--------------------------

### Go Home (5)
* Type `cd` with no arguments to go home:
    ```bash
    cd
    ```
* You can also use the special **~** character to go home:
    ```bash
    cd ~
    ```
------------------------
## Navigate using Relative Paths (5)
* Go to the hidden **.config** directory in your home directory. 
* If you were not in your home directory, the following command would fail!
    ```bash
    cd .config
    ```
* Check where you are and what's here.

------------------
* List the contents of your current parent directory.  In this case, your parent directory is your home directory, because you are in **.config**:
    ```bash
    ls ..
    ```
-----------------
* Go up two levels relative to your current directory:
    ```bash
    cd ../..
    ```
* Don't forget to check where you are and what's here!
-------------------
### Question (5)

* 5.2 What would you use to navigate to your great-grandparent directory?

-------------------
## Summary: Navigation (5)
* Navigate the directory tree with `cd`.
* Specify either absolute or relative paths to your target directory.

----------------
* Develop good habits: 
  * After you change directory, use `pwd` to confirm where you are, and
  * Use  `ls`  to ensure you know what's here. 

---

## Conclusion

* This concludes your introduction to four critical Unix commands: `cd`,`pwd` , `cat`, and  `ls` .
* These are non-destructive commands that allow you to navigate (cd), determine where you are (pwd), and view the contents of files (cat) and directories (ls). 
* Subsequent lessons will assume you are familiar with `ls` and `pwd` and that you are using them regularly without being prompted.  
* You should reflexively check where you are with `pwd` after you navigate to a new directory. 
* You should list the contents of the current directory with `ls` every time you are curious, forgetful, or think something should have changed.

