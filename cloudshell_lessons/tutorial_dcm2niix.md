# Tutorial: dcm2niix

------

This dataset is free from all HIPAA restrictions.  However, **most DICOMS contain protected health information, and you should NOT use the Google Cloudshell to convert those DICOMS to NIfTI**. 

Goal: To download and use the simplest DICOM to BIDS conversion program available. 

-  dcm2niix is a command-line program that does all the heavy lifting in other conversion tools. 
-  It will handle DICOMs from all the major manufacturers, including CT images, but may fail on dicoms from less common instruments.  
-  Used by itself, dcm2niix does not provide the peripheral files or folders that BIDS requires, but it is still useful for one-off conversions, CT, or replacing bad conversions. 
-  Complete documentation for **dcm2niix** is available [here on NITRC](https://www.nitrc.org/plugins/mwiki/index.php/dcm2nii:MainPage), including [instructions for Microsoft Windows](https://www.nitrc.org/plugins/mwiki/index.php/dcm2nii:MainPage#Microsoft_Windows_Usage). 
-  You can download *dcm2niix* for your computer: Find the latest releases here: https://github.com/rordenlab/dcm2niix/releases.

----

## Questions

-  1)  Is dcm2niix available for your computer?
-  2.  Should you routinely upload participant DICOM files to Google Cloud Shell for conversion? 

---

## Setup 

- Retrieve the data:

  ```bash
  wget https://osf.io/mqgzh/download -O sub-219_dicom.zip
  unzip sub-219_dicom.zip; rm sub-219_dicom.zip
  ```

- This creates a directory called `MRIS`.  
  
- This dataset is public so you can use it to learn, but usually you should NOT upload DICOM data to Google cloud as it is a HIPAA violation.
  
- Retrieve the dcm2niix Linux executable 
  ***Note**: This assumes you have a bin directory and it is in your path!*:

  ```bash
  cd bin; wget https://github.com/rordenlab/dcm2niix/releases/download/v1.0.20230411/dcm2niix_lnx.zip -O dcm2niix.zip; unzip dcm2niix.zip; chmod a+x dcm2niix; cd ~ 
  ```
  
- If you are new to Google Cloud shell and have not retrieved the `tree` executable, do that now (it won't hurt, even if you have done it before):

  ```bash
  cd bin; wget https://bitbucket.org/dpat/neuro4rii/raw/main/binaries/tree chmod a+x tree; cd ~ 
  ```

---

## Questions

-  3.  If you want dcm2niix for your Windows computer, which binary should you download?

-  4)  Why did I have you put dcm2niix in your bin directory?
-  5)  Why did you run `chmod a+x` on dcm2niix?

---

## Double-Check the Downloads

- Check the MRIS directory with the tree program (`-d` just shows you the directories and not their contents):
  ```bash
  tree -d MRIS
  ```
  You should see a directory tree like this:
  ```terminal
  MRIS
  ├── dicom
  │   └── 219
  │       └── itbs
  │           ├── Bzero_verify_PA_17
  │           ├── DTI_30_DIRs_AP_15
  │           ├── field_mapping_20
  │           ├── field_mapping_21
  │           ├── Localizers_1
  │           ├── MoCoSeries_19
  │           ├── MoCoSeries_31
  │           ├── Post_TMS_restingstate_30
  │           ├── restingstate_18
  │           └── T1_mprage_1mm_13
  └── Nifti
    └── code
  ```
  
- Check that `dcm2niix` is available:
  ```bash
  dcm2niix
  ```
  
- You should see a help message (if you see `permission denied` then something went wrong.)

---

## Cleanup

First navigate to the dicom directory:

```bash
cd MRIS/dicom
```

Remove the Localizers and derived DICOM sequences from the 219 folder: 

-  Localizers_1
-  MoCoSeries_19
-  MoCoSeries_31

```bash
rm -fr 219/itbs/{Local*,MoCo*}
```

Derived images are processed versions of the original (a.k.a primary) images. Because they are so similar to the primary images, if the derived images remain in the dataset they can overwrite the corresponding primary images. 

Your dataset may include several derived DTI images, such as TENSOR, FA, ADC, ColFA, and TRACEW images. You should remove these or be very careful not to convert them.  
It is also likely that you have MoCo (motion-corrected) fMRI images as we do in this dataset. 
Localizer images (a.k.a scouts) get converted into lots of images of no interest, so you might as well delete those too.

**Warning**: Don't go overboard!  Work with a copy of the DICOMS when you are deleting files!

---

## Questions

-  6.  Why did I have you remove the derived MoCo images from your DICOMs?
-  7.  Why did I have you remove localizers from your DICOMs?



---

## Using dcm2niix

Use the abbreviated help below, and the examples, to convert the files in the sub-219 DICOM dataset to BIDS NIfTI-1 and JSON pairs. 

---

### Abbreviated dcm2niix help

-  By default, *dcm2niix* generates NIfTI images and companion JSON sidecar files. 
-  **-f : filename** (%a=antenna (coil) name, %b=basename, %c=comments, %d=description, %e=echo number, %f=folder name, %i=ID of patient, %j=seriesInstanceUID, %k=studyInstanceUID, %m=manufacturer, %n=name of patient, %o=mediaObjectInstanceUID, %p=protocol, %r=instance number, %s=series number, %t=time, %u=acquisition number, %v=vendor, %x=study ID; %z=sequence name; default 'sub-001')
-  **-h** : show help
-  **-i** : ignore derived, localizer and 2D images (y/n, default n)
-  **-o** : output directory (omit to save to input folder)
-  **-w** : write behavior for name conflicts (0,1,2, default 2: 0=skip duplicates, 1=overwrite, 2=addsuffix)
-  **Note**: If there are spaces in the name of the output directory, you must enclose it in double -quotes: *dcm2niix -o "~/outdir with spaces/dir" 219* 

---

## Simple Call

Call `dcm2niix`:


  ```bash
  dcm2niix 219
  ```

This creates all of the NIfTI and JSON files in the 219 directory. The names are not very helpful. Look at them:

```bash
ls 219
```

 Then remove them:

  ```bash
  rm 219/*.*
  ```

## Control the Output Directory

Put the results of running dcm2niix in the current directory instead of mixing the results with the DICOMS. Use the `-o` flag and the following dot (for "here") to control the location of the output:

  ```bash
  dcm2niix -o . 219
  ```

-  Look at the results with `ls`. 

-  The filenames are still terrible. Remove these results:

  ```bash
  rm *.*
  ```



---

## Question

-  8.  How are the results different for `dcm2niix 219` vs `dcm2niix -o . 219`?

---

## Improve file naming with dcm2niix flags

Improve filenames by using the string "sub" and explicitly entering the session entity (ses-itbs). Use flags that dcm2niix recognizes (e.g., %n=name of patient, and %p=protocol)

  ```bash
  dcm2niix -o . -f sub-%n_ses-itbs_%p 219
  ```
-  Look at the results with `ls`. 
-  This is not quite BIDS-compliant naming, but at least the subject number and session are correct!

```terminal
sub-219_ses-itbs_Bzero_verify_P-A.json
sub-219_ses-itbs_Bzero_verify_P-A.nii.gz
sub-219_ses-itbs_DTI_30_DIRs_A-P.bval
sub-219_ses-itbs_DTI_30_DIRs_A-P.bvec
sub-219_ses-itbs_DTI_30_DIRs_A-P.json
sub-219_ses-itbs_DTI_30_DIRs_A-P.nii.gz
sub-219_ses-itbs_Post_TMS_restingstate.json
sub-219_ses-itbs_Post_TMS_restingstate.nii.gz
sub-219_ses-itbs_T1_mprage_1mm.json
sub-219_ses-itbs_T1_mprage_1mm.nii.gz
sub-219_ses-itbs_field_mapping_e1.json
sub-219_ses-itbs_field_mapping_e1.nii.gz
sub-219_ses-itbs_field_mapping_e2.json
sub-219_ses-itbs_field_mapping_e2.nii.gz
sub-219_ses-itbs_field_mapping_e2_ph.json
sub-219_ses-itbs_field_mapping_e2_ph.nii.gz
sub-219_ses-itbs_restingstate.json
sub-219_ses-itbs_restingstate.nii.gz
```

---

## Question

-  9.  Is dcm2niix a complete solution for all your BIDS conversion needs?
-  10.  How would you display the dcm2niix help?

---

## Summary

-  Although dcm2niix does not create any directory structure or peripheral BIDS files. I regularly use it for troubleshooting and one-off conversions, e.g., images that are not part of a larger defined set, or a single image that did not convert as expected. 

-  dcm2niix will handle DICOM from all the major manufacturers, including CT images.

-  dcm2niix offers additional support for BIDS conversion, which you can pursue [here](https://www.nitrc.org/plugins/mwiki/index.php/dcm2nii:MainPage#Brain_Imaging_Data_Structure_.28BIDS.29_Support).

-  With a bit more setup, you can achieve better conversion results for large projects as you will see in subsequent lessons on EZ-BIDS and Heudiconv.

  ```bash
  rm *.*
  ```

-  You are done with dcm2niix for now. 

   

