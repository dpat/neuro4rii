# Tutorial: Intend4

Intend4 provides a mechanism to ensure that the optimal fieldmaps, if present, will be used for susceptibility distortion correction of bold and dwi images respectively. Intend4 can generate or remove the `IntendedFor` field and its values from either the phasediff fieldmap json for bold data (e.g., *fmap/sub-188_phasediff.json*) or the reverse phase encode fieldmap json for dwi data (e.g., *fmap/sub-188_dir-PA_epi.json*).  

This is important because DICOM-to-BIDS conversion tools do not always generate and populate the optional IntendedFor field; however, some BIDS apps, notably fMRIprep and QSIprep, rely on the presence of the IntendedFor field and its values to implement optimal susceptibility distortion correction. Without the populated IntendedFor field in the phasediff field map, fMRIprep will not use that image to correct the distortion in the fmri images.  Likewise, without the populated IntendedFor field in the reverse phase encode field map, QSIprep will not use that image to correct the distortion in the dwi images. 

For susceptibility distortion correction, **one and only one fieldmap** must be selected.  The Intend4 tool will therefore skip subjects for whom the relevant fieldmap file is missing or fieldmap selection is ambiguous (e.g., multiple phasediff files exist for the bold images of a particular subject and session). 

If there are **sessions**, Intend4 expects an fmap directory in each session subdirectory.

Intend4 will handle **permissions** on the json files it needs to alter, including read-only json files. The original permissions will be restored after the change. However, you may still run into permission errors if the dataset is read-only.

Intend4 has **NOT** been revised to handle ASL data!

Intend4 is also available on the HPC as a Singularity container.

As 0f late 2022, the IntendedFor field is being replaced by an alternative approach,  [the B0FieldIdentifier and B0FieldSource](https://github.com/bids-standard/bids-specification/pull/622).  BIDS handling of distortion correction continues to evolve, and BIDS DICOM to NIfTI conversion tools continue to improve, so, eventually, this tool will not be needed.



---

## Questions

-  1.  What is the IntendedFor Field and how is it useful?  

-  2.  What is the difference between Intend4 and the IntendedFor field?

-  3.  Which BIDS tools understand how to use the IntendedFor field?

-  4.  How many field maps can be used to correct the target images?

-  5.  How many target images can a field map be used to correct?

-  6.  Is there any alternative approach to using the IntendedFor field?

-  7.  Can Intend4 handle Pepolar correction images for fMRI?

-  8.  Can Intend4 handle ASL data?

---

### Download Intend4 Script and Test Data

- Download the `intend4.sh` bash script file:

  ````bash
  wget https://github.com/hickst/intend4/raw/main/intend4.sh -O bin/intend4.sh; chmod a+x bin/intend4.sh
  ````

  Ensure it is executable and in your path.

- Download the dataset BIDS_MRI.zip: 

  ```bash
  wget https://bitbucket.org/dpat/neuro4rii/raw/main/data/BIDS_MRI.zip; unzip BIDS_MRI.zip; rm BIDS_MRI.zip
  ```

### Understand **Intend4**

- The script assumes you are running from your **BIDS_MRI/data** directory!  
  This is the directory where your NIfTI BIDS data and the dataset_description.json reside.

```terminal
BIDS_MRI
├── data
│   ├── sub-188
│   ├── sub-190
│   ├── sub-194
│   ├── sub-215
│   └── sub-221
```

- If you have not yet pulled the `hickst/intend4` Docker container, the script will do that the first time you run it.
- If you use `--participant-label`, subject numbers are specified with the number only! That is `sub-078` is specified as `078`.
- By default, the `intend4.sh` script runs in **verbose** mode to provide maximal information.
- Intend4 will handle permissions on the JSON files it needs to alter: Even if the files are read-only, changes will be made, and then the original permissions will be restored after the change.
- Intend4 will handle valid bids directories, including sessions.

---

## Questions

-  9.  What is verbose mode for the Intend4 tool?

-  10.  Will Intend4 handle BIDS directories that include sessions?



-----------

## Run Intend4 on bold data and one subject

```bash
intend4.sh bold --participant-label 188
```

If you have not pulled the hickst/intend4 docker container, the script will do that now:

```terminal
Unable to find image 'hickst/intend4:latest' locally
latest: Pulling from hickst/intend4
bb7d5a84853b: Pull complete
f02b617c6a8c: Pull complete
d32e17419b7e: Pull complete
c9d2d81226a4: Pull complete
3c24ae8b6604: Pull complete
8a4322d1621d: Pull complete
0bde298e076a: Pull complete
e169b6c7c628: Pull complete
1b7366f8a3aa: Pull complete
6a5053af2d01: Pull complete
4f4fb700ef54: Pull complete
625f71f679c0: Pull complete
b1fcf0200c79: Pull complete
b4fed0aeb8b9: Pull complete
1a889060d520: Pull complete
ed2a74acbd84: Pull complete
ccb71067e190: Pull complete
04802e957995: Pull complete
415c42501533: Pull complete
Digest: sha256:5a9b06a6581ebe5ff96304d64a9dd282c9815261c1a59a5005b84429ce12e478
Status: Downloaded newer image for hickst/intend4:latest
(intend4): Modifying IntendedFor field in sidecar files for modality 'bold'.
(intend4): Processing subject 188
(intend4): Modified IntendedFor fields in 1 phasediff sidecars.
```

 Examine the phasediff.json file to confirm that it has been altered:

```bash
cat sub-188/fmap/sub-188_phasediff.json
```

You should see the IntendedFor field 

```json
"IntendedFor": [
    "func/sub-188_task-nad1_run-01_bold.nii.gz",
    "func/sub-188_task-nad1_run-02_bold.nii.gz",
    "func/sub-188_task-nad1_run-03_bold.nii.gz",
    "func/sub-188_task-nad1_run-04_bold.nii.gz"
  ],
```



-------

## Run Intend4 to remove values

```bash
intend4.sh bold --participant-label 188 --remove
```

You see the verbose message:

```terminal
(intend4): Removing IntendedFor field in sidecar files for modality 'bold'.
(intend4): Processing subject 188
(intend4): Removed IntendedFor fields in 1 phasediff sidecars.
```

Inspect the file:

```bash
cat sub-188/fmap/sub-188_phasediff.json
```

The values for IntendedFor have been removed:

```json
 "IntendedFor": [],
```

-------------

## Run Intend4 for dwi data and all subjects

````bash
intend4.sh dwi
````

You see the verbose message:

```terminal
(intend4): Modifying IntendedFor field in sidecar files for modality 'dwi'.
(intend4): Processing subject 215
(intend4): Processing subject 188
(intend4): Processing subject 194
(intend4): Processing subject 190
(intend4): Processing subject 221
(intend4): Modified IntendedFor fields in 5 epi sidecars.
```

Inspect the reverse-phase-encode file:

```bash
cat sub-188/fmap/*epi.json
```

You should see the IntendedFor field:

```bash
"IntendedFor": [
    "dwi/sub-188_acq-AP_dwi.nii.gz"
  ],
```

-----------

## Questions

-  11.  Can Intend4 run on fieldmaps for fMRI?

-  12.  Can Intend4 remove IntendedFor values, or only create them?

---

## Run Intend4 for bold data and see missing data alert

````bash
intend4.sh bold 
````

You see the verbose message:

```terminal
(intend4): Modifying IntendedFor field in sidecar files for modality 'bold'.
(intend4): Processing subject 215
(intend4): Processing subject 221
Error: phasediff sidecar file is missing for subject 221. Skipping...
(intend4): Processing subject 194
(intend4): Processing subject 188
(intend4): Processing subject 190
(intend4): Modified IntendedFor fields in 5 phasediff sidecars.
```

**Note**: Intend4 alerts you to the missing phasediff image for sub-221!

-------

### Get Usage Help

To see a help (usage) message for intend4.sh, call the tool with the special ***help flag*** (`-h` or `--help`):
```bash
intend4.sh -h
```

```terminal
This script calls the 'intend4' docker container.
Run it from the bids data directory containing your subjects.
Modality is the only required argument: specify 'bold' or 'dwi'.

Examples:
  Modify the phasediff fieldmap JSON files for all subjects:
    > /home/dkp/bin/intend4.sh bold

  Modify the Reverse Phase encoded image JSON file (fmap/*.epi) for all subjects:
    > /home/dkp/bin/intend4.sh dwi

  To remove the phasediff IntendedFor values, add the flag --remove after the modality:
    > /home/dkp/bin/intend4.sh bold --remove

  Modify the phasediff fieldmap JSON files for just subjects 078 and 215:
    > /home/dkp/bin/intend4.sh bold --participant-label 078 215

Usage: /home/dkp/bin/intend4.sh -h | --help
       OR
       /home/dkp/bin/intend4.sh {bold, dwi} [--participant-label [SUBJ_IDS ...]] [--remove]
```





