# Tutorial 9: Docker Zork
* This lesson has one point: The environment inside a container is not the same as the environment outside the container.
* In many cases, the environment on the inside and outside a container is linux, so it may be difficult to believe the inside and outside are different!
* However, in this example, you are embedded in a game inside the container, so it is clearly NOT the same as the host computer.

--------------
### Zork Resources
* [dockerhub: zork1](https://hub.docker.com/r/clockworksoul/zork1)   
* [zork1: commands](https://zork.fandom.com/wiki/Command_List)

-------------
## Download and run Zork 
Download and run the zork1 game.
```bash
docker run -it --rm clockworksoul/zork1
```
-------------

## Container Isolation 
* When you run the Zork Docker container, you are transported to the game inside the Docker container. 
* Your prompt changes, and you no longer have access to the cloud shell: Bash commands will not work! 
  * Try **ls** or `pwd`  
      These don't work because you are not on your host computer
* Try navigating in the game:
  * Type **go north**
  * You can type short sentences: **climb tree** or **get egg**. 
* See [zork1: commands](https://zork.fandom.com/wiki/Command_List).
* Use **quit** to quit the game and follow the instructions to exit the container.
* You will be returned to Google Cloudshell!

* **WARNING**: You may encounter some problems typing at the command-line after exiting Zork. I found that clicking in the editor and then back in the terminal window solved the problem.

---

## Questions

Answer the questions in the OpenClass lesson by copying and pasting from the terminal to OpenClass:

1. What do you see when you type `ls` from inside the Zork Docker container?
2. What did you do in the game?  



---
## Summary
* Docker containers are quite isolated from their host environment.  
* Importantly, you cannot access the directories on your computer from the tools inside the container.
* The next lesson explores bind mounts which are used to solve the container isolation problem.
* Interestingly, Singularity and Apptainer containers are less isolated than Docker containers, and although they also use bind-mounts, they are more flexible (for example, a Singularity/Apptainer container can see anything in your home directory).

---

