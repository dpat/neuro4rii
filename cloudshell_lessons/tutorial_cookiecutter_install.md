

# Tutorial: Cookie Cutter Install

Conda is a package manager, which means you use it to install programs.  It is available as [Anaconda](https://www.anaconda.com/) and as [miniconda](https://docs.conda.io/en/latest/miniconda.html).  Miniconda is small, and only requires a command-line interface.  If you install miniconda in your home environment, you can install many tools that would otherwise be difficult or impossible for a non-root user to install. 

In this tutorial, you'll install several tools that you can use later.  Anything you install here, you can also install the same way with miniconda on your local computer or on the HPC.

----------
## Cloud Shell Lessons

  * Install Cookiecutter in a separate environment

-------

## Install CookieCutter in a Separate Environment 

- To this point, you've installed tools in the same environment. This seems to be okay for `tree` and `datalad`, but now you are going to install another Python program with its own libraries and dependencies.  Maybe it would not conflict with Datalad, but it is better to create a separate environment and install it there!  

- In general, you should create separate environments for your different tools. 
  
- Create a new environment with a helpful name:
  
  ```bash
  conda create -n cookie
  ```
  
- Follow the prompts:
  ```terminal
  Proceed ([y]/n)? y
  
  Preparing transaction: done
  Verifying transaction: done
  Executing transaction: done
  #
  # To activate this environment, use
  #
  #     $ conda activate cookie
  #
  # To deactivate an active environment, use
  #
  #     $ conda deactivate
  ```
  
- Activate the cookiecutter environment:
  ```bash
  conda activate cookie
  ```
  
- You see the command-prompt change to indicate you are in the new environment:
  ```terminal
  (cookie) dkp@cloudshell:~$
  ```
  
- Install cookiecutter now:
  ```bash
  conda install cookiecutter
  ```
  
- Once it is installed, invoke *cookiecutter help* to see the usage message:  
  ```bash
  (cookie) dkp@cloudshell:~$ cookiecutter -h
  ```
  
- You see a usage message if all went well:
   ```terminal
    Usage: cookiecutter [OPTIONS] TEMPLATE [EXTRA_CONTEXT]...
   
    Create a project from a Cookiecutter project template (TEMPLATE).
   
    Cookiecutter is free and open source software, developed and 	managed by
      volunteers. If you would like to help out or fund the project, please get in
      touch at https://github.com/audreyr/cookiecutter.
   
    Options:
      -V, --version              Show the version and exit.
      --no-input                 Do not prompt for parameters and only use
                                 cookiecutter.json file content
      -c, --checkout TEXT        branch, tag or commit to checkout after git clone
      --directory TEXT           Directory within repo that holds
                                 cookiecutter.json file for advanced repositories
                                 with multi templates in it
      -v, --verbose              Print debug information
      --replay                   Do not prompt for parameters and only use
                                 information entered previously
      -f, --overwrite-if-exists  Overwrite the contents of the output directory if
                                 it already exists
      -s, --skip-if-file-exists  Skip the files in the corresponding directories
                                 if they already exist
      -o, --output-dir PATH      Where to output the generated project dir into
      --config-file PATH         User configuration file
      --default-config           Do not load a config file. Use the defaults
                                 instead
      --debug-file PATH          File to be used as a stream for DEBUG logging
      -h, --help                 Show this message and exit.
   ```

- You'll use cookiecutter in a later lesson.

-------------

### Think about This 

- If you activate the base conda environment `cact`, is cookiecutter in the search path?
- If you activate the cookie environment, is  `datalad` in the search path?

----------------

## Solution

- cookiecutter is in an isolated environment.  It is not available from outside a conda environment, and it is not available from the base conda environment.
- The base conda environment is also isolated.  So, `cookiecutter` and `datalad` are not available from cookie.

-----

## Summary

You set up a separate conda environment and installed a tool there.  You learned that these separate environments are isolated from each other and the rest of the operating system.

-----------
## Conclusion
Cookiecutter helps you make consistent directory structures for your projects by creating a template directory structure with file stubs and using variables to fill in values in those stubs.



