# Tutorial 6: GIT Revision Control

## Cloud Shell Lessons
  * git init (1)
  * Prepare file to commit (2)
  * Make a git commit (3)
  * Revise the file and commit changes (4)
  * Summary (5)

--------------
## git init (1)

* You have a directory called **bin** in your home directory. In it you have several scripts you wrote.
* Initialize this bin as a git repository. 

* *cd* to the **bin** directory: 

  ```bash
  cd bin
  ```
* Run the git initialization command using :

  ```bash
  git init .
  ```
* This initializes the present working directory using `.` as the argument to the command.

* Initialization puts the directory under git control and creates the hidden **.git** repository internally.  

* You should see something like: 

  ```
  Initialized empty Git repository in /home/dkp/bin/.git/
  ```
* **TIP**: Initialize a **.git** repository in an empty directory if possible.  This will make it easier to synchronize it to a cloud service like GitHub.

---------------

## git status (1)

* List the contents of the directory, especially the hidden contents with the **-a** flag to see all.  You should now see your **.git** repository

  ```bash
  ls -a
  ```

* Ask git for the status of the directory:

  ```bash
  git status
  ```
* `git status` reports on what files are untracked, which are added and which are committed.  Mostly, you use it to ensure that you have a clean repository (that you have committed everything).

* Previously, you should have created several git aliases in your .bashrc, you can use these instead:

  ```terminal
  # git aliases
    alias gitcia='git add . -A; git commit'
    alias gitd='git diff' #unstaged vs last commit
    alias gitls='git log --oneline'
    alias gits='git status'
  ```
* For example, you can see the git status like this:

  ```bash
  gits
  ```
* GIT acknowledges that there are **untracked** files (yours should be similar): 

  ```terminal
   On branch master
  
  No commits yet
  Untracked files:
    (use "git add <file>..." to include in what will be committed)
          control_1.sh
          control_2.sh
          control_3.sh
          mkclassdir.sh
          mkclassdir_with_args.sh
          tree
  
  nothing added to commit but untracked files present (use "git add" to track)
  ```
-------------------

## Questions (1)

-  1.1 What does it mean to initialize a git repository?

-  1.2 What does `git status` do?

-  1.3 What alias do you have that runs `git status` for you?

---

## Prepare Files to Commit: Add Files to git (2)

* First add the files you want to commit.  In this case, commit everything in the bin directory using the dot `.`

  ```bash
  git add .
  ```
* Ask git for the status of the directory:

  ```bash
  git status
  ```

* GIT acknowledges that there are **new** files to be committed (yours should be similar): 

  ```terminal
  On branch master
  
  No commits yet
  
  Changes to be committed:
    (use "git rm --cached <file>..." to unstage)
          new file:   control_1.sh
          new file:   control_2.sh
          new file:   control_3.sh
          new file:   mkclassdir.sh
          new file:   mkclassdir_with_args.sh
          new file:   tree
  ```

* **git add** is like putting items in your shopping cart.  You have not yet committed to them, but you have picked out the ones you would like to keep.

---

## Questions (2)

-  2.1 What does `git add .` do?

-------------------

## Set up to Commit Files to GIT (2) 

* Try to commit the directory contents to GIT with a particular commit message (`-m`):

  ```bash
  git commit -m "Several bash scripts for educational purposes." 
  ```
  
* GIT provides a nice message explaining that if you have never set up git before, you need to configure a username and email (substitute your own information):

  ```terminal
  *** Please tell me who you are.

    Run  
      git config --global user.email "you@example.com"  
      git config --global user.name "Your Name"  
    to set your account's default identity.    
    Omit --global to set the identity only in this repository.  

    fatal: unable to auto-detect email address (got 'dkp@cs-748449686528-default-default-qg8tk.(none)')
  ```

* Configure the information GIT wants like so (substituing your own username)

  * WARNING: **Watch out for smart quotes!** The double quotes must be matched.  If you accidentally put in smart quotes (the tipped ones), you'll get weird behaviors, especially if you have straight double quotes on one side of the string and smart quotes on the other!
  
  
  ```bash
  git config --global user.name "dkp"
  ```
  
* Also, configure the information GIT wants like so (substituing your own email)

  ```bash
  git config --global user.email "dkp@email.arizona.edu"
  ```

* This creates a hidden configuration file, `.gitconfig`, in your home directory.  You can examine its contents:

  ```bash
  cat ../.gitconfig
  ```

* You should see something like (but with your own values):

  ```terminal
  [user]
          name = dkp
          email = dkp@email.arizona.edu
  ```


* Add some aliases to ``.gitconfig` and save it:

  ```bash
  [alias]
        co = checkout
        ci = commit
  ```
  
* Ensure the `.gitconfig` gets read by the system:

  ```bash
  git config -l
  ```


* You only need to set up this configuration information once on a computer.  Future git repositories on the same computer will use this same configuration information.

---

### Question (3)

-  3.1 If you try to use git on the HPC, will you need to do setup like this?

-------------------

## Commit files to GIT (3)

* Commit the directory contents:

  ```bash
  git commit -m "Several bash scripts for educational purposes." 
  ```

* GIT creates the commits. You should see something like: 

  ```terminal
  [master (root-commit) ffa28b7] Several bash scripts for educational purposes.
   5 files changed, 209 insertions(+)
   create mode 100755 control_1.sh
   create mode 100755 control_2.sh
   create mode 100755 control_3.sh
   create mode 100755 mkclassdir.sh
   create mode 100755 mkclassdir_with_args.sh
   create mode 100755 tree
  ```

* `git commit` actually puts the files into the **.git** repository with a message and date, so you can retrieve or examine them later.

-------------
### Check GIT status and Commits (3) 

* Ask git for the status of the directory:

  ```bash
  git status
  ```
* You should see something like:

  ```terminal
  On branch master
  nothing to commit, working tree clean  
  ```

* Ask git to see a log:

  ```bash
  git log
  ```
* Try the **gitls** alias instead.  It produces a one line log, which is slightly different:

  ```bash
  gitls
  ```
-----------------

### Questions (3)

-  3.2 What does `git log` do?

-  3.3 What does `gitls` do?

---

## Prepare a New File to Commit with GIT (3)

* Create a **Readme.txt** file in the editor:

  ```bash
  touch Readme.txt
  edit Readme.txt
  ```

* Copy and paste the following into the editor: 

  ```markdown
  # Project Title
  ## Project Aim
  ## Author(s)
  ## Date started
  ## Content
  ### Directory Structure
  ### File Naming Conventions
  ```

* In the editor, **save** the **Readme.txt** file.
* Ask git for the status of the directory:

  ```bash
  git status
  ```
* You should see:

  ```terminal
  On branch master
  Untracked files:
    (use "git add <file>..." to include in what will be committed)
          Readme.txt
  
  nothing added to commit but untracked files present (use "git add" to track)
  ```

---------------

## Add and Commit the New File with GIT (3)  

* Add the changes to git:

  ```bash
  git add .
  ```
* Ask git for the status of the directory:

  ```bash
  git status
  ```

* You should see:   

  ```terminal
  On branch master
  Changes to be committed:
    (use "git restore --staged <file>..." to unstage)
          new file:   Readme.txt  
  ```

* Commit your changes with an informative message: 

  ```bash
  git commit -m "Created Readme"
  ```

* You should see something like:

  ```terminal
  [master 01b4474] Created Readme
   1 file changed, 7 insertions(+)
   create mode 100644 Readme.txt
  ```

------------------
## Learn more about Committing with GIT (3)  

* Once again, run git status: 

  ```bash
  git status
  ```
* You should see this message indicating the everything is committed and thus clean:

  ```terminal
  On branch master  
  nothing to commit, working tree clean  
  ```
*  Run git log.  This will show you the history of your commits:

  ```bash
  git log
  ```

* You should see something like this: 

  ```terminal
  commit 01b4474c2fc8335ee457a0a8af4b4b38615d636a (HEAD -> master)
  Author: dkp <dkp@email.arizona.edu>
  Date:   Mon Feb 15 01:04:02 2021 +0000

      Created Readme

  commit ffa28b7c3716775e0fb1171509d200483d580c5
  Author: dkp <dkp@email.arizona.edu>
  Date:   Mon Feb 15 00:45:33 2021 +0000

      Several bash scripts for educational purposes.
  ```

* Try the **gitls** alias again:

  ```bash
  gitls
  ```
* You should see a compact view of the commits, like this:

  ```terminal
  01b4474 (HEAD -> master) Created Readme
  ffa28b7 Several bash scripts for educational purposes.
  ```

* The commit numbers are shorter, but this part of the commit number is all you need if you later want to refer to a commit.



---

### Question

-  3.4 How is `git commit` different than `git add`?

----------------

## Revise and Diff with GIT (4)

* The real power of GIT is that it saves changes
* Edit the Readme.md file by ading some text:

  ```bash
  edit Readme.txt
  ```
* In the editor, **save** the **Readme.txt** file **after making changes**!
* Use an alias (defined in your .bashrc) to see the status:

  ```bash
  gits
  ```

* You should see something like:

  ```terminal
  On branch master
  Changes not staged for commit:
    (use "git add <file>..." to update what will be committed)
    (use "git restore <file>..." to discard changes in working directory)
          modified:   Readme.txt
  ```


* Use an alias to see the difference between your original **Readme.txt** and the one you are about to commit:

  ```bash
  gitd Readme.txt
  ```
* **git diff** shows you the difference between the current version of your file, and the last one you checked in. This can help you when you create a commit message because it shows you exactly what you changed.
* You should see lines with additions in green, and subtractions in red. Everything else is unchanged.
* Use an alias (defined in your .bashrc) to add and commit changes in one step:

  ```bash
  gitcia -m "revised the Readme"
  ```
  You should see something like:

  ```terminal
  [master c3f55bd] revised the Readme
   1 file changed, 2 insertions(+), 2 deletions(-)
  ```

Note: There is a difference between saving the file in the editor and committing the file in git! In the first case, you are saving the file to your working directory. In the second case, you are saving (committing) the file to the git repository.

--------

## Retrieve a missing file (4)

Delete your readme file.  Because it is under git control, you can get it back:

  ```bash
  git checkout Readme.txt
  ```

Because you also configured `.gitconfig`, you should also be able to use the `co` alias for `checkout`: 

  ```bash
  git co Readme.txt
  ```



---

### Question

-  4.1 How do I retrieve a missing file with git?

------------

## Summary of GIT (5)

You have learned 
* To create a git repository, add, and commit changes
* To check the status of your directory (i.e., are all of your changes committed).
* To view a log of your past commits.
* To view the differences between your last commit and the current uncommitted version.
* To retreive a file that was accidentally deleted

-------------------
### GIT Resources (5)

* **git** is available in most Linux environments, and easily downloaded for any platform if not already installed:
  * See [Git Downloads](https://git-scm.com/downloads)
* There are many [graphical user interfaces for GIT](https://git-scm.com/downloads/guis). These can be very helpful for viewing logs and changes, editing commit messages etc.
* Remember, the hidden **.git** directory contains the git history, so don't remove it!
* In addition to keeping a local git repository, you can make a copy in the cloud using services like:
  * [github](https://github.com/) 
  * [bitbucket](https://bitbucket.org/) 
  * [gitlab](https://about.gitlab.com/)
* All of these services require you to sign up. Generally, it is easier to create the repository on the remote service first, but you can go either way. Allow some time to figure out how to do this. It sounds easier than it is. 

---

## Conclusion (5)

* GIT is the standard way to save versions of your scripts so you don't lose anything.  It allows you to annotate the changes you make and return to a previous version if the current version stops working.  

* Many tools are available on services like github: you can download them, see how they work, and even submit issues to the developer if you find a bug.  
* GIT is built for preserving small text files.  It will buckle under the pressure of backing up large binary files (like NIfTI images).  If you want to back up large files, you should check out Datalad, which is a revision control system developed with the neuroimaging community in mind.

---

