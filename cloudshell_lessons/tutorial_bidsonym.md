# Tutorial: BIDSonym

## Setup

-  Retrieve the ~5 GB Docker container

   ````bash
   docker pull peerherholz/bidsonym
   ````

-  BIDSonym is a [BIDS](https://bids-specification.readthedocs.io/en/stable/) [App](https://bids-apps.neuroimaging.io/) for de-identification of neuroimaging data. It defaces anatomical images (T1w and optionally T2w). Additionally, the user can evaluate the sidecar JSON files for potentially sensitive information, like participant names, and define a list of fields for which information should be deleted.

-  BIDSonym is relatively small (4.71 GB) and runs in ~ 5 minutes.

-  Using BIDSonym can help you make collected neuroimaging data available for others without violating subjects' privacy or anonymity (depending on the regulations of the country you're in).

-  See https://github.com/PeerHerholz/BIDSonym

-  Keep in mind that you must have permission to write to the BIDS directory to use BIDSonym! 

-  Generally, you will run BIDSonym  before you upload data to the HPC for processing or to a public data sharing site: 

   -  Simply move the `sourcedata` directory before the upload.


--------

## Download and Prepare BIDSonym Test Data

- Download the 12 MB dataset: [bidsonym_data.zip](https://osf.io/g823c/) 
  ```bash
  wget https://osf.io/g823c/download -O bidsonym_data.zip
  ```
  
- Unzip bidsonym_data.zip to reveal the MRIS directory:

   ```bash
   unzip bidsonym_data.zip
   ```
- Navigate to the resulting MRIS/data directory
  ```bash
  cd MRIS/data
  ```

---
## Questions

- 1) What subject number is in the data directory?
- 2. Does this subject directory contain a session, and if so what is it?

---

## Learn about bidsonym

Run the following command to see the bidsonym help:

```bash
docker run -i --rm  peerherholz/bidsonym /bids_dataset -h
```



---

## Questions

- 3) What are the 5 defacing options and how do you know from the bidsonym help?
- 4) If you want to deface a T2w image, what flag would you use according to the bidsonym help?

---

## T1w+T2w: mridefacer

-  mridefacer is nice because it also removes ears which the other algorithms don't!

   ```bash
   docker run -i --rm -u $UID:$GID -v ${PWD}:/bids_dataset peerherholz/bidsonym /bids_dataset  participant --deid mridefacer --participant_label 219 --brainextraction bet --bet_frac 0.5 --deface_t2w 
   ```

----

## Understand the Docker Command

-  `docker run -i --rm` runs Docker interactively and cleans up when finished.
-  `-u $UID:$GID` is used on Linux to ensure the resulting files are owned by you, the user, and not by root. On a Mac or Windows, you don't need ``-u $UID:$GID`  to retain ownership of the resulting files.  
-  `-v ${PWD}:/bids_dataset` bind mounts your present working directory `${PWD}` to the directory inside the container `/bids_dataset`
-  `peerherholz/bidsonym` is the container we are using
-  `/bids_dataset` tells the BIDS app to use this as the input
-  `participant` identifies the level to work at (the other option is `group` which processes all the participants in the BIDS data directory)
-  `--deid mridefacer` identifies the defacing algorithm to use.  Choices are `pydeface`, `mri_deface`, `mridefacer`, and `quickshear`.
-  `--participant_label 219` identifies the participant number to process.
-  `--brainextraction bet --bet_frac 0.5` selects the brain extraction algorithm and threshold. Note: it is not the case that you get an extracted brain from running BIDSonym, rather the extraction process is used internally to improve quality control.
-  `--deface_t2w` Tells BIDSonym to also deface the T2w image.



---

## Questions

- 5) What additional directory gets created by bidsonym when it runs? 
- 6) What are the contents of this additional directory?
- 7) Who owns this additional directory and how do you know?

---

## Explore sourcedata/images

-  In the Explorer Window (upper left on Google Cloud shell), navigate to `MRIS/data/sourcedata/bidsonym/sub-219/ses-itbs/images` 

   These images will allow you to determine whether the defacer has removed any brain (which we hope it has not). 

-  sub-219_ses-itbs_T1w.gif is an animated gif of the defaced T1w data.
-  sub-219_ses-itbs_T2w.gif is an animated gif of the defaced T2w data.
-  sub-219_ses-itbs_T1w_desc-brainmaskdeid.png is a lightbox style view of the T1w defacing.
-  sub-219_ses-itbs_T2w_desc-brainmaskdeid.png is a lightbox style view of the T2w defacing.

### Explore sourcedata/metadata

-  In the Explorer Window navigate to `MRIS/data/sourcedata/bidsonym/sub-219/ses-itbs/metadata`
-  Select each of the ``*.csv` files.  
-  These classify JSON fields as *problematic*, *maybe* or *no* to indicate whether they present any possible HIPAA concerns.  
-  The classifications are generally overly cautious for the research data shared here, but you should examine them to see what they are like.  This could be relevant if you work with clinical data.

---

## Questions

- 8) In the images directory, which images can you display in google cloud shell?
- 9) In the meta_data_info directory, what are the three column headers in sub-219_ses-itbs_scans_desc-jsoninfo.csv?

---

## Clean Up and Start Over

-  Remove the directory:
    ```bash
    cd; rm -fr MRIS 
    ```
- Start again: Unzip bidsonym_data.zip:
   ```bash
   unzip bidsonym_data.zip
   ```
- Navigate to the resulting MRIS/data directory
   ```bash
   cd MRIS/data
   ```

---

## T1w: pydeface + meta-data deletion 

-  pydeface from the Poldraklab is one of the best defacers out there (Theyer et al, 2021).  
    ```bash
    docker run -i --rm -v $HOME:/home/user -e "HOME=/home/user" -u $UID:$GID -v ${PWD}:/bids_dataset  peerherholz/bidsonym /bids_dataset  participant --deid pydeface --del_meta 'InstitutionAddress' --participant_label 219 --brainextraction bet --bet_frac 0.5
    ```
-  This Docker command uses `-v $HOME:/home/user -e "HOME=/home/user" -u $UID:$GID` because pydeface does nor honor `-u $UID:$GID`! This may have something to do with the issue described [here](https://neurostars.org/t/docker-fmriprep-permissionerror-errno-13-permission-denied-cache/3693/3). At any rate, mounting your home directory to the container establishes the user.
   
-  `--del_meta 'InstitutionAddress'` has been added to demostrate that BIDSonym can remove values from the BIDS JSON files for the dataset.  This might be a useful capability if you are deidentifying clinical data.  
---

## Question

- 10) What special additions to the Docker command are required to control permissions for pydeface, and why?



---

## T1w+T2w: mridefacer, skip bids validation

-  You can  run a second algorithm on the results of the first algorithm. This works surprisingly well. mridefacer also removes ears!

-  When you run again on the same data, the defacemask file (e.g., `anat/sub-219_ses-itbs_T1w_defacemask.nii.gz`) will cause BIDS validation to fail unless you skip bids validation.
    ```bash
    docker run -i --rm -u $UID:$GID -v ${PWD}:/bids_dataset peerherholz/bidsonym /bids_dataset  participant --deid mridefacer --participant_label 219 --brainextraction bet --bet_frac 0.5 --deface_t2w --skip_bids_validation
    ```
    
-  **Note**: In this case, because pydeface has already created files owned by root, you must run mridefacer as root too.

---

## Question

- 11) Why do we skip bids validation when running bidsonym a second time? 

-----

## Conclusion

-  Save space by deleting the MRIS directory when you are done.
-  Of the available algorithms, pydeface is probably the best, but mridefacer removes ears which are uniquely identifiable.  Perhaps to be really thorough, you should run both pydeface and mridefacer.
-  Caution: mri_deface is a different algorithm than mridefacer! 
-  If you are running bidsonym in a non-linux environment, like a mac, download [bidsonym_wrap.sh](https://bitbucket.org/dpat/tools/raw/master/LIBRARY/bidsonym_wrap.sh) to make it easier. bidsonym_wrap.sh runs pydeface, skips bids validation, and does not process the T2w image. Modify it as you wish.

