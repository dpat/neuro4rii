# Datalad on the HPC
✅  Start an interactive desktop on the HPC.  I suggest you allow 2 hours. If you use less, you'll only be charged for what you use.
✅  On the HPC, invoke DataLad from an interactive desktop or job submission.  
```bash
module load contrib
module load python-virtualenvs/datalad
```
Then you should be able to directly execute datalad from anywhere
 ```bash
(datalad) [zwickl@r2u12n1 ~]$ datalad --version
datalad 0.17.9
 ```

# OpenNeuro: Datalad dataset install
✅ Install a dataset (install is a superset of clone):
```bash
datalad install https://github.com/OpenNeuroDatasets/ds003550.git
```
You see the dataset download:
```bash
[INFO   ] Remote origin not usable by git-annex; setting annex-ignore
[INFO   ] https://github.com/OpenNeuroDatasets/ds003550.git/config download failed: Not Found
[INFO   ] access to 1 dataset sibling s3-PRIVATE not auto-enabled, enable with:
| 		datalad siblings -d "/Users/dpat/test/ds003550" enable -s s3-PRIVATE
install(ok): /Users/dpat/test/ds003550 (dataset)
```
✅ Navigate into the directory:
```bash
cd ds003550
```
✅ How big is it?
```bash
du -s -h
```
My copy is 2.7M (2.7 Megabytes) `2.7M`


✅ Learn about its history (--oneline keeps the log output small):
```bash
git log -–oneline
```
```bash
a65680c (HEAD -> master, tag: 1.0.1, origin/master, origin/HEAD) [DATALAD] Recorded changes
31aace7 [DATALAD] Recorded changes
07a1333 [DATALAD] Recorded changes
922ef3f [DATALAD] Recorded changes
bca65c6 [DATALAD] Recorded changes
538452d [DATALAD] Recorded changes
4948b01 [DATALAD] Recorded changes
a335f3b [DATALAD] Recorded changes
43d8677 (tag: 1.0.0) [DATALAD] Recorded changes
be23d5f [DATALAD] Recorded changes
1a3614a [DATALAD] exclude paths from annex'ing
b27f41c [DATALAD] new dataset
```

✅ How big is the original online dataset?
```bash
datalad status --annex
```
The actual dataset is MUCH larger (8.9 GB)!
```bash
114 annex'd files (8.9 GB recorded total size)
nothing to save, working tree clean
```
Your clone has the dataset structure and text files, but not the large binary images (these are just broken links at the moment). You can browse the directores.

# OpenNeuro: datalad get and drop
✅ Get some of the actual image data. Navigate into the sub-sc001 directory and get everything:
```bash
cd sub-sc001
datalad get .
```
```bash
get(ok): sub-sc001/anat/sub-sc001_T1w.nii.gz (file) [from s3-PUBLIC...]
get(ok): sub-sc001/func/sub-sc001_task-RepMem1_bold.nii.gz (file) [from s3-PUBLIC...]
get(ok): sub-sc001/func/sub-sc001_task-RepMem2B_bold.nii.gz (file) [from s3-PUBLIC...]
get(ok): sub-sc001 (directory)
action summary:
  get (ok: 4)
```

✅ How big is the directory now?
```bash
sub-sc001 % cd ..
ds003550 % du -s -h
253M	.
```

✅ Open and view one of the images for sub-sc001. The image is still a link to data in the .git/annex directory, BUT, the link is no longer broken!
✅ Download just the anatomical files for each subject with sub-sc00 in the directory name:
```bash
ds003550 % for s in sub-sc00*; do
for> datalad get ${s}/anat
for> done
```
One by one, they download:
```bash
get(ok): sub-sc002/anat/sub-sc002_T1w.nii.gz (file) [from s3-PUBLIC...]
get(ok): sub-sc002/anat (directory)
action summary:
  get (ok: 2)
get(ok): sub-sc003/anat/sub-sc003_T1w.nii.gz (file) [from s3-PUBLIC...]
get(ok): sub-sc003/anat (directory)
action summary:
  get (ok: 2)
```
Now the directory contains the anat images for the first several subjects and is 310 MB in size. 
## datalad drop
Reverse the effects of get with drop, by running the following command from ds003550:
```bash
datalad drop .
```
The directory is back to being 2.7 MB!



# Nested Datasets
<u>Goal: Understand what datalad nesting is and why it is so important.</u>

✅ Install the abide dataset from the Datalad repository.  This retrieves the structure of the top-level dataset
```bash
datalad install https://datasets.datalad.org/abide
```
You see:
```bash
install(ok): /Users/dpat/test/abide (dataset)
```
Although the complete dataset is 266 GB, this clone is a mere 2.9 MB!

✅  However, the dataset includes subdatasets, and the repository information on those subdatasets has not yet been installed. Use the -r option to install all subdatasets:
```bash
datalad install -r https://datasets.datalad.org/abide
```
As datalad installs each separate dataset, it reports its progress:
```bash
install(ok): /Users/dpat/test/abide (dataset)
[INFO   ] Installing Dataset(/Users/dpat/test/abide) to get /Users/dpat/test/abide recursively
install(ok): /Users/dpat/test/abide/Derivatives (dataset)
install(ok): /Users/dpat/test/abide/PhenotypicData (dataset)
[INFO   ] scanning for annexed files (this may take some time)
install(ok): /Users/dpat/test/abide/RawData (dataset)
install(ok): /Users/dpat/test/abide/RawDataBIDS (dataset)
install(ok): /Users/dpat/test/abide/Resources (dataset)
[INFO   ] scanning for annexed files (this may take some time)
install(ok): /Users/dpat/test/abide/Derivatives/ccs_nofilt_global (dataset)
[INFO   ] scanning for annexed files (this may take some time)
install(ok): /Users/dpat/test/abide/Derivatives/cpac_nofilt_global (dataset)
install(ok): /Users/dpat/test/abide/Derivatives/dparsf_nofilt_global (dataset)
install(ok): /Users/dpat/test/abide/Derivatives/niak_nofilt_global (dataset)
  [24 similar messages have been suppressed; disable with datalad.ui.suppress-similar-results=off]
action summary:
  install (ok: 34)
```
This clone, even with the subdatasets, is still quite small: 64 MB. 

>One aspect of nested datasets is that any lower-level DataLad dataset (the subdataset) has a stand-alone history. The top-level DataLad dataset (the superdataset) only stores which version of the subdataset is currently used" 
>(1.5. Dataset nesting — The DataLad Handbook). 

This means that git log will have different commits depending on which dataset you query.



# Top level Commits
✅ Navigate to the top level of the abide directory, and use git to view the history of the dataset:
```bash
git log --oneline
```
```bash
b0dee83 (HEAD -> master, origin/synced/master, origin/master, origin/HEAD) Recrawled abide/RawDataBIDS to get fresh versions of some adjusted sidecar files etc
03abcae update to abide/Derivatives/dparsf_nofilt_global from June 2020
c0efd7f updated abid/Derivatives/dparsf_nofilt_global
ae8c908 updated abid/Derivatives/cpac_nofilt_global
036db51 Some subdatasets within abide/RawDataBIDS were adjusted/recrawled
62c1f74 (tag: 0.0.20180523+1) [DATALAD] Updated git/annex from a remote location
6c048bf (tag: 0.0.20180523) [DATALAD] Updated git/annex from a remote location
f660781 [DATALAD] Dataset aggregate metadata update
2bf92d4 ABIDE: Assigned metadata extractors (bids, nifti1)
2ab5ea3 [DATALAD] aggregated meta data
306bb4f [DATALAD] auto-saved changes
8249ad6 [DATALAD] added content
6d9b021 [DATALAD] removed content
a7ed9fe .csv should go into annex
1a96cc9 actually generated subdatasets for each derivative
7b29ca5 will generate subdataets for each one
6cf8aff (tag: 0.0.20161216) [DATALAD] Updated git/annex from a remote location
aa08acb initial crawl config
0800e99 [DATALAD] new dataset
e7da60e [DATALAD] Instructed annex to add text files to git
5f337fc [DATALAD] Set default backend for all files to be MD5E
```
You may need to use `q` to quit the displayed repository information.

## abide/Derivatives Commits
✅ Navigate to the subdataset abide/Derivatives and use git to view the history of the dataset:
```bash
git log --oneline
```
```bash
bc98422 (HEAD -> master, origin/synced/master, origin/master, origin/HEAD) update to abide/Derivatives/dparsf_nofilt_global from June 2020
13186c8 updated abid/Derivatives/dparsf_nofilt_global
ed25142 updated abid/Derivatives/cpac_nofilt_global
94ae8d0 [DATALAD] a commit
2efb481 [DATALAD] Dataset aggregate metadata update
f9424c8 ABIDE: Assigned metadata extractors (bids, nifti1)
680e452 [DATALAD] aggregated meta data
f22d688 [DATALAD] auto-saved changes
08d302c [DATALAD] auto-saved changes
dce306f [DATALAD] a commit
dfbfac9 will generate subdataets for each one
f42cbdb [DATALAD] Initialized crawling configuration to use template simple_s3
bac1724 [DATALAD] new dataset
bfed197 [DATALAD] Set default backend for all files to be MD5E
```

The commits are different because the dataset was stored as a separate nested subdataset.

### abide/Derivatives/cpac_nofilt_global Commits
And there are further nested datasets under derivatives, e.g., abide/Derivatives/cpac_nofilt_global:
```bash
7a6df948 ABIDE: Assigned metadata extractors (bids, nifti1)
eb75eaae [DATALAD] aggregated meta data
77922511 [DATALAD] Initialized crawling configuration to use template simple_s3
12c0ddb3 [DATALAD] new dataset
ad4b3ce4 [DATALAD] Set default backend for all files to be MD5E
```



# Install TemplateFlow with Datalad
✅ Download the templates with Datalad (see TemplateFlow Archive):
```bash
datalad install -r ///templateflow
```

✅ Navigate into the new templateflow directory.
The download contains template directories:
```bash
dataset_description.json
tpl-MNI152Lin
tpl-MNI152NLin2009cAsym
tpl-MNI152NLin2009cSym
tpl-MNI152NLin6Asym
tpl-MNI152NLin6Sym
tpl-MNIInfant
tpl-MNIPediatricAsym
tpl-NKI
tpl-OASIS30ANTs
tpl-PNC
tpl-WHS
tpl-fsLR
tpl-fsaverage
```

# Understand Storage in .git/annex
Each templateflow directory contains links to binary image files that you don't have:
```bash
ls -lR tpl-MNI152Lin/*nii.gz
```

This long listing reveals that the binary files are links deep into the .git/annex directory.  Links are small and the actual binary contents of the files have not yet been downloaded.
```bash
lrwxr-xr-x  1 dpat  staff  186 Dec  5 12:55 tpl-MNI152Lin/tpl-MNI152Lin_res-01_PD.nii.gz -> .git/annex/objects/5m/4z/URL-s10250635--https&c%%files.osf.io%v1%resourc-d38cc6938c26e9389a1a9acf03f5a4b6/URL-s10250635--https&c%%files.osf.io%v1%resourc-d38cc6938c26e9389a1a9acf03f5a4b6
lrwxr-xr-x  1 dpat  staff  186 Dec  5 12:55 tpl-MNI152Lin/tpl-MNI152Lin_res-01_T1w.nii.gz -> .git/annex/objects/pM/Fm/URL-s10669511--https&c%%files.osf.io%v1%resourc-2e59511114a1686f937e0127af887b83/URL-s10669511--https&c%%files.osf.io%v1%resourc-2e59511114a1686f937e0127af887b83
lrwxr-xr-x  1 dpat  staff  186 Dec  5 12:55 tpl-MNI152Lin/tpl-MNI152Lin_res-01_T2w.nii.gz -> .git/annex/objects/63/jK/URL-s10096230--https&c%%files.osf.io%v1%resourc-7ee9c493542a55d96d28d55d57a3ee52/URL-s10096230--https&c%%files.osf.io%v1%resourc-7ee9c493542a55d96d28d55d57a3ee52
lrwxr-xr-x  1 dpat  staff  182 Dec  5 12:55 tpl-MNI152Lin/tpl-MNI152Lin_res-01_desc-brain_mask.nii.gz -> .git/annex/objects/J4/J9/URL-s131839--https&c%%files.osf.io%v1%resourc-4a92beb360af57cc397642c99e4f34ee/URL-s131839--https&c%%files.osf.io%v1%resourc-4a92beb360af57cc397642c99e4f34ee
lrwxr-xr-x  1 dpat  staff  182 Dec  5 12:55 
```

✅ Use datalad get to retrieve an actual file.   
```bash
datalad get tpl-MNI152Lin/tpl-MNI152Lin_res-01_PD.nii.gz
```
You can view it, but it still resides in ./git/annex. 
If you drag and drop the link tpl-MNI152Lin_res-01_PD.nii.gz to somewhere else on your machine, it won't work.  The link is broken.



# Extract files from datalad with cp

Goal: extract an individual file from datalad control
✅ Copy the file using the command line: the actual binary data rather than the link should be copied. 
```bash
cp tpl-MNI152Lin/tpl-MNI152Lin_res-01_PD.nii.gz ..
```
✅ Create a target directory:
```bash
mkdir ../test_cp_folder_contents
```

✅ Copy multiple files:
```bash
cp -R tpl-MNI152Lin/* ../test_cp_folder_contents
```
The files and subdirectories are recursively extracted from datalad control and placed into the target folder.

# Extract a dataset from datalad with export-archive
<u>Goal: extract a dataset from datalad control</u>

✅ First, from the templateflow superdataset, use datalad get to retrieve all the binary files in the small tpl-MNI152Lin dataset:
```bash
datalad get tpl-MNI152Lin
```

✅ From the templateflow superdataset, export the dataset as an archive: 
```bash
datalad export-archive -d tpl-MNI152Lin ../export_test
```

Datalad should report its progress:
```bash
export_archive(ok): /Volumes/Main/Working/Tool_Testing/datalad/data/inputs/templateflow/../export_test.tar.gz (file)
```

✅ After navigating to the location of the tar file,  untar it:  
```bash
 tar xvf export_test.tar.gz
```
All of the `nii.gz` files have been converted from links into stand-alone files. The `.git` directory has been removed.

# View Metadata
Although the large binary image files have not been downloaded, JSON files are text and therefore present in the dataset. For example, each template directory contains a JSON file: `template_description.json` with the informative fields Identifier and Name.
Use `jq` to display just those fields, thus demonstrating that they exist, and getting some useful information about the template flow dataset.

```bash
for t in tpl*; do echo "==="; echo ${t}; jq '.Identifier, .Name'  ${t}/template_description.json; done
```
```
tpl-MNI152Lin
"MNI152Lin"
"Linear ICBM Average Brain (ICBM152) Stereotaxic Registration Model"
===
tpl-MNI152NLin2009cAsym
"MNI152NLin2009cAsym"
"ICBM 152 Nonlinear Asymmetrical template version 2009c"
===
tpl-MNI152NLin2009cSym
"MNI152NLin2009cSym"
"ICBM 152 Nonlinear Symmetrical template version 2009c"
===
tpl-MNI152NLin6Asym
"MNI152NLin6Asym"
"FSL's MNI ICBM 152 non-linear 6th Generation Asymmetric Average Brain Stereotaxic Registration Model"
===
tpl-MNI152NLin6Sym
"MNI152NLin6Sym"
"ICBM 152 non-linear 6th Generation Symmetric Average Brain Stereotaxic Registration Model"
===
tpl-MNIInfant
"MNIInfant"
"MNI Unbiased nonlinear Infant Atlases 0-4.5yr."
===
tpl-MNIPediatricAsym
"MNIPediatricAsym"
"MNI's unbiased standard MRI template for pediatric data from the 4.5 to 18.5y age range"
===
tpl-NKI
"NKI"
"ANTs' NKI-1 template"
===
tpl-OASIS30ANTs
"OASIS30ANTs"
"OASIS30 - Generated with ANTs for MICCAI 2016"
===
tpl-PNC
"PNC"
"Philadelphia Neurodevelopmental Cohort template"
===
tpl-WHS
"WHS"
"Waxholm Space atlas of the Sprague Dawley rat brain"
===
tpl-fsLR
"fsLR"
"HCP Pipelines' fsLR template"
===
tpl-fsaverage
"fsaverage"
"FreeSurfer's fsaverage template"
```



# Human Connectome Project 

✅ Install the HCP1200 dataset with Datalad (2.3 MB):
```bash
datalad install ///hcp-openaccess
```
Each HCP1200 subject directory in this dataset is a DataLad subdataset, and the content is not available to browse. 

✅  Navigate to hcp-openaccess/HCP1200 directory, and install any individual subject directory: e.g.,
```bash
datalad install 100206
```

In the subject dataset, a DataLad subdataset exists for each additional available subdirectory (e.g., MEG, T1w, etc., as far as available for the particular subject). 

>The command datalad get -n <subject-id> clones this subdataset and allows access to this subject's release notes (subdirectory release-notes).  >http://datasets.datalad.org/?dir=/hcp-openaccess


✅ Run the following datalad get command with -n. 

```bash
datalad get -n 100307
```

This retrieves just the release notes, but not the other subdatasets for this subject.
```bash
install(ok): /Volumes/Main/Working/Tool_Testing/datalad/data/inputs/hcp-openaccess/HCP1200/100307 (dataset) [Installed subdataset in order to get /Volumes/Main/Working/Tool_Testing/datalad/data/inputs/hcp-openaccess/HCP1200/100307]
```

✅ Run the install command recursively with -r to install subdatasets of a given dataset
```bash
datalad install -r 100206
```

This downloads all the subdataset structure for the given subject. It takes longer than installing the original HCP 1200 dataset! However, it provides a good look at the contents of one of these subject directories. 
```bash
install(ok): /Volumes/Main/Working/Tool_Testing/datalad/data/inputs/hcp-openaccess/HCP1200/100206 (dataset) [Installed subdataset in order to get /Volumes/Main/Working/Tool_Testing/datalad/data/inputs/hcp-openaccess/HCP1200/100206]
[INFO   ] Installing Dataset(/Volumes/Main/Working/Tool_Testing/datalad/data/inputs/hcp-openaccess) to get /Volumes/Main/Working/Tool_Testing/datalad/data/inputs/hcp-openaccess/HCP1200/100206 recursively
[INFO   ] scanning for annexed files (this may take some time)
install(ok): /Volumes/Main/Working/Tool_Testing/datalad/data/inputs/hcp-openaccess/HCP1200/100206/MNINonLinear (dataset)
install(ok): /Volumes/Main/Working/Tool_Testing/datalad/data/inputs/hcp-openaccess/HCP1200/100206/T1w (dataset)
install(ok): /Volumes/Main/Working/Tool_Testing/datalad/data/inputs/hcp-openaccess/HCP1200/100206/unprocessed (dataset)
action summary:
  install (ok: 4)
```

✅  Compare the directories for 100307 and 100206 to see the differences.

Neither of the above commands actually installs image files. Those are all broken links, but after running datalad install -r, you see all of their names and the way the subject directory is organized.



# Nesting and .gitmodules

The Human Connectome data is deeply nested. Look at .gitmodules in each directory to see nesting information about its children. For example,
✅ Display the text file `hcp-openaccess/.gitmodules` to see the nested subject directories
✅ Display `hcp-openaccess/HCP1200/100206/.gitmodules` to see the subdatasets nested under that subject.