# Tutorial: ROIxtractor

ROIxtractor is implemented in the scif_fsl container. Documentation is available here: 
https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/choropleths.html#roixtractor

------

## Setup

- Retrieve the docker container

  ```bash
  docker pull diannepat/scif_fsl
  ```

- Create a working directory for testing:

  ```bash
  mkdir xtract
  ```

- Retrieve sfw.sh to your bin directory and the sample data to your xtract directory with wget.  Make sfw.sh executable:

  ```bash
  wget https://bitbucket.org/dpat/scif_fsl/raw/master/sfw.sh -O bin/sfw.sh
  wget https://osf.io/uty3v/download -O xtract/run-01_IC-06_Russian_Unlearnable.nii
  wget https://osf.io/hc5tg/download -O xtract/sample_lesion_MNI.nii.gz
  chmod a+x bin/sfw.sh
  ```



---

## Navigate to the **xtract** directory

```bash
cd xtract
```

You should have two test files:

- *run-01_IC-06_Russian_Unlearnable.nii* 
  - the ICA output from GIFT for an fMRI analysis. Voxel values are t-statistics.

- *sample_lesion_MNI.nii.gz* 
  - a binary mask file for a lesion.  This means it has the value 1 where there is lesioned tissue, and the value 0 elsewhere.


----

## roixtractor: building up the command line

-  *sfw.sh* includes several tools. You want *xtract*, because you are running roixtractor: 
   `sfw.sh xtract`.

-  **xtract**: Indicate which of the three xtract functions to run: `Stats`, `Lat`, and `Mask`

-  **Input file**: Specify the MNI space input file.  

   Tip: If the input is not in 2mm FSL MNI space (e.g., it is in SPM space, or 1mm space), xtractor will convert it and save the original, e.g, If *sub-001_tstats.nii.gz* did not conform, then it would be backed up to *sub-001_tstats_orig.nii.gz*.  *Explanation: The two MNI spaces are one-off from each other*

-  **Threshold**: Use 0 if you want to see all positive values, and use 0 for binary masks.  

   -  If you have a statistical image, you may want to threshold it some appropriate statistical value so you get less output (not all regions will meet the criterion), e.g., `1.3`

-  **Atlas**: Specify the atlas xtract should use: ARTERIAL1, ARTERIAL2, HO-CB, HCP-MMP1

-  **Example**: `sfw.sh xtract Stats sub-001_tstats.nii.gz 1.3 ARTERIAL1` 

An output CSV will be generated and named to includes the name of the atlas, input image, threshold, and function (stats, lat or mask)



------

## Stats

If you run Stats, roixtractor will generate a CSV file and a NIfTI image containing the regions from the chosen atlas that meet the threshold.

```bash
sfw.sh xtract Stats run-01_IC-06_Russian_Unlearnable.nii 1.3 HO-CB
```



---

### Stats Output

Open the csv file: *HO-CB_run-01_IC-06_Russian_Unlearnable_1.3_stats.csv*.

The output CSV file contains: 

-  Region: the short region name;
-  Mean, Peak, and SD are descriptive statistics for each region:
   -  The mean is the average value in the region, whereas the peak is the maximum value in the region.

-  LR: identifies the region as left or right (redundant, but useful for sorting)
-  Lobe: The lobe in which that region is found
-  CogX, CogY and CogZ are the x,y,z center of gravity voxel coordinates for each region
-  RegionLongName: A more detailed name for the region if available
-  RegionVol_MM3: The volume of the region in cubic mm
-  Image: The name of the image you passed to the program: the name of the atlas you used is prepended and the threshold you used is appended to the name. Because image is the last field, you can easily use Excel to split the data on underscores. This splitting is useful if you want to concatenate several CSVs from different runs of the xtractor.

Output NIfTI image: 

-  In addition to the csv file, the Stats function outputs a 3D choropleth image of the mean value in each region. 
-  This provides a quick visual impression of which regions have the highest values.

-  Sadly, there is no easy way to view this file on Google Cloud shell!



---

## Questions

- 1) What is the first region listed in HO-CB_run-01_IC-06_Russian_Unlearnable_1.3_stats.csv?

- 2) How many rows are in  HO-CB_run-01_IC-06_Russian_Unlearnable_1.3_stats.csv?

- 3) If you run the stats command again with a higher threshold (1.5), would you expect to have more rows in the csv file, or fewer? (Try it)

- 4) What is the difference between the mean and the peak values produced by roixtractor?



## Lat

The CSV file will contain laterality information based on [Wegrzyn, M., Mertens, M., Bien, C. G., Woermann, F. G., & Labudda, K. (2019). Quantifying the Confidence in fMRI-Based Language Lateralisation Through Laterality Index Deconstruction. Frontiers in Neurology, 10, 106915](http://doi.org/10.3389/fneur.2019.00655)

The number of suprathreshold voxels in each region is reported for left and right homologs.

```bash
sfw.sh xtract Lat run-01_IC-06_Russian_Unlearnable.nii 1.3 HO-CB
```

---

### Lat Output

Open the CSV file: *HO-CB_run-01_IC-06_Russian_Unlearnable_1.3_LI.csv*.  
It is similar to the stats output, but contains:

-  VoxSuprathresh_L is the number of suprathreshold voxels on the left 
-  VoxSuprathresh_R is the number of suprathreshold voxels on the right 
-  Strength=L+R 
-  Laterality=L-R 
-  LI (laterality index)=laterality/strength

---

## Questions

- 5) In HO-CB_run-01_IC-06_Russian_Unlearnable_1.3_LI.csv, is the Amygdala left or right lateralized? How do you know?

- 6) In HO-CB_run-01_IC-06_Russian_Unlearnable_1.3_LI.csv, is the CRUS_I left or right lateralized? How do you know?

-----

## Mask

The CSV will contain categorical information to characterize the region and two measures: 

```bash
sfw.sh xtract Mask sample_lesion_MNI.nii.gz 0 HO-CB
```

---

### Mask Output

Open the CSV file: *HO-CB_sample_lesion_mask.csv*

Again, it is similar to the Stats file, but contains:

-  *MaskVol_in_Region_mm3* is the cubic mm in the region that are also in the mask.
-  *MaskVol_in_Region_Percent* is the percentage of voxels in the region that are also in the mask. 



---

- 7) In HO-CB_sample_lesion_MNI_mask.csv, is the lesion on the left or the right? How do you know?

- 8) In HO-CB_sample_lesion_MNI_mask.csv, how many regions are impacted by the lesion?

- 9) In HO-CB_sample_lesion_MNI_mask.csv, which region is most impacted (greatest percentage) by the lesion?

---

## Summary

-  roixtractor provides regional information for any standard MNI space image

-  Three different functions are available: Stats, Lat and Mask
-  Several atlases are also provided.