# Tutorial: Heudiconv Demystified

------

This dataset is free from all HIPAA restrictions.  However, **most DICOMS contain protected health information, and you should NOT use the Google Cloudshell to convert those DICOMS to NIfTI**. 

## Cloud Shell Lessons

Introduction and Setup (0)   
Stage 1: Parsing the DICOMS (1)   
Stage 2: Edit the translator file (2)    
Test It (3)   
Summary (4)    

---

## Setup (0)

- Retrieve the data:

  ```bash
  wget https://osf.io/39qdn/download -O MRIS_HDC.zip
  unzip MRIS_HDC.zip
  ```

- Retrieve the scripts and make them executable 
  ***Note**: This assumes you have a bin directory and that it is in your path*:

  ```bash
  wget https://bitbucket.org/dpat/tools/raw/master/LIBRARY/hdc1.sh -O bin/hdc1.sh; chmod a+x bin/hdc1.sh 
  
  wget https://bitbucket.org/dpat/tools/raw/master/LIBRARY/hdc2.sh -O bin/hdc2.sh; chmod a+x bin/hdc2.sh; chmod a+x bin/hdc2.sh 
  ```

## Go to the Data Directory and Check the Setup (0)

-  Navigate to the **MRIS_HDC** directory and ensure that it contains the Dicom subdirectory:

    ```bash
    cd MRIS_HDC
    ```

-  Ensure that *hdc1.sh* is in your path.  It should produce a usage message:

   ```bash
   hdc1.sh
   ```

   -  Read the usage message, it is helpful if you run into problems later.

-  Ensure that *hdc2.sh* is in your path.  It should produce a usage message:

    ```bash
    hdc2.sh
    ```

    -  Read the usage message, it is helpful if you run into problems later.

----

## Stage 1: Parse the DICOM directory and generate a template translator (1)

**From the MRIS_HDC directory**, run *hdc1.sh* by specifying the Dicom directory, the bids directory (it will be created if it does not exist), the subject number and the session:

```bash
hdc1.sh Dicom data 219 itbs  
```

*hdc1.sh* descends into the Dicom directory looking for sequences under `Dicom/219/itbs`.  It does not matter how deeply nested those directories are, or if there are spaces in the directory names, or whether the DICOM files have `*.IMA` or `*.dcm` extensions.

## Open Output of Stage 1 (1)

The `data` directory gets created with a `code` subdirectory.  In `data/code` you should find two files:

-  The generic translator file is called *heuristic_template_sess.py* if it includes a session, or *heuristic_template.py* if there are no sessions. It includes typical sequences, but the DICOM sequences you **actually** have and the names of the DICOM directories will likely be different. 
   
   -  You need to edit this translator file to work with your dataset.
   
   -  Back up *heuristic_template_sess.py* before you edit it:
   
      ```bash
      cp data/code/heuristic_template_sess.py data/code/heuristic_template_sess_back.py
      ```
   
   -  In the Google Cloud shell explorer (on the left): Double-click *heuristic_template_sess.py* to open it in the Cloudshell editor.
   
-  *dicominfo_sub-219_ses-itbs.tsv* is critical to determining the criteria used to test DICOM series for inclusion in the BIDS directory. 
   -  Right-click *dicominfo_sub-219_ses-itbs.tsv*  in the Google Cloud shell explorer window (on the left) and download it to your local machine so you can open it with a spreadsheet editor like Excel.  If Excel does not recognize the TSV format, open a new blank Excel file and import  *dicominfo_sub-219_ses-itbs.tsv* using TAB as the separator.  


---

## Stage 2: Edit the translator file *heuristic_template_sess.py* in the Cloudshell editor (2)

-  Use *dicominfo_sub-219_ses-itbs.tsv* to inform your revisions of *heuristic_template_sess.py*. That is, *heuristic_template_sess.py* includes the right sections, syntax, formatting and several common sequences.  However, the names and characteristic of the DICOMS in this tutorial (and your DICOMS in the future) may or may not match and you'll have to check those VERY carefully.
-  If Google Cloud shell offers to install pylinter, say *yes*.  I installed it with pip but conda is probably fine too.  pylint will help the editor identify errors like variables that are misspelled, undefined, or unused.

## Edit Section 1 of *heuristic_template_sess.py*(2)

Section 1 defines names for the BIDS data. Each row consists of a variable key name, e.g. `anat_t1w`, that you decide and then a rule for naming that file and placing it in the BIDS directory structure:

```python
create_key('sub-{subject}/{session}/anat/sub-{subject}_{session}_T1w'):
```

BIDS is very consistent so these are fine, except there are files defined here that you don't have:

-  Remove lines corresponding to sequences you don't have.  For example, there is no flair in this dataset, so delete that line.  

   ***Note**: If your dataset included other files not covered in the template translator, then you need to create entries for those by following the BIDS specification.*

---

### Question (2)

-  1)  Which other sequences are defined in the *heuristic_template_sess.py* that you don't need for this dataset?

---

## Edit Section 1b of *heuristic_template_sess.py* (2)

Section 1b defines a Python dictionary. A dictionary is like a list.  This list consists of all and only the variable names defined in Section 1, e.g, `anat_t1w: []`etc.  The entries must be separated by commas.  

-  Remove entries corresponding to the flair and t2w variables, because you don't have either of these.
-  ***Note**: If you add entries to Section 1, then you need to create corresponding entries in the dictionary.*

-----

## Edit Section 2 of *heuristic_template_sess.py* (2)

Section 2 consists of a for-loop containing a conditional expression for each sequence you want to convert to BIDS.  At this point, all and only the variables in section 1 and 1b should be included.  

-  Remove the conditionals corresponding to sequences you don't have:

```python
  # FLAIR: The string 'FLAIR' must appear somewhere in the protocol_name
  if ('FLAIR' in s.protocol_name):
      info[anat_flair].append(s.series_id)
  # T2w hippocampus image: the protocol_name must equal 'HighResHippocampus'
  if ('HighResHippocampus' == s.protocol_name):
      info[anat_t2w].append(s.series_id)
```

-  ***Note**: If you added variables in sections 1 and 1b, add corresponding conditionals to section 2*

----

## Anatomy of a Python Conditional (2)

-  The hardest thing you will have to do is to define the conditionals based on the information in *dicominfo_sub-219_ses-itbs.tsv* and the [heudiconv_cheatsheet.pdf](https://drive.google.com/file/d/1mG3ThroM_SAT7R4BBoy8retsa8eNTIam/view?usp=sharing).  Open these two documents on your local desktop.

-  There are a lot of columns in *dicominfo_sub-219_ses-itbs.tsv*, I suggest hiding colums of no interest (those that contain no distinguishing information or are subject-specific).  

-  When you identify a cell you want to use as criteria, you can actually copy and paste from Excel on your local desktop to Google Cloud shell, thus preventing typos.

-  Each conditional is **indented** under the for loop.  In Bash, this is just stylish, but Python actually relies on the indentation to determine how the code is organized!  

-  Each conditional consists of an if statement followed by an indented line that explains what to do if the condition is met. The following example says "If the string MPRAGE appears *in* the series protocol name, then this is the anat_t1w variable".

  ```python
  # T1w: the string 'mprage' must appear somewhere in the protocol_name
  if ('MPRAGE' in s.protocol_name):
      info[anat_t1w].append(s.series_id)
  ```

-  Is this correct for our dataset?  Look at  *dicominfo_sub-219_ses-itbs.tsv*.  Protocol name is column M, and the T1w image is defined in row 14.  The translator is wrong.  It should say:

   ```python
   # T1w: the string 'T1_mprage_1mm' must appear somewhere in the protocol_name
   if ('T1_mprage_1mm' in s.protocol_name):
       info[anat_t1w].append(s.series_id)
   ```
   
- Take a moment to make sure that this uniquely defines the T1w image, and that you didn't misspell it (remember you can copy and paste).

- Note, it is mostly the first line of each conditional that you should alter. 

- However, check the indented line (e.g., `info[anat_t1w].append(s.series_id)`) to ensure it references the correct variable name.

-----

## Check the Rest of the Conditionals (2)

The translator file provides examples of several kinds of critera, e.g.,

A string must appear in the field (e.g. protocol_name).  This may often be all you need if you have defined a unique protocol name for each sequence!

Sometimes the string is not enough.  This is true for field maps which must be distinguished by more than just the protocol_name.  The translator requires that for the magnitude image, dim3 must be 64, whereas for the phasediff, dim3 is 32.  A check of *dicominfo_sub-219_ses-itbs.tsv* reveals that for this dataset, the assumption is correct.

You have the opportunity to add more criteria.  For example, if there are acquisiton issues that sometimes result in sequences with incorrect dimensions (like the wrong number of timepoints in the fMRI scan), you could add a check here.  That way, a sequence that has the incorrect values will not get converted.

Generally you do not want derived or motion corrected (MOCO) images. If your dataset includes motion corrected images (usually for fMRI) and derived images (usually for DWI or ASL), use criteria from the **BOOLEAN** (TRUE FALSE) fields *is_motion_corrected* and *is_derived*.  

 ```python
  # FMRI: The protocol name must equal 'RS-FMRI-MB' AND the sequence must NOT be motion corrected or any derived.
 if ('RS-FMRI-MB' == s.protocol_name) and (not s.is_motion_corrected) and (not s.is_derived):
     info[func_fmri_run1].append(s.series_id)
 ```

You can also use values from the field *image_type*.  This field is called a **tuple** and used like this:

```python
# ASL: If the protocol name is equivalent to ASL_3D_tra_iso and the value 'TTEST' appears in the image_type field
if ('ASL_3D_tra_iso' == s.protocol_name) and ('TTEST' in s.image_type):
   info[asl_der].append(s.series_id)
```

See the [heudiconv_cheatsheet.pdf](https://drive.google.com/file/d/1mG3ThroM_SAT7R4BBoy8retsa8eNTIam/view?usp=sharing) for more examples.

---

### Question(2)

-  2.  Which criteria did you have to change to alter the translator ?

If you really get stuck, see the [sample finished translator file for tutorial](https://drive.google.com/file/d/1gO6P2Z32T5b4W7C2k3iAtnZJIexPpHbQ/view?usp=sharing). 
This is not the only workable example, so your solution may be different! 

---

## Stage 3: Test it (3)

### Class Credit (3)

If you are taking COGS 510, you need to record your terminal session and turn it in.

```bash
script heudiconv_${USER}_${DATE}.txt
```

---

### Test your Translator (3)

**From the MRIS_HDC directory**, run hdc2.sh like this:

```bash
hdc2.sh Dicom data heuristic_template_sess.py 219 itbs
```

Examine your results: `ls -R data`.   Did all of the images you expected get generated? 

Below I list the results using a great little program called `tree`.  As you can see, the output of `tree` is easier to look at than the output of `ls -R`.

(Run tree in the directory `MRIS_HDC/data/sub-219/ses-itbs` else you'll be overwhelmed by all the files in the DICOM directory)

If the translator file was modified correctly, then all five directories are created and populated.  Compare your results to the results below.

It's okay if you have some extra files, like a Readme, but to succeed you need to have these image files:

```terminal
└── ses-itbs
    ├── anat
    │   ├── sub-219_ses-itbs_T1w.json
    │   └── sub-219_ses-itbs_T1w.nii.gz
    ├── dwi
    │   ├── sub-219_ses-itbs_dir-AP_dwi.bval
    │   ├── sub-219_ses-itbs_dir-AP_dwi.bvec
    │   ├── sub-219_ses-itbs_dir-AP_dwi.json
    │   └── sub-219_ses-itbs_dir-AP_dwi.nii.gz
    ├── fmap
    │   ├── sub-219_ses-itbs_dir-PA_epi.json
    │   ├── sub-219_ses-itbs_dir-PA_epi.nii.gz
    │   ├── sub-219_ses-itbs_magnitude1.json
    │   ├── sub-219_ses-itbs_magnitude1.nii.gz
    │   ├── sub-219_ses-itbs_magnitude2.json
    │   ├── sub-219_ses-itbs_magnitude2.nii.gz
    │   ├── sub-219_ses-itbs_phasediff.json
    │   └── sub-219_ses-itbs_phasediff.nii.gz
    ├── func
    │   ├── sub-219_ses-itbs_task-rest_run-01_bold.json
    │   ├── sub-219_ses-itbs_task-rest_run-01_bold.nii.gz
    │   └── sub-219_ses-itbs_task-rest_run-01_events.tsv
    ├── perf
    │   ├── sub-219_ses-itbs_asl.json
    │   └── sub-219_ses-itbs_asl.nii.gz
    └── sub-219_ses-itbs_scans.tsv
```

***Note**: Cloud shell doesn't have a tool for viewing NIfTI files, but if this data were on your local computer, you should also look at each image to ensure it is correct.*

If something is amiss, revisit *heuristic_template_sess.py* file and try again.  If there is a mistake in the code, the translator will succeed for all images until it hits the mistake, so pay close attention to what's missing from your output, as this will tell you where to look for mistakes in your translator file.

If you run into trouble with heudiconv running your revisions, try deleting and the `.heudiconv` directory from your bids directory.



**Warning**: 2023/03/13: Heudiconv v0.12.0 fails to run if the `-u $UID` flag is included in the docker command. The upshot is that the files produced by heudiconv belong to root on Google cloudshell (or any Linux system).  To give yourself permission to manipulate the files, run the following command from the `MRIS_HDC` directory:

```bash
sudo chmod -R a+rwx .
```



## Summary (4)

-  Heudiconv is a very flexible converter.  It will let you build data checks right into your conversion by using the flexible conditional statements.
-  **Provenance Tracking**: Heudiconv keeps track of what subjects and sessions were converted, when, and with what settings.  All of this is stored in `data/.heudiconv`.  Heudiconv is also compatible with Datalad which provides a revision control system for data.
-  You can maintain several heudiconv conversion scripts in your code directory.  This is helpful if you need to do a one-off conversion, or the protocol changes and you need to start using a revised one.

-  This dataset is free from all HIPAA restrictions.  However, **most DICOMS contain protected health information, and you should NOT use the Google Cloudshell to convert those DICOMS to NIfTI**. 

-  In this tutorial, you created a *perf* directory containing an ASL file.  ASL is a newer addition to the BIDS specification and is not well supported.  As it stands, the ASL file will not pass the [BIDS validator](https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/bids-validator.html).  But, that is for another day.

