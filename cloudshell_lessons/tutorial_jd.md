# Tutorial for JD: JSON Diff Tool

## Cloud Shell Lessons
-  Compare files (1)
-  Patch files (2)

-----------------------
### Prerequisites

* This tutorial uses Google Cloudshell. 
* You should complete the JSON and `jq` lessons, and be familiar with Unix, and the BIDS data structure.

-------

### Data

If you do not already have it, download the sample dataset and unzip it:
  ```bash
  wget https://bitbucket.org/dpat/neuro4rii/raw/main/data/json_tutorial_data.zip
  ````
---

## Setup On Google Cloud shell

-  Navigate to the `bin` directory in your home. 
-  Get the `jd` binary:
    ```bash
    wget https://github.com/josephburnett/jd/releases/download/v1.4.0/jd-amd64-linux -O jd
    ```
-  Make `jd` executable: 
    ```bash
    chmod a+x jd
    ```
-  Get help with jd:
    ```bash
    jd
    ```

------------
## Compare Files (1)

-  Navigate to `json_tutorial_data/bids_sidecars`

- `jd` compares two JSON files, much like Unix `diff`, but `jd` understands the structure of JSON files. 
  ```bash
  jd sub-test_asl_orig.json sub-test_asl.json
  ```

- This lists what has changed in file2 (sub-test_asl.json) relative to file1 (sub-test_asl_orig.json).

- Each key is listed with an initial `@`.

- Values that have been added are on a line prefixed with a `+`.

- In this case, the comparison only adds values.

- However, the comparison could be more complex and include deleted properties or modified values:
  ```bash
  jd sub-test_asl_orig.json sub-test_asl2.json
  ```

- In this more complex case:    
   1)  MagneticFieldStrength was modified from `3` to `12`,        
   2)  `Modality` was deleted,                
   3)  `"BackgroundSuppression": true` was added.   

----

### Think about this (1)
Add values with a plus: `+`.  How are modification and deletion indicated?

## Solution (1)
-  `-` minus is used to indicate deletion.
-  `-` and `+` are used together to indicate modification.

--------------------
## Part 1: Summary (1)
-  `jd` is very similar to Unix `diff`, except `jd` understands the structure of JSON files.  
-  This is important because `diff` is dumb...it compares lines of text without any understanding of their content. 
-  And because JSON files are inherently unordered, `diff` does not work reliably to identify real changes in content. 

------------------
## Patch Files (2)
-  JD allows you to revise a set of properties (additions, deletions, and modifications) in a single step.  
-  It requires a patch file containing the differences between two files.  
-  This difference file (patch) can then be used to modify a third file.

-------------

### Create a Patch File (2)

-  You  generated the contents of the patch when you compared two files.  
-  Use the output flag `-o` to save that content to a file you can use as a patch (called `patch` here, but you can call it anything you want):
    ```bash
    jd -o patch sub-test_asl_orig.json sub-test_asl.json
    ```
- Confirm that the file `patch` was created and contains the diffs.
-----
## Apply a patch (2)

- Before applying the patch, copy the file so you can compare afterwards. 
  ```bash
  cp sub-1006_T1w.json sub-1006b_T1w.json
  ```
  
- The patch file can be used to revise any other json file: Follow the `-p` flag with the name of the patch file and then the file-to-be-revised:
  ```bash
  jd -p patch sub-1006b_T1w.json
  ```
  
- By default, just like `jq`, `jd -p patch sub-1006b_T1w.json` outputs compact results to the terminal (standard out).  

- This is NOT VERY SATISFYING!

- To actually revise the file, you need to use the same trick we used with `jq`.
  
- Output the file to a temporary new file and then, in a second step, move that temp file to the file-to-be-revised (yes, override permissions if necessary):
  
  ```bash
  jd -p patch sub-1006b_T1w.json >> tmp.$$.json && mv tmp.$$.json sub-1006b_T1w.json
  ```
-----

### Think about this (2)

How can you confirm that sub-1006_T1w.json and sub-1006b_T1w.json are now different?

## Solution (2)

```bash
jd sub-1006_T1w.json sub-1006b_T1w.json
```

-----

### There is a catch (2)

-  A patch will fail if the file-to-be-revised is missing a property that the patch is designed to delete or modify! 
-  For example, if the patch was designed to delete the property `Modality`, but that property was already missing from the file-to-be-revised, then `jd` would fail:

    ```terminal
    2022/01/04 16:41:15 Found  at [Modality]. Expected "MR".
    ```

-------------
## Part 2: Summary (2)

- In some circumstances, like conforming to the BIDS metadata specification for ASL files, it may be necessary to add several JSON properties. 
- This could be done one at a time, manually, but it is faster and more reliable to generate a patch containing the modifications and then apply that patch to all the relevant files.

-----------------
## Conclusion
- `jd` is a valuable tool for comparing and modifying JSON files. 
- Together with knowledge about JSON schemas and `jq`, knowledge of `jd` can facilitate the process of modifying BIDS sidecar files.