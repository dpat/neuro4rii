# Tutorial 3 Unix Configuration

## Cloud Shell Lessons
* Aliases (1)
* Environment Variables (2)
* Set the Search Path (3)

--------------
## Introduction to Bash Configuration
Understanding shell configuration is important if you want to customize your shell to recognize custom shortcut commands (aliases), track information about the current session (environment variables), and find installed programs (set the search path). In this tutorial, you will configure **aliases**, **environment variables**, and the **search path** by copying and saving text into two files: **.bash_aliases** and **.bashrc**.

In this tutorial you will use the editor to save configuration information.  Be careful to distinguish between pasting into the terminal window and saving configurations in a file. 

You do NOT need to make a script recording!

---------------
* **Aliases** are like shortcuts that save you some typing and reduce your memory load.

--------------
* **Environment variables** contain information about your login session (like who you are and what shell you are using)

-------------
* The **search path** is a list of directories where the operating system should look for commands it can run, including installed programs and custom scripts.

------------

## Part 1: Aliases (1)

* Aliases are short commands that you use to save some typing.
* Create a new file called **.bash_aliases**:
  ```bash
  touch .bash_aliases
  ```
* Open **.bash_aliases** with the editor:
  ```bash
  edit .bash_aliases
  ```
  Insert the following aliases into **.bash_aliases** in your editor (NOT your terminal):
	```
  # Misc General Aliases
  alias cls='clear'
  alias duh="du -h"
  alias pathdump='echo $PATH | tr ":" "\n"'
  alias sb='source ~/.bashrc'
  
  # conda aliases
  alias cact='conda activate'
  alias cdact='conda deactivate'
  alias ceu='conda env update'
  alias ci='conda install'
  alias cl='conda list'
  
  # git aliases
  alias gitcia='git add . -A; git commit'
  alias gitd='git diff' #unstaged vs last commit
  alias gitls='git log --oneline'
  alias gits='git status'
  
  # datalad aliases
  alias dl='datalad'
  alias dlr='datalad run'
  alias dlstat='datalad status'
  alias dlsv='datalad save'
  
  # docker aliases
  alias dk=docker
  alias dkc='docker container'
  alias dkls='docker container ls -a'
  alias dki='docker image'
  alias dkils='docker image ls'
  ```
- Any line that starts with `#` is a comment.  Comments are not read by the operating system, they are just helpful annotations for you.
- Organize the aliases as you want (alphabetically, by topic, etc.). 

--------------------

## Save and Test Aliases (1)
* Save and close the  **.bash_aliases** file using the file menu on the upper left.
* Source **.bash_aliases** so that bash reads the file and finds your aliases:
  ```bash
  source .bash_aliases
  ```
* After adding and sourcing these aliases, you can start using them.
* For example, you never have to type `echo $PATH | tr ":" "\n"` again, you can use the alias `pathdump` instead.  
* Look at all your available aliases. Some are built in, and some are the ones you just created:
  ```bash
  alias
  ```
------------
### Questions (1)

-  1.1 How would you try the alias `duh`?

* 1.2 How would you display the meaning of the alias `duh`?
* 1.3 How would you display all of your aliases?

--------------
## Part 1: Summary (1)

* **Aliases** save you some typing. You created and viewed aliases.
  * In this tutorial, you added aliases to a **.bash_aliases** file. Often, aliases are added directly to the **.bashrc** file, which is fine too.
* Use the command `alias` to list all of your aliases.
* Make sure that your configuration changes have been saved to file, not just temporarily implemented in your terminal window.

---------

## Part 2: Environment Variables (2)

In addition to configuring aliases, you should add environment variables to your **.bashrc** file. 

-------------
### Questions (2)

-  2.1 How would you open your `.bashrc` file to edit it? 

-  2.2 Should you paste lines into the editor or the terminal to make them a permanent part of your configuration?

-  2.3 How can you ensure that you have correctly created and saved your new `duh` alias?

-------------
## Order lines in your **.bashrc** (2)
* Linux reads the lines in your **.bashrc** from top to bottom. 

* If you add two conflicting environment variables or aliases, the last one wins!

* Enter the following lines at the **bottom** of the **.bashrc** file to ensure they are not undone by any other commands in the file.
  ```
  export GID=$(id -g)
  export PS1="\u@cloudshell:\W$ "
  ```
  
* Save and close **.bashrc**.

* Source your **.bashrc** file to ensure Linux rereads the file, and applies the changes.

-------------
### Questions (2)

-  2.4 How would you force bash to read the **.bashrc** configuration file? (Hint, you did this before for the **.bash_aliases** file)  

-------------
## Double Check that environment variables were added (2)
* Echo the Group ID variable to ensure that you get something back:
  ```bash
  echo $GID
  ```
* You should see something like:
  ```terminal
  1001
  ```
* The GID is your group ID. You will need to refer to it in a later, in a tutorial about Docker.
* WARNING: If you entered the environment variables in the terminal instead of saving them in the editor, `echo $GID` will work in that particular terminal. 
* To be REALLY sure you made permanant changes, you should open another terminal window and test there. Or, use `sb` to source your **.bashrc** and then echo the `$GID` variable.

--------------
## Built in Environment Variables (2)

* Find out which shell you are using:

  ```bash
  echo $SHELL
  ```

* Find out who you are:

  ```bash
  echo $USER
  ```
* View all available environment variables:

  ```bash
  env
  ```
----------------------

### Questions (2)

-  2.5 How would you display all of your environment variables?

---

## Configure your Prompt (2)

* The `PS1` variable describes how your prompt will look. 

* Customize your prompt by changing the PS1 variable: 

* **Go to [EZ Bash Prompt Generator](https://ezprompt.net/) and generate your own prompt!**

* Create a new prompt, then copy and paste it into the terminal to make the change temporarily.
  * **TIP**: Avoid adding **status elements** as these are more complicated and might be harder to paste onto the commandline.
  
* If you mess up the prompt, try sourcing the **.bashrc** again. You can try the alias:

  ```bash
  sb
  ```

* If that doesn't work, then click the **+** sign next to cloudshell and open a new terminal. The system will reread your **.bashrc** and return you to a normal prompt. 

* If you like your new prompt, then you can add it to your **.bashrc**!

* If you are taking COGS 500, share your revised prompt in the OpenClass practicum by answering the question. 

* In this section, you see that it is sometimes helpful to change a configuration value temporarily so you can see if you like it before making it permanant.

---

### Questions (2)

-  2.6 What prompt did you create with  [EZ Bash Prompt Generator](https://ezprompt.net/)? Display the command for the PS1 variable and what the prompt looks like in your terminal. Why do you like this prompt? (Add this to the discussion)

----------
## Part 2: Summary

* **Environment variables** contain information about your login session.  Many variables are set behind the scenes for you (like `$USER` and `$SHELL`).  
* You added a couple of environment variables to the **.bashrc**:  
  * One variable contained your group ID, which you will need to refer to in a later lesson.  
  * Another variable, `PS1`, describes what your command prompt looks like.
  * Use the command `env` to list all environment variables and their contents.

----------------------
## Part 3: Set the Search Path (3)

* Keep your scripts in one place, and put it in the path.
* The default choice is a directory named **bin** in your home directory. 
* You should have created this directory when you set up Google Cloudshell, but if not, then run `mkdir bin` from your home directory now.
 Make the **bin** directory and source the **.bashrc** again.
* This adds **~/bin** to your path, because there is a statement in another configuration file (**.profile**) adding the user's bin to the path if it exists! Have a look for yourself!
  ```bash
  cat .profile
  ```
* You should see:

  ```terminal
  # set PATH so it includes user's private bin if it exists
  if [ -d "$HOME/bin" ] ; then
  PATH="$HOME/bin:$PATH"
  fi
  ```
---------------
## Save and Test the Search Path (3)

* To **ensure** Linux understands that it should look in your new **bin** directory, source **.profile**: 
  ```bash
  source .profile
  ```

* To add a different path to the configuration, modify **.bashrc**.
* For example, in my **.bashrc** on my mac, I add various neuroimaging programs like AFNI and FSL to my path  (just look, don't copy!), e.g.,:
  ```terminal
  PATH=$PATH:$AFNI_HOME  
  PATH=$PATH:$FSLDIR/bin:$FSLDIR/data/atlases/bin  
  ```
* Although you could put all paths in one long statement, it is easier to organize and read if you break it up so that related paths are in the same statement, as illustrated here!
--------------

* Use the `pathdump` alias to list all the paths bash searches:

```bash
pathdump
```
* Ensure that your **bin** directory is in the listed path (it should be near the top of the pathdump).
* Mine is ``/home/dkp/bin`.  
  * Yours will be similiar, but use **your username** instead of mine. 

* To find the path to a particular command, use **which**:
  ```bash
  which ls
  ```
* In addition to telling you the path to the command, this is a quick way to ensure that the command is in your path.

---

### Questions (3)

-  3.1 How can you display your search path?

---------------------
## Part 3: Summary (3)
* The **Search Path** lists directories where the operating system should look for commands (including scripts). 
* If commands are in the search path, you don't have to type the path to the command to run it.  
* The command `which`  displays the path to a command (if the command is found).
* The alias `pathdump` displays an ordered list of all paths in the search path.
* Unlike aliases and environment variables, the path honors the first path it finds.

-----------
## Conclusion
The tutorial repeatedly emphasized the difference between entering configurations in your terminal (to make changes temporarily) and entering configurations in the approprioate file (to make it permanant). You configured bash shell preferences by altering the **.bashrc** and creating the **.bash_aliases**.  You sourced several files to ensure bash reread them: **.bashrc**, **.bash_aliases**, and **.profile**. Other shells have fewer configuration files than bash (e.g., zsh always executes **.zshrc**), which is less confusing. 

------------

* **Aliases** save you some typing. You created and viewed aliases in a file called **.bash_aliases**. Often, aliases are added directly to the **.bashrc** file, which is fine too.
* Use the command **alias** to list all of your aliases.

-----------

* **Environment variables** contain information about your login session.  Many variables are set behind the scenes for you (like `$USER` and `$SHELL`).  
* You added a couple of environment variables to the **.bashrc:**  
  * One variable contained your group ID, which you will need to refer to in a later lesson on Docker bind mounts.  
  * Another variable, `PS1`, describes what your command prompt looks like.
  * Use the command `env` to list all environment variables and their contents.

-----------

* The **Search Path** lists directories where the operating system should look for commands (including scripts). 
* If commands are in the search path, you don't have to type the path to the command to run it.  
* The command `which` displays the path to a command (if the command is found).
* The alias `pathdump` displays an ordered list of all paths in the search path.

----------------

Once you understand configuration, especially the search path, you are ready to install software and learn to write and maintain scripts.  In susequent tutorials, you will learn to use control structures and to write useful Bash scripts. 







