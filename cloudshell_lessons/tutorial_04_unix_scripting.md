

# Tutorial 4 Unix Scripting

This tutorial focuses on scripting in Bash, especially control structures (loops and conditionals), passing arguments from the command line, and documenting. 

There is a real focus on making sure you are in the correct directory, setting the search path and the permissions.  If you get a `command not found` error, it is because the search path or permissions are wrong.

## Cloud Shell Lessons

Retrieve and Unzip Data (1)
For loops (2)
Sort (3)
Scripts (4)
check_files1.sh (5)
check_files2.sh: A Command line Argument (6)
check_files3.sh: Documentation (7)
check_files4.sh: Two Command line Arguments (8)



----------------------
## Part 1: Retrieve and Unzip Data (1)
* Retrieve **BIDS_MRI.zip**:
  ```bash
  wget https://bitbucket.org/dpat/neuro4rii/raw/main/data/BIDS_MRI.zip
  ```
* In this BIDS-compliant dataset the image files are empty to keep the directory small. 
* Unzip the file, and navigate into the resulting BIDS_MRI directory:
  ```bash
  unzip BIDS_MRI.zip; cd BIDS_MRI/
  ```

--------------------
## Part 2: For loops (2)
* The dataset contains the results of a dwi analysis on several tracts for each subject. 
* The results for each subject are in a text file    
  e.g., see **BIDS_MRI/derivatives/sub-188/analysis/access_tract_bip.txt** 
* Concatenate data for each subject into a single text file for group analysis.  
* In this case, you'll generate the for loop interactively in the terminal.  From the **BIDS_MRI **directory, type the following to define a variable **s** and a pattern that repeats (any directory under derivatives that begins with **sub-**) :
  ```bash
  for s in derivatives/sub-*; do
  ```
* Hit enter. You see a **>** prompt. Define the command you want to run for all instances of **s**. In this case, you want to concatenate any file that starts with `access` under the subject's analysis directory into a new file, **group_tract_bip.txt**, in the current directory.
  ```bash
  cat ${s}/analysis/access* >> ./group_tract_bip.txt
  ```
* Hit enter. Type **done** to end the for loop.  Hit enter to run it.
  ```bash
  done
  ```
* List the files in the directory, and look at the contents of the new file **group_tract_bip.txt**.
* It should contain the rows from **access_tract_bip.txt** for every subject.

**Note**:: If you know what the for loop will look like, you can enter it as a single line (there's no need to run this if you ran the previous commands.  In fact, if you run this again, it'll append the results to the file doubling its size and adding duplicates.  This is because I use the `>>` redirect instead of the `>` redirect.  If I don't use the `>>` redirect, then each subject in the for loop will overwrite the data from the previous subject:

```terminal
for s in derivatives/sub-*; do cat ${s}/analysis/access* >> ./group_tract_bip.txt; done
```

-------------------
### Questions (2)

* 2.1 How would you list the contents of the **fmap** directory for each subject?
* 2.2 How would you list the **bval** and **bvec** files in the dwi directories?

-  2.3 How would you use the for loop to create an additional directory **stats** under derivatives for each subject?

-  2.4 How would you rename the **stats** directory for each subject to **func_stats**?

--------------------
## Part 2: Summary (2)
* A for-loop repeats a block of code a known number of times.
* In Bash, it requires the keywords **for**, **in**, **do**, and **done**.
* The **first** statement begins with **for** and then a variable you make up.
  * Use **in** to specify the pattern or list to iterate over. The examples have used the available subject directories, but the pattern could be a hard-coded list: 
    ```bash
    for a in 1 2 3 4; do echo ${a}; done
    ```
  * End the statement with a semicolon.
* The keyword **do** precedes one or more commands. Use semicolons to separate the commands.
* When you've reached the last command, use a semicolon and the keyword **done** to finish the loop.

----------------------
## Part 3: Sort (3)
* Unfortunately, **group_tract_bip.txt** includes the header row for the file repeated over and over again. 
* Remove the duplicate lines with **sort** 
  ```bash
  sort -u -r group_tract_bip.txt >> clean_group_tract_bip.txt
  ```
* `-u` means retain only unique lines.  
* `-R` sorts the lines in reverse order (in this case, that means the header line ends up at the top, which is what you want).
* Examine **clean_group_tract_bip.txt**. You will use it in a later lesson, so keep it around.

---------------------
## Part 4: Scripts (4)
* If the for loop is complicated and useful, then it is convenient to save it in a text file and re-use it. In addition, saving what you do in a script is part of good data management and allows you to replicate processing exactly.

----------------------
### Retrieve and set up Scripts (4)
* Navigate to your **~/bin** directory and retrieve four *check_files* scripts with this command:
  ```bash
  wget -i https://bitbucket.org/dpat/neuro4rii/raw/main/bash_scripts/script_files.txt
  ```
  
* `wget` can retrieve multiple files with the `-i` option followed by a list of the files to retrieve (e.g. script_files.txt). 

   ### Did you `cd` to your `bin` directory before running this command?

   If not, the scripts will be in the WRONG place!

----------------------
### Questions (4)

* 4.1 How do you check the permissions on the four scripts you just downloaded?
* 4.2 How do you change the permissions so they are executable for everyone?

---------------------
## check_files scripts (4)
* You do not have to add the **.sh** extension. I do it because it helps the code editor recognize and color-code shell scripts appropriately. 
* It is good practice to include a shebang statement: `#!/bin/bash` as the first line of your code. It not only tells Linux which language the script is in, but it helps you remember.
* Because the four scripts are in your search path and are executable, the system will find them when you invoke them.

----------

## Part 4: Summary (4)

Scripts are text files that contain commands you want to rerun and/or document. It is a good idea to store scripts together in a directory in the search path.  In addition, it is helpful to ensure the scripts are executable.

----------------------
## Part 5: check_files1.sh (5)
* **check_files1.sh** loops through your subject directories looking for any subject that is missing the magnitude2 image. 
  
* Run it from your **BIDS_MRI/data** directory:
  
  ```bash
  cd ~/BIDS_MRI/data
  check_files1.sh
  ```
  
* **check_files1.sh** contains a for-loop AND a conditional statement. 

* For each subject: 
  * it echos a divider line `=========` to improve the look of the output.
  * It echos the subject `${s}`
  * It then runs a conditional test:   
    ```terminal
    if [ ! -e "${s}/fmap/${s}_magnitude2.nii.gz" ]
    ```
    This says: *if not exist the fmap magnitude2 image for this subject* (`!`=not; `-e`= exists, `${s}` is the variable for the subject number and will be replaced by the current subject number)
  * If the condition is true, then the code echos an alert, else it echos that the subject is fine.
    ```terminal
    echo "ALERT: ${s} is missing magnitude2 data!"
	  else 
	  echo "${s} is fine"
    ```

## Check the Results!

- The output should look like this:

  ```terminal
  =================
  sub-188
  ALERT: sub-188 is missing magnitude2 data!
  =================
  sub-190
  sub-190 is fine
  =================
  sub-194
  sub-194 is fine
  =================
  sub-215
  sub-215 is fine
  =================
  sub-221
  sub-221 is fine
  ```

## Troubleshooting

If you do not see this, then you probably have a problem with permissions or the search path. Go back and ensure that you have execute permissions on the scripts in your bin directory AND ensure that `~/bin` is in your search path.

### Check that your path variable contains your bin directory

  ```terminal
  # echo the path variable
  echo $PATH
  ```
The path should include your **bin** directory, e.g., 

*/home/dkp/.local/bin:**/home/dkp/bin**:/home/dkp/miniconda/condabin:/opt/gradle/bin:/opt/maven/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/local/go/bin:/usr/local/nvm/versions/node/v16.4.0/bin:/usr/local/rvm/bin:/google/go_appengine:/google/google_appengine:/google/migrate/anthos:/home/dkp/.gems/bin:/usr/local/rvm/bin:/home/dkp/gopath/bin:/google/gopath/bin:/google/flutter/bin*

### Ensure the new scripts are found

```terminal
which check_files1.sh
```

This should return the path to the script in your **bin** directory, e.g., **/home/dkp/bin/check_files1.sh**
If you can find one, the others *should* be fine too.

If your home bin is not in the path, you *may* have to source your `.bashrc`.  The following should work no matter where you are in the directory tree because it uses the tilde to stand for your home directory.

```terminal
source ~/.bashrc
```

------------

## Part 5: Summary (5)

**check_files1.sh**  is more complex than what you wrote at the command line: it incorporates a conditional statement inside the for loop. 

**check_files1.sh** has some problems:

* It has limited functionality: it only identifies missing magnitude2 images. 
* It has very little documentation: It would be easy to forget what directory you have to run **check_files1.sh** in, or even what it does!

-----------

## Part 6: check_files2.sh: A Command line argument (6)

* **check_files2.sh** also runs from your **BIDS_MRI/data** directory.  However, its functionality is more general.

* Instead of specifying **magnitude2**, it takes a command line argument specifying which file to check for.

* To specify an argument, the code adds: 

  ```terminal
  img_base=$1
  ```

* This line means that the first argument on the command line will get the name **img_base**

* In the rest of the script, `${img_base}` is used anywhere that **magnitude2** was used before:

  ```terminal
  for s in sub-*; do
  echo "================="
  # print the subject dir name
  echo ${s}
    if [ ! -e "${s}/fmap/${s}_${img_base}.nii.gz" ]; then
  	echo "ALERT: ${s} is missing ${img_base} data!"
  	else 
  	echo "${s} is fine"
    fi
  done
  ```



## Run check_files2.sh

- Run it like this from your **BIDS_MRI/data** directory, e.g.: 

  ```bash
  check_files2.sh phasediff
  ```

  (You can enter `magnitude1`, `magnitude2` or `phasediff` as the argument to the script)

- Your output should look like this:

  ```terminal
    =================
    sub-188
    sub-188 is fine
    =================
    sub-190
    sub-190 is fine
    =================
    sub-194
    sub-194 is fine
    =================
    sub-215
    sub-215 is fine
    =================
    sub-221
    ALERT: sub-221 is missing phasediff data!
  ```

--------

## Part 6: Summary (6)

**check_files2.sh** is an improvement because it is more general that the first script.

**check_files2.sh** has some problems: 

- Good luck to the poor user: This script desperately needs documentation!
- It could be even more general.



-----------

## Part 7: check_files3.sh: Documentation (7)

**check_files3.sh** adds two forms of documentation: 

* Comments are lines starting with `#`. They document the code as you read the script.  

* The usage message explains the code to the user when it is called incorrectly:

  *  `$#`= the number of arguments; `-lt` = less than (See [bash hackers wiki](https://wiki.bash-hackers.org/commands/classictest) for details about specifying conditions)
  * The argument `${0}` is replaced by the name of the script in the message the user sees.

* `exit 1` stops the script from running if the usage message has been displayed.

  ```terminal
  # If the user provides less than one argument to the script, 
  # then print the help message
  
  if [ $# -lt 1 ]
   then
    echo ""
    echo "The script reports missing fmap files."
    echo "Run it from the bids data directory"
    echo "It takes one argument, the name of the fmap file type:"
    echo "e.g., phasediff, magnitude1, magnitude2"
    echo "example: ${0} phasediff"
    echo ""
    exit 1
  fi
  ```

## Try check_files3.sh (7)

* Call the script without an argument to see the usage message:

  ```bash
  check_files3.sh
  ```
  
* The usage message looks like this:

  ```terminal
  The script reports missing fmap files.
  It takes one argument, the name of the fmap file type:
  e.g., phasediff, magnitude1, magnitude2
  example: /Volumes/Main/Working/neuro4rii/bash_scripts/check_files3.sh phasediff
  ```

-  Follow the instructions and try one of the three options, e.g.,:

   ```bash
   check_files3.sh magnitude1
   ```

   

---

## Part 7: Summary (7)

**check_files3.sh** is an improvement because it has documentation:

- It tells you what directory to run from
- It describes the arguments
- It provides an example

**check_files3.sh** has some problems: 

- It could be even more general!

-----------

## Part 8: check_files4.sh: Two Command line arguments (8)

**check_files4.sh** adds a second command line argument to generalize the script for other directories (not just **fmap**).

  ```terminal
  #!/bin/bash

  # If the user provides less than two arguments to the script, 
  # then print the help message
  if [ $# -lt 2 ]
   then
    echo ""
    echo "The script reports missing image (*.nii.gz) files."
    echo "Run it from the bids data directory containing your subjects."
    echo ""
    echo "It takes two arguments:" 
    echo "1) the name of the file (without the subject or nii.gz extension)"
    echo "e.g., phasediff, magnitude1, magnitude2"
    echo "2) the name of the directory to check"
    echo "e.g., anat, dwi, fmap, func"
    echo "example:  ${0} phasediff fmap"
    echo "example: ${0} acq-AP_dwi dwi"
    echo "example: ${0} task-nad1_run-04_bold func"
    echo ""
    exit 1
  fi

  # Argument 1 is the part of the image name between the subject number and the extension.
  # e.g., magnitude1, magnitude2, phasediff, acq-AP_dwi, task-nad1_run-04_bold
  img_base=$1
  # Argumant 2 is the directory wheere the image file should be (e.g., anat, dwi, fmap, func) 
  modality=$2

  # Remind the user of the modality and image file name being checked.
  echo "Checking each subject for ${modality}: ${img_base}"

  # For each directory starting with sub-, print the requested information
  # about any missing files.
  for s in sub-*; do
  echo "----------------------------"
  # print the subject dir name
  echo ${s}
    # if not exist the specified file
    if [ ! -e "${s}/${modality}/${s}_${img_base}.nii.gz" ]; then
    # then print an alert with details
    echo "ALERT: ${s} is missing ${modality}: ${img_base} data!"
    else 
    # Otherwise, print that it is fine
    echo "${s} is fine"
    fi
  done
  ```

## View the Usage Message (8)

The usage message explains how to run it and provides examples:

```bash
check_files4.sh
```

---------------
### Question (8)

-  8.1 How would you run **check_files4.sh** for the **task-nad1_run-02_bold** files?

-----------
## Part 8: Summary (8)

**check_files4.sh** is an improvement because it is very general.
It could really use a better name!

---------------

### Try tree (9)

* Now try tree:

   ```bash
   tree BIDS_MRI
   ```

* Use the `-d` flag to list just the directories without their contents:

   ```bash
   tree -d BIDS_MRI
   ```

-  See one level of directories only:

   ```bash
   tree -L 1
   ```

## Conclusion

This tutorial introduced you to bash scripting.  Initially, it may take a long time to implement a script. It may even be faster to make a change without a script.  But, if you take the time to figure out the script, it'll save you time in the future, and you'll get better and better.

* You learned to implement a for loop.
* You saw a conditional nested in a for-loop.
* You learned to save commands as a script, ensure executable permissions, and place your script in the search path.
* You learned to pass one or more arguments to a script from the command line.
* The structures and logic you saw here are similar to those you would create in other programming languages.

-------------

### Next Steps

- Use Google and [grepper](https://www.codegrepper.com/) to your advantage to look up how to implement some function in code. 

- Learn to use revision control (git) so you don't lose anything you've implemented.

