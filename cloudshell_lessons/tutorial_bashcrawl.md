# What is Bashcrawl?

In this practicum, you are going to play a game called bashcrawl in Google Cloudshell.   Playing for about an hour or so should be very helpful.  The game will give you experience with commands you've been learning, and about the Unix directory tree, permissions, environment variables, linking, and globbing.

The game was forked from https://gitlab.com/slackermedia/bashcrawl.

## Install Tree

**If you have not already installed** `tree` in Google Cloud shell, you should install it now. `tree` is very similar to `ls -R` but much cleaner.  I highly recommend it.  

**TIP**: This assumes you have a directory called `bin` in your home directory.  This will be true if you completed `tutorial_01_unix.md`.  

Enter the following command to get tree, put it in your bin directory (you have one, right?), and make it executable:

```bash
wget https://bitbucket.org/dpat/neuro4rii/raw/main/binaries/tree -O ~/bin/tree; chmod a+x ~/bin/tree
```

You installed a small but useful program called `tree` which helps display directory nesting.

## Start the game

```bash
cd entrance
```

Your first move is very important. Type this into your terminal:

```
cat scroll
```

Read the scroll!

Next you should go to the cellar, collect the treasure.

```bash
cd cellar
./treasure
```

The procedure for adding to your inventory is tedious and hard to remember!

Carefully read the scroll in the cellar. This scroll of configuration will make your life easier.  Follow its instructions carefully.

```bash
cat scroll
```

Finish the tutorial.    
You are now playing the game.   
May the gods save you.   

## You can pause and resume the game

At any time you can **go to the entrance** and run the `save` command.  This will save your inventory and health points so you can come back later.

```bash
./save
```

When next you go to the entrance, resume the game like this

```bash
./resume
```









