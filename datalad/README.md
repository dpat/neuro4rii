# Time Savers

## Mask Image Manipulation
**count** cluster count with neighborhood of 6 (the most stringent)  
**clean** cluster_clean.sh with neighborhood of 6   
**fill**  fill holes in a mask  
**smooth** safe smoothing: output has *_smooth appended to facilitate comparison.    
**tochar** change datatype to char (8-bit)       
**img_info** provide information on NIfTI files: voxel dim, mean, sd, range, non-zero voxel count 

## Mask Image Manipulation complete with Datalad save  
Run these from the main subject directory. Many call the functions listed above, but just do so through datalad, thus doing the save and creating an appropriate save message.  

**dl_addnums** Rename files in a subject directory to prepend the subject number as the Beeson lab prefers.
Run this for all of the subject directories unless one or more subject directories are specified.  See also *dl_rmnums* below.
(so 0 arguments: help, 1 argument=all, process all available sub-* subdirectories; list of subject dirs = process those listed:)  

**dl_clean** Example: code/dl_clean lesion.nii.gz 1000  
This culls clusters smaller than the threshold size. The neighbourhood for clusterhood is 6 (which is the most rigorous).  

**dl_dilate** This dilates a mask in 3D by the provided number of iterations:   
code/dl_dilate lesion.nii.gz 3  

**dl_erode** This erodes a mask in 3D by the provided number of iterations:   
code/dl_erode lesion.nii.gz 3  

**dl_fill** This script will fill holes in a mask. It requires the path to the mask to be filled.  

**dl_fixmask** This script will fix a mask by ensuring it is binary, 8 bit (char) and without Nans.  

**dl_rmnums** run code/rmnums to remove initial numbers from image file names.  

**dl_smooth** This script will smooth a mask, producing a new file with *_smooth appended. It requires the path to the mask to be smoothed.  

## DP DataLad Aliases
alias **datalad**='/Users/dpat/anaconda3/envs/datalad/bin/datalad'     
alias **dl**='datalad'    
alias **dlcp**='datalad copy-file'   
alias **dlr**='datalad run'   
alias **dls**='datalad status'   
alias **dlstat**='datalad status'   
alias **dlsv**='datalad save'   
alias **dlsib**='datalad siblings'    
alias **dlu**='datalad unlock'     
alias **dl_code** add code from current directory to the path  
alias **dl_ls** long listing, but don't display link path
