
import os

def create_key(template, outtype=('nii.gz',), annotation_classes=None):
    if template is None or not template:
        raise ValueError('Template must be a valid format string')
    return template, outtype, annotation_classes


def infotodict(seqinfo):
    # Section 1: These key-value pairs should be revised by the user
    ###################################################################
    # define keys and values for each sequence: a short name for the key
    # and an output directory path and name for the value.
    # data defines a special key that creates sequential numbers.
    # These are available to other keys for naming sequences. This is especially valuable
    # if you run the same sequence multiple times at the scanner
    data = create_key('run-{item:03d}')
    t1w = create_key('sub-{subject}/anat/sub-{subject}_T1w')
    flair = create_key('sub-{subject}/anat/sub-{subject}_FLAIR')
    flair_nd = create_key('sub-{subject}/anat/sub-{subject}_acq-nd_FLAIR')
    dwi_ap = create_key('sub-{subject}/dwi/sub-{subject}_acq-AP_dwi')
    fmap_dwi_pa =  create_key('sub-{subject}/fmap/sub-{subject}_dir-PA_epi')
    fmap_mag_3mm =  create_key('sub-{subject}/fmap/sub-{subject}_acq-3mm_magnitude')
    fmap_phase_3mm = create_key('sub-{subject}/fmap/sub-{subject}_acq-3mm_phasediff')
    fmap_mag_4mm =  create_key('sub-{subject}/fmap/sub-{subject}_acq-4mm_magnitude')
    fmap_phase_4mm = create_key('sub-{subject}/fmap/sub-{subject}_acq-4mm_phasediff')
    func_rest = create_key('sub-{subject}/func/sub-{subject}_task-rest_bold')
    func_train = create_key('sub-{subject}/func/sub-{subject}_task-train_run-{item:03d}_bold')
    func_test = create_key('sub-{subject}/func/sub-{subject}_task-test_run-{item:03d}_bold')
    ep2d_ap = create_key('sub-{subject}/fmap/sub-{subject}_acq-ep2dap')
    ep2d_pa = create_key('sub-{subject}/fmap/sub-{subject}_acq-ep2dpa')


    # Section 1b: This data dictionary (below) should be revised by the user.
    ##########################################################################
    # Enter a key in the dictionary for each key-value pair you created above in section 1.
    info = {data: [], t1w: [], flair_nd: [], flair: [], fmap_dwi_pa: [], dwi_ap: [], fmap_mag_3mm: [], fmap_phase_3mm: [], fmap_mag_4mm: [], fmap_phase_4mm: [],func_rest: [], func_train: [], func_test: [], ep2d_ap: [], ep2d_pa: []}
    last_run = len(seqinfo)

    # Section 2: These criteria should be revised by user.
    ##########################################################
    # Define test criteria to check that each dicom sequence is correct
    # seqinfo (s) refers to information in dicominfo.tsv. Consult that file for
    # available criteria.
    # Here we use two types of criteria:
    # 1) An equivalent field "==" (e.g., good for checking dimensions)
    # 2) A field that includes a string (e.g., 'mprage' in s.protocol_name)
    for idx, s in enumerate(seqinfo):
        if ('mprage' in s.protocol_name):
             info[t1w].append(s.series_id)
        if ('T2 FLAIR ND' in s.protocol_name):
             info[flair_nd].append(s.series_id)
        if ('T2 FLAIR' in s.protocol_name):
             info[flair].append(s.series_id)
        if ('DTI' in s.protocol_name) and (s.dim3 == 66) and (s.dim4 == 74):
            info[dwi_ap].append(s.series_id)
        if ('DTI_REVERSE' in s.protocol_name):
            info[fmap_dwi_pa] = [s.series_id]
        if ('field_mapping 3mm' in s.protocol_name) and (s.dim3 == 88):
            info[fmap_mag_3mm] = [s.series_id]
        if ('field_mapping 3mm' in s.protocol_name) and (s.dim3 == 44):
            info[fmap_phase_3mm] = [s.series_id]
        if ('field_mapping 4mm' in s.protocol_name) and (s.dim3 == 88):
            info[fmap_mag_4mm] = [s.series_id]
        if ('field_mapping 4mm' in s.protocol_name) and (s.dim3 == 44):
            info[fmap_phase_4mm] = [s.series_id]
        if ('RestingState' == s.protocol_name):
            info[func_rest].append(s.series_id)
        if ('EPI' in s.protocol_name) and (s.dim4 == 33):
            info[func_train].append(s.series_id)
        if ('EPI' in s.protocol_name) and (s.dim4 == 300):
            info[func_test].append(s.series_id)
        if ('ep2d_se_ap' == s.protocol_name):
            info[ep2d_ap].append(s.series_id)
        if ('ep2d_se_pa' == s.protocol_name):
            info[ep2d_pa].append(s.series_id)
    return info
