import os


def create_key(template, outtype=('nii.gz',), annotation_classes=None):
    if template is None or not template:
        raise ValueError('Template must be a valid format string')
    return template, outtype, annotation_classes


def infotodict(seqinfo):
    # Define keys for each sequence
    # If there are sessions, then the session must be a folder name and appear in the file name
    t1w = create_key('sub-{subject}/{session}/anat/sub-{subject}_{session}_T1w')
    dwi = create_key('sub-{subject}/{session}/dwi/sub-{subject}_{session}_acq-AP_dwi')
    # Save the RPE (reverse phase-encode) B0 image as a fieldmap (fmap).  It will be used to correct
    # the distortion in the DWI
    fmap_rev_phase =  create_key('sub-{subject}/{session}/fmap/sub-{subject}_{session}_dir-PA_epi')
    fmap_mag =  create_key('sub-{subject}/{session}/fmap/sub-{subject}_{session}_magnitude')
    fmap_phase = create_key('sub-{subject}/{session}/fmap/sub-{subject}_{session}_phasediff')
    func_rest = create_key('sub-{subject}/{session}/func/sub-{subject}_{session}_task-rest_run-01_bold')
    func_rest_post = create_key('sub-{subject}/{session}/func/sub-{subject}_{session}_task-rest_run-02_bold')

    # info is a Python dictionary containing the following keys from the infotodict defined above.
    # It is initiated with no values
    info = {t1w: [], dwi: [], fmap_rev_phase: [], fmap_mag: [], fmap_phase: [], func_rest: [], func_rest_post: []}

    # define test criteria to check that each sequence is correct
    # The following illustrates the use of multiple conditions:
    for idx, s in enumerate(seqinfo):
        # Dimension 3 must equal 176 and the string 'mprage' must appear somewhere in the protocol_name
        if (s.dim3 == 176) and ('mprage' in s.protocol_name):
            info[t1w].append(s.series_id)
        # Dimension 3 must equal 74 and dimension 4 must equal 32, and the string 'DTI' must appear somewhere in the protocol_name
        if (s.dim3 == 74) and (s.dim4 == 32) and ('DTI' in s.protocol_name):
            info[dwi].append(s.series_id)
        # The string 'verify_P-A' must appear somewhere in the protocol_name    
        if ('verify_P-A' in s.protocol_name):
            info[fmap_rev_phase] = [s.series_id]
        # Dimension 3 must equal 64, and the string 'field_mapping' must appear somewhere in the protocol_name    
        if (s.dim3 == 64) and ('field_mapping' in s.protocol_name):
            info[fmap_mag] = [s.series_id]
        # Dimension 3 must equal 32, and the string 'field_mapping' must appear somewhere in the protocol_name      
        if (s.dim3 == 32) and ('field_mapping' in s.protocol_name):
            info[fmap_phase] = [s.series_id]
        # The string 'resting_state' must appear somewhere in the protocol_name and the boolean field is_motion_corrected must be False (i.e. not motion corrected) 
        # This ensures I do NOT get the motion corrected MOCO series instead of the raw series.
        if ('restingstate' == s.protocol_name) and (not s.is_motion_corrected):
            info[func_rest].append(s.series_id) 
        # The string 'Post_TMS_resting_state' must appear somewhere in the protocol_name and the boolean field is_motion_corrected must be False (i.e. not motion corrected) 
        # This ensures I do NOT get the motion corrected MOCO series instead of the raw series.
        if ('Post_TMS_restingstate' == s.protocol_name) and (not s.is_motion_corrected):
            info[func_rest_post].append(s.series_id)

    # Populate a list if the wrong number of files is created
    # (No message is a good message)
    msg = []

    if len(info[t1w]) != 1: msg.append('WARNING: Missing correct number of T1w runs')
    if len(info[dwi]) != 1: msg.append('WARNING: Missing correct number of dwi runs')
    if len(info[fmap_rev_phase]) != 1: msg.append('WARNING: Missing correct number of RPE runs')
    if len(info[fmap_mag]) != 1: msg.append('WARNING: Missing correct number of magnitude runs')
    if len(info[fmap_phase]) != 1: msg.append('WARNING: Missing correct number of phasediff runs')
    if len(info[func_rest]) != 1: msg.append('WARNING: Missing correct number of resting_state runs')
    if len(info[func_rest_post]) != 1: msg.append('WARNING: Missing correct number of resting_state_post runs')

    # If there is an error, a message will be generated and no NIfTI files will be generated for the subject.
    if msg:
      raise ValueError('\n'.join(msg))

    return info

    